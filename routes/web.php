<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\UserVerifiedEmail;


//Untuk API


Route::group(['middleware' => ['api_token']], function() {
    Route::middleware('throttle:30,1,default')->group(function () {
        Route::GET('public/api/v1/spacecat/acc/{room_secret_key}/{rfid}/{mac}/{ip}/{ssid}', 'ApidevicesController@proccess');
        Route::POST('public/api/v1/spacecat/dvc/{room_secret_key}', 'DevicesController@store');
        // Route::POST('api/v1/spacecat/pwd/{room_secret_key}', 'ApidevicesController@proccess_password');
        Route::POST('public/api/v1/spacecat/pwd/{room_secret_key}', 'PasswordTemp@cekpassword');
        Route::POST('public/api/v1/spacecat/paramp/{room_secret_key}', 'RoomController@cekpassword');
        // Route::POST('api/v1/spacecat/usrprm/{room_secret_key}/{rfid}', 'UsersParameters@api_store_users_parameter');
    });
});

// Untuk Web Dashboard
// Route::group(['prefix' => 'member', 'middleware' => 'auth'], function () {
Auth::routes();
Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function() {

    Route::get('profile', 'UsersAdmin@index')->name('user.setting');
    Route::post('profile/change-password', 'UsersAdmin@chanagePassword')->name('change.password');

    Route::POST('resendmail/{id}', 'ProviderUser@resendemail')->name('resend.mail');
    Route::get('dashboard','ProjectsController@index')->name('dashboard');
    Route::get('project/add', 'ProjectsController@create')->name('tambah.project');
    Route::get('edit/{id_project}', 'ProjectsController@edit')->name('edit.project');
    Route::resource('projects', 'ProjectsController');

    //users
    Route::get('users/{id}', 'UsersController@index')->name('user.list');
    Route::get('search/user/{id_project}', 'UsersController@search')->name('search.user');
    Route::get('user/detail/{id_project}/{id}', 'UsersController@show')->name('lihat.user');
    Route::get('user/add/{id_project}', 'UsersController@adduser')->name('tambah.user');
    Route::put('user/update-basicinfo/{id_project}/{id_user}', 'UsersController@updatebasicinfo')->name('update.basicinfo');
    Route::put('user/update-pass/{id_project}/{id_user}', 'UsersController@updatepass')->name('update.password');
    Route::get('user/delete/{id}', 'UsersController@destroy')->name('user.destroy');
    Route::get('user/save/{id}', 'UsersController@destroy')->name('user.store');
    Route::get('user/parameters/{id_project}/{id_user}/{id_parameters}', 'UsersController@logparameters')->name('user.log.parameters');
    Route::resource('user', 'UsersController');

    //room
    Route::get('placement/{id}', 'RoomController@index')->name('daftar.placement');
    Route::get('placement/detail/{id_project}/{id}', 'RoomController@show')->name('placement.detail');
    Route::get('placement/edit/{id_project}/{id}', 'RoomController@edit')->name('placement.edit');
    Route::get('placement/add/{id}', 'RoomController@create')->name('placement.addnew');
    Route::PUT('placement/update/{id}', 'RoomController@update')->name('placement.update');
    Route::get('deleteroom/{id}', 'RoomController@destroy')->name('room.delete');

    Route::get('settings/{id_project}/{id_room}', 'RoomController@settings')->name('placement.settings');
    Route::POST('settings/update/{id_project}/{id_room}', 'RoomController@updatesettings')->name('placement.update.settings');
    Route::get('settings/edit/{id_project}/{id_room}/{id_parameter}', 'RoomController@editsettings')->name('placement.edit.settings');
    Route::PUT('settings/save/{id_project}/{id_room}/{id_parameter}', 'RoomController@savesettings')->name('placement.save.settings');
    Route::resource('room', 'RoomController');

    /// TIDAK DIPAKAI LAGI
    Route::GET('parameter/{id_porject}', 'ParametersUserController@index')->name('parameter.list');
    Route::GET('parameter/add/{id_project}', 'ParametersUserController@create')->name('parameter.add');
    Route::POST('parameter/save/{id_project}', 'ParametersUserController@store')->name('parameter.save');
    Route::GET('parameter/edit/{id_project}/{id_parameter}', 'ParametersUserController@show')->name('parameter.edits');
    Route::PUT('parameter/update/{id_project}/{id_parameter}', 'ParametersUserController@update')->name('parameter.updates');
    Route::GET('parameter/delete/{id}', 'ParametersUserController@destroy')->name('parameter.delete');
    Route::resource('parameter', 'ParametersUserController');
    //untuk melihat rules pada suatu room
    // Route::get('/rules/{id}', "RuleController@index")->name('rules.index/');
    /// TIDAK DIPAKAI LAGI


    Route::get('delete/{id}', 'FieldController@destroy')->name('field.delete');

    Route::POST('saverules/{id_project}/{id_room}', "RuleController@store")->name('save.rules');
    Route::PUT('rule/update/{id_project}/{id_room}/{id_rules}', "RuleController@update")->name('update.rules');
    Route::get('rule/add/{id_project}/{id_room}', "RuleController@create")->name('add.rules');
    Route::get('/edit/rule/{id}/{id_project}', "RuleController@edit");
    Route::get('rule/view/{id_project}/{id_room}/{id_rules}', 'RuleController@show')->name('show.rule');
    Route::get('rule/delete/{id}', 'RuleController@destroy')->name('delete.rules');
    Route::resource('rule', 'RuleController');


    Route::POST('storeruleaccess/{id_project}/{id_rooom}', "RuleController@storeruleaccess")->name('save.rule');
    Route::POST('storeschedule/{id_project}/{id_rooom}', "RuleController@storeschedule")->name('save.schedule');
    Route::POST('update/{id_room}', "RuleController@update")->name('update.rules_access');

    Route::get('devices/{id}', "DevicesController@index")->name('list.device');
    Route::resource('devices', 'DevicesController');
    Route::get('deletedevices/{id}', 'DevicesController@destroy')->name('devices.delete');


    Route::get('logs/{id_project}', "LogDataController@index")->name('log');
    Route::resource('logs', "LogDataController");
    // Route::get('get/schedule/{id}', "LogDataController@LogDataController")->name('get.log');
    Route::get('log/{id_project}/{id_room}', "LogDataController@getCompleteLog")->name('get.complete.log');


    Route::get('user/settings', "UsersAdmin@index")->name('show.useradmin');

    // ADMIN ONLY

    // Route::get('/administrator/manage/')
});

Route::POST('register/github', 'ProviderUser@create')->name('redirect.github');
Route::get('login/github', 'Auth\LoginController@redirectToProvider')->name('login.github');
Route::get('login/github/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
// Route::get('sentemail', 'JobMailController@enqueue');
Route::get('error', function(){
    return view('error_pages.404');
});
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// verifikasi user ketika sudah register.
Route::get('verify/{token}', 'Auth\RegisterController@verifyEmail')->name('verifymail');

Route::get('/', function () {
//    url('/login');
    return redirect()->guest('login');
});
