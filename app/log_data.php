<?php

namespace App;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Log_data extends Model{
    //

    protected $table = 'log_data';
    protected $casts = ['id' => 'string'];

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        "id_reference_user_doing",
        "id_room",
        "id_rules",
        "id_project",
        "id_user",
        "id_device",
        "mac_address",
        "ip_address",
        "wifi_ssid",
        "user_doing",
        "callback",
        'note_schedule',
        'old_room_name',
        'old_rule_name',
        'old_project_name',
        'old_user_name',
        'old_device_name'
    ];

    public function log(){
        return $this->belongsTo(Projects::class, 'id_project');
    }

    public function room(){
        return $this->belongsTo(RoomModel::class, 'id_room');
    }

    public function device(){
        return $this->belongsTo(Devices::class, 'id_device');
    }

    public function user(){
        return $this->belongsTo(UserModel::class, 'id_user');
    }

    public function allTap(){
        return $this->belongsTo(RoomModel::class, 'id_room');
    }

}
