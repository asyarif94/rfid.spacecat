<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Webpatser\Uuid\Uuid;
use App\Notifications\NotifyUser;

class User extends Authenticatable {

    use Notifiable;

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','id_plan', 'id_level' , 'proficiency', 'token', 'provider_id', 'provider', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // public function sendEmailVerificationNotification(){
    //     $this->notify(new NotifyUser($this->token));
    // }

    public function plan(){
        return $this->hasOne(PlanModel::class, 'id' ,'id_plan');
    }

    public function level(){
        return $this->hasOne(UsersLevel::class, 'id', 'id_level');
    }

    public function projects(){
        return $this->hasMany(Projects::class, 'id_pembuat');
    }


}
