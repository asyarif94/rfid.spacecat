<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Devices extends Model{
    protected $table = 'devices';
    protected $casts = [
        'id' => 'string',
    ];

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id',
        'id_room',
        'id_project',
        'device_name',
        'type_devices',
        'firmware_version',
        'ssid',
        'wifi_strength',
        'secret_key_room',
        'project_key',
        'mac_address',
        'local_ip_address',
        'esp_chip_id',
        'core_version',
        'sdk_version',
        'boot_version',
        'boot_mode',
        'uptime',
        'rc522_chip_version'
    ];

    public function room(){
        return $this->belongsTo(RoomModel::class, 'id');
    }


}
