<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

class RoomModel extends Model{

    protected $table = 'roomdata';
    protected $casts = ['id' => 'string'];
    protected $primaryKey = 'id';
    // protected $keyType = 'string';

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id',
        'id_project',
        'name',
        'status',
        'enable_parameters',
        'guest_mode',
        'secret_key',
        'description'
    ];

    public function project(){
        return $this->belongsTo(Projects::class, 'id_project');
    }

    public function users_selected(){
        return $this->hasMany(RulesAccessModel::class, 'id_project');
    }



    public function associatedUser(){
        return $this->hasMany(RulesAccessModel::class, 'id_room');
    }

    public function getStrIdAttribute(){
        return (string) $this->id;
    }

    public function schedule(){
        return $this->hasMany(ScheduleModel::class, 'id_room');
    }

    public function tapToday(){
        return $this->hasMany(Log_data::class, 'id_room')->where('user_doing', '=', 'Success Tap Card')->whereDate('created_at', '=', Carbon::today()->toDateString());
    }

    public function allTap(){
        return $this->hasMany(Log_data::class, 'id_room')->where('user_doing', '=', 'Success Tap Card');
    }

    public function device(){
        return $this->hasMany(Devices::class, 'id_room');
    }

    public function therules(){
        return $this->hasMany(Rules_model::class, 'id_room');
    }

    //relasi ini untuk user parameters
    public function placementparam(){
        return $this->hasMany(ConnectorRuleToParameter::class, 'id_placement');
    }


    //relasi ini untuk placement parameters
    public function placementparameters(){
        return $this->hasMany(PlacementParameters::class, 'id_placement');
    }


}
