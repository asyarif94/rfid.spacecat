<?php

namespace App;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class PlacementParameters extends Model{

    protected $table = 'placement_parameters';
    protected $casts = ['id' => 'string'];

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id_placement',
        'name',
        'sub_name',
        'allow_remote',
        'value',
        'unit',
        'min_value',
        'max_value',
        'operator',
        'by_value',
    ];

    public function project(){
        return $this->belongsTo(RoomModel::class, 'id_placement');
    }

}
