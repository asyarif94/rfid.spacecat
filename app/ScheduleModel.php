<?php

namespace App;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class ScheduleModel extends Model{

    protected $table = 'rules_schedule';
    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id_project',
        'id_room',
        'id_rules',
        'is_schedule_active',
        'name_schedule',
        'type_schedule',
        'date',
        'days',
        'start_time',
        'end_time',
        'offset_time'
    ];

    public function schedule(){
        return $this->belongsTo(RulesAccessModel::class, 'id_room');
    }
}
