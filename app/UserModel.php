<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
use App\Notifications\NotifyUser;
use Illuminate\Notifications\Notifiable;

class UserModel extends Model{

    protected $table = 'usersdata';

    protected $primaryKey = 'id';
    protected $keyType = 'string';


    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id',
        'name',
        'avatar',
        'email',
        'phonenr',
        'rfid',
        'password',
        'status',
        'description',
        'id_project'
    ];

    public function project(){
        return $this->belongsTo(Projects::class, 'id_project');
    }


    public function log(){
        return $this->belongsTo(Log_data::class, 'id_user');
    }

    public function rules(){
        return $this->hasMany(RulesAccessModel::class, 'id_user');
    }


    public function getUrlAvatarAttribute(){
        return is_null($this->avatar) ? asset('media/users/default.jpg') : asset('media/users/' .$this->avatar);
    }

    public function parameters(){
        return $this->belongsToMany(UserParameters::class, 'users_to_parameters', 'user_model_id', 'user_parameters_id' )->withPivot('id')->withPivot('values')->withTimestamps();
    }


    public function logparameters(){
        return $this->hasMany(LogUserParameter::class, 'user_id');
    }


}
