<?php

namespace App;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class AmountModel extends Model{

    protected $table = 'rulesdata_amount';

    protected $primaryKey = 'id';
    protected $keyType = 'string';


    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id_rules',
        'is_amount_active',
        'name_amount',
        'action_amount',
        'value_amount'
    ];

}
