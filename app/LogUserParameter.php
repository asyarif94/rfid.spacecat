<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class LogUserParameter extends Model{
    protected $table = "log_user_parameter";

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'user_id',
        'parameter_id',
        'last_value',
        'new_value',
        'operator'
    ];


    public function user(){
        return $this->belongsTo(UserModel::class);
    }

}
