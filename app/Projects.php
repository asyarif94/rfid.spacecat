<?php

namespace App;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;


class Projects extends Model {

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });

        self::creating(function ($model) {
            $model->project_key = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id',
        'id_pembuat',
        'project_key',
        'name',
        'status',
        'image',
        'description'
    ];

    public function pembuat(){
        return $this->belongsTo(User::class, 'id_pembuat');
    }

    public function users(){
        return $this->hasMany(UserModel::class, 'id_project');
    }

    public function rooms(){
        return $this->hasMany(RoomModel::class, 'id_project');
    }

    public function parameters(){
        return $this->hasMany(UserParameters::class, 'id_projects');
    }

    public function rule(){
        return $this->belongsTo(RulesAccessModel::class, 'id_project');
    }

    public function usermodels(){
        return $this->hasMany(UserModel::class, 'id_project');
    }

    public function plan(){
        return $this->belongsTo(PlanModel::class, 'id');
    }

    public function device(){
        return $this->hasMany(Devices::class, 'id_project');
    }

    public function logs(){
        return $this->hasMany(Log_data::class, "id_project");
    }


}
