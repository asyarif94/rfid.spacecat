<?php

namespace App;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use App\RulesAccessModel;

class Rules_model extends Model{
    protected $table = 'rulesonroom';
    protected $primaryKey = 'id';
    protected $casts = ['id' => 'string' ];
    public $incrementing = false;

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id',
        'rule_name',
        'sub_name',
        'id_room',
        'status',
        'enable_schedule',
        'day',
        'date',
        'start_time',
        'end_time',
        'offset_time',
        'description'
    ];

    // public function project(){
    //     return $this->belongsTo(Room::class, 'id_room');
    // }


    public function getAccess(){
        return $this->hasMany(RulesAccessModel::class,'id_rules');
    }

    public function getListUserAttribute(){
        // $tes = RulesAccessModel::all()->pluck('id_rules');
        // return [$tes, $this->id];
        return RulesAccessModel::where('id_rules','=', $this->id)->get();
    }

    public function userParameters(){
        return $this->hasMany(UserToParameters::class, 'rule_id');
    }

    public function conectorparameter(){
        return $this->hasOne(ConnectorRuleToParameter::class, 'rule_id');
    }

    public function ruleparameters(){
        return $this->hasMany(UserToParameters::class, 'rule_id');
    }

    public function connectorPlacementParameter(){
        return $this->hasMany(ConnectorRuletoPlacementParameter::class, 'id_rule');
    }


}
