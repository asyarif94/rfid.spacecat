

<?php
use App\Projects;

    function getNamePlan(){
        $madeby = auth()->user()->plan->name;
        return $madeby;
    }

    function countPlanuser(){
        $madeby = auth()->user()->plan->total_users;
        return $madeby;
    }

    function countPlanProject(){
        $madeby = auth()->user()->plan->total_project;
        return $madeby;
    }


    function countPlanRoom(){
        $madeby = auth()->user()->plan->total_room;
        return $madeby;
    }

    function countDevice(){
        $madeby = auth()->user()->plan->total_devices;
        return $madeby;
    }

    function countRequestApiTap(){
        $madeby = auth()->user()->plan->total_request;
        return $madeby;
    }

    function countUserRoom(){
        $madeby = auth()->user();
        $room =  $madeby->projects;
        return $room;
    }


    function getTotaluserCreated($id_project){
       $get =  Projects::find($id_project);
       return $get->users->count();
    }

    function getTotalroomCreated($id_project){
        $get =  Projects::find($id_project);
        return $get->rooms->count();
    }
