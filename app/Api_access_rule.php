<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Api_access_rule extends Model{
    // Project key = username
    // secret room key = password
    protected $table = 'rules_access';
    protected $primaryKey = 'project_key';
    protected $casts = [
        'project_key' => 'string',
    ];
    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }


}
