<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class UsersLevel extends Model{
    protected $table = 'user_level';
    
    protected $primaryKey = 'id';
    protected $keyType = 'string';


    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    public function level(){
        return $this->belongsTo(User::class);
    }
}
