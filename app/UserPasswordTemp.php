<?php

namespace App;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class UserPasswordTemp extends Model
{
    //
    protected $table = 'user_password_temporary';

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id_refrence',
        'id_user',
        'secret_key_room',
        'id_project',
        'used'
    ];
}
