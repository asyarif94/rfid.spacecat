<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class ConnectorRuleToParameter extends Model{

    protected $table = "connector_rule_to_parameter";
    protected $casts = [
        'id' => 'string',
    ];
    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id_projects',
        'rule_id',
        'parameter_id',
        'id_placement',
        'min_value',
        'enable',
        'max_value',
        'by_value',
        'operator',
        'enable',
    ];

    public function parameters(){
        return $this->belongsTo(UserParameters::class, 'parameter_id');
    }

}
