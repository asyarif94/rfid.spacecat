<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Datatables, Redirect,Response,File, Validator;
Use Image;
use DB;
use App\UserModel;
use App\Projects;
use App\RoomModel;
use App\UserParameters;
use App\UserToParameters;
use App\RulesAccessModel;
use App\UsersParametersModel;
use Intervention\Image\Exception\NotReadableException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UsersController extends Controller{


    protected $getRoom = [];
    public function index(Request $request, $id_project){
        $searchData = $request->search;
        $madeby = auth()->user();
        $data['id_projects'] = $id_project;

        $data['subheader_title'] = "Users";
        $data['countUser'] = UserModel::where('id_project', $id_project)->get()->count();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();

        $project = Projects::findorFail($id_project);
        if($searchData){
            $data['users'] = UserModel::where('id_project', $id_project)
                                        ->where('name','like',"%".$searchData."%")
                                        ->orWhere('email', 'like',"%".$searchData."%")
                                        ->paginate(10);
        }else{
            $data['users'] = $project->users;
        }
        $data['page_title'] = $project->name;
        return view('user_pages.users_list', $data);
    }

    public function adduser($id){

        $data['page_title'] = "Add User";
        $data['id_projects'] = $id;
        $madeby = auth()->user();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        return view('user_pages.add_user', $data);
    }

    public function create(){
        //
    }


    public function store(Request $request){

        $id_project = $request->id;

            if($request->status == null){
                $_status = '0';
            }else{
                $_status = '1';
            }

            $id_project = $request->id;

            $validateData = $request->validate([
                'name'           => 'required|string|max:255',
                'profile_avatar' => 'image|mimes:jpeg,png,jpg|max:1024',
                'email'     =>  'nullable|email',
                'phonenr'   => 'nullable|numeric|min:10',
                'rfid'      => [
                                'required',
                                'min:40',
                                Rule::unique('usersdata', 'rfid')
                                    ->where('id_project', $id_project)
                               ],
                'description'   => 'string|max:255|nullable',
            ]);

            if($files = $request->file('profile_avatar')){
                $ImageUpload    = Image::make($files);
                $originalPath   = public_path('/media/users/');
                $ImageUpload->save($originalPath.time().$files->getClientOriginalName());
                $thumbnailPath = public_path('/media/users/thumbnail/');
                $ImageUpload->resize(250,125);
                $ImageUpload = $ImageUpload->save($thumbnailPath.time().$files->getClientOriginalName());
                $Imagefilename = time().$files->getClientOriginalName();
            }else{
                $Imagefilename = "default.jpg";
            }

            $validateData = array_merge($validateData, [
                'id_project'    => $id_project,
                'avatar'        => $Imagefilename,
                'status'        => $_status,
            ]);


            $user = UserModel::create($validateData);
            return Response()->json(['success' => 'success insert data'], 200);


    }

    //menampilkan detail user + edit user.
    public function show($id_project, $id_user){

        $data['id_projects'] = $id_project;
        $madeby = auth()->user();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "User";
        $data['datauser'] = UserModel::find($id_user);

        //test get user room
        $data['hasroom'] = RulesAccessModel::where('id_user', $id_user)->groupBy('id_room')->get();
        $getHaveRoom =  $data['hasroom']->pluck("id_room")->toArray();
        // dd($getHaveRoom);
        $hasil= [];
        foreach($getHaveRoom as $index => $item){
            $hasil[$index] = RoomModel::where("id", $item)->first();
        }
        $data['rooms'] = $hasil;
        $getParam = $data['datauser']->parameters;
        $data['param'] = $getParam;
        $data['userParam'] = UserParameters::get();
        $data['id_user'] = $id_user;
        return view('user_pages.show_user', $data);
    }

    public function logparameters($id_project, $id_user, $id_parameter){
        $data['page_title'] = "User Log Parameter";
        $data['id_projects'] = $id_project;
        $madeby = auth()->user();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        $data['datauser'] = UserModel::find($id_user);
        return view('user_pages.log_user_parameter', $data);
    }


    public function edit($id){
        $data['page_title'] = "Edit User";
        $data['subheader_title'] = "Edit User";
        $data['id'] = $id;
        $data['users'] = UserModel::find($id);

        $madeby = auth()->user();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        return view('user_pages.edit_user', $data);
    }

    public function updatepass($id_project, $id_user, Request $request){
        // dd($request);
       if($request->passowrd != null){
            $request->validate([
                'passowrd' => 'required|min:6|numeric|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'min:6|numeric'
            ]);

            $updatePass = [
                // 'password' => sha1("ASYARIF".$request->passowrd),
                'password' => sha1($request->passowrd),
            ];

        UserModel::where('id', $id_user)->update($updatePass);

       }else{
        $updatePass = [
            'password' => null,
        ];
        UserModel::where('id', $id_user)->update($updatePass);
       }

       return Redirect::back()->with('success', 'User Password Successfully Updated');
    }

    public function updatebasicinfo($id_project, $id_user, Request $request){

        $lastdata = UserModel::find($id_user);

        $request->validate([
            'name' => 'required',
            'email' => 'nullable|email',
            'profile_avatar'  => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
            'phonenr' => 'nullable|numeric|min:10',
            'rfid'      => 'required|min:40',
        ]);

        if($files = $request->file('profile_avatar') ){
            $ImageUpload    = Image::make($files);
            $originalPath   = public_path('/media/users/');
            $ImageUpload->save($originalPath.time().$files->getClientOriginalName());

            $thumbnailPath = public_path('/media/users/thumbnail/');
            $ImageUpload->resize(250,125);
            $ImageUpload = $ImageUpload->save($thumbnailPath.time().$files->getClientOriginalName());
            $Imagefilename = time().$files->getClientOriginalName();
        }else{

            //if images not updated, and save last image name
            $Imagefilename = $lastdata->avatar;
        }


        $update = [
            'name'      => $request->name,
            'phonenr'   =>  $request->phonenr,
            'email'     => $request->email,
            'status'    => $request->status,
            'avatar'    => $Imagefilename,
            'rfid'      => $request->rfid,
            'description'  => $request->description
        ];

        UserModel::where('id', $id_user)->update($update);
        return Redirect::back()->with('success', 'User Profile Successfully Updated');
    }


    public function update(Request $request, $id){

        $data = UserModel::findOrFail($id);
        $data->update($request->all());
        return redirect()->route('user.index/', $request->id_project)->with('success', 'User is successfully added.');
    }

    public function destroy(UserModel $user){
        // dd($user);
        $user->delete();
        // return Redirect::back()->with('success', 'User has been deleted!');
        return redirect()->route('user.list', $user->id_project)->with('success', 'User has been deleted!');
    }

}
