<?php

namespace App\Http\Controllers;

use App\UserToParameters;
use Illuminate\Http\Request;

class UsertoParametersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserToParameters  $userToParameters
     * @return \Illuminate\Http\Response
     */
    public function show(UserToParameters $userToParameters)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserToParameters  $userToParameters
     * @return \Illuminate\Http\Response
     */
    public function edit(UserToParameters $userToParameters)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserToParameters  $userToParameters
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserToParameters $userToParameters)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserToParameters  $userToParameters
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserToParameters $userToParameters)
    {
        //
    }
}
