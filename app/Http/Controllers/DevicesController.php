<?php

namespace App\Http\Controllers;

use App\Devices;
use Illuminate\Http\Request;
use App\Projects; //Model Projects
use App\PlanModel;
use App\User;
use App\RoomModel;
use DB;

class DevicesController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_project){

        $data['id_projects'] = $id_project;
        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "Dashboard";

        $madeby = auth()->user();
        $data['projects_']  = Projects::where('id_pembuat', $madeby->id)->get();

        $data['project'] = Projects::findOrFail($id_project);
        $getProjectKey      = Projects::where('id', $id_project)->first();
        $getData =  Devices::where('project_key', $getProjectKey->project_key)->orderBy('created_at', 'desc')->groupBy('mac_address')->get();
        $data['devices'] = $getData->map(function ($item, $key){
            return $item;
        });



                                    //    ->orderBy('updated_at', 'desc')
                                    //    ->latest('updated_at')
                                    //    ->get();

        $data['count_device'] = Devices::where('project_key', $getProjectKey->project_key)->count();

        return view('devices_pages.devices_list', $data);
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($secret_key_room, Request $request){
        $header = $request->header('SECRET_KEY');
        $room = RoomModel::where('secret_key', $secret_key_room)->first();
        $data   = Devices::where('mac_address', '=', $request->mac_address)->latest()->first();
        $data_ = Devices::create([
                    'device_name'       => $request->device_name,
                    'type_devices'      => $request->type_devices,
                    'id_room'           => $room->id,
                    'id_project'        => $room->id_project,
                    'firmware_version'  => $request->firmware_version,
                    'ssid'              => $request->ssid,
                    'wifi_strength'     => $request->wifi_strength,
                    'secret_key_room'   => $secret_key_room,
                    'project_key'       => $header,
                    'mac_address'       => $request->mac_address,
                    'local_ip_address'  => $request->local_ip_address,
                    'esp_chip_id'       => $request->esp_chip_id,
                    'core_version'      => $request->core_version,
                    'sdk_version'       => $request->sdk_version,
                    'boot_version'      => $request->boot_version,
                    'boot_mode'         => $request->boot_mode,
                    'uptime'            => $request->uptime,
                    'rc522_chip_version'=> $request->rc522_chip_version
                ]);
                return response()->json($data_, 201);
        // if($data->count() > 0){
        //     $update = [
        //         'device_name'       => $request->device_name,
        //         'id_room'           => $room->id,
        //         'id_project'        => $room->id_project,
        //         'type_devices'      => $request->type_devices,
        //         'firmware_version'  => $request->firmware_version,
        //         'ssid'              => $request->ssid,
        //         'wifi_strength'     => $request->wifi_strength,
        //         'secret_key_room'   => $secret_key_room,
        //         'project_key'       => $header,
        //         'mac_address'       => $request->mac_address,
        //         'local_ip_address'  => $request->local_ip_address,
        //         'esp_chip_id'       => $request->esp_chip_id,
        //         'core_version'      => $request->core_version,
        //         'sdk_version'       => $request->sdk_version,
        //         'boot_version'      => $request->boot_version,
        //         'boot_mode'         => $request->boot_mode,
        //         'uptime'            => $request->uptime,
        //         'rc522_chip_version'=> $request->rc522_chip_version
        //     ];
        //     $data->update($update);
        //     return response()->json($data, 201);
        // }else{// jika data tidak ada
        //     $data_ = Devices::create([
        //         'device_name'       => $request->device_name,
        //         'type_devices'      => $request->type_devices,
        //         'id_room'           => $room->id,
        //         'id_project'        => $room->id_project,
        //         'firmware_version'  => $request->firmware_version,
        //         'ssid'              => $request->ssid,
        //         'wifi_strength'     => $request->wifi_strength,
        //         'secret_key_room'   => $secret_key_room,
        //         'project_key'       => $header,
        //         'mac_address'       => $request->mac_address,
        //         'local_ip_address'  => $request->local_ip_address,
        //         'esp_chip_id'       => $request->esp_chip_id,
        //         'core_version'      => $request->core_version,
        //         'sdk_version'       => $request->sdk_version,
        //         'boot_version'      => $request->boot_version,
        //         'boot_mode'         => $request->boot_mode,
        //         'uptime'            => $request->uptime,
        //         'rc522_chip_version'=> $request->rc522_chip_version
        //     ]);
        //     return response()->json($data_, 201);

        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function show(devices $devices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function edit(devices $devices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, devices $devices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function destroy(devices $devices){

    //   dd($devices);
    //     $devices->delete();
    //    return redirect('/dashboard')->with('success', 'Project has been deleted!');
    }
}
