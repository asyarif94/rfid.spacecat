<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Api_access_rule;
use App\RoomModel;
use App\UserModel;
use App\Projects;
use Carbon\Carbon;
use App\Log_data;
use App\Devices;
use App\Rules_model;
use App\ConnectorRuleToParameter;
use App\UserToParameters;
use App\UserParameters;
use App\LogUserParameter;
use App\UserPasswordTemp;
use DB;

class ApidevicesController extends Controller{

    public function __construct(){


    }

    public function index(){
        //
    }

    private function getUserdata($rfid){
        return UserModel::where('rfid', $rfid)->first();
    }

    private function getRoomdata($secret_key_room){
        return RoomModel::where('secret_key', $secret_key_room)->first();
    }


    private function cekRoomStatus($secret_key_room){
        $getRoom = RoomModel::where('secret_key', $secret_key_room)->first();
        if($getRoom){
            return array(
                'id_room'=> $getRoom->id,
                'status' => $getRoom->status
            );
        }
        return false;
    }



    private function cekProjectStatus($project_key){
        $getProject = Projects::where('project_key', $project_key)->first();
        return [
            'id_project'    => $getProject->id,
            'status' => $getProject->status
        ];
    }


    //cek user status
    private function cekUserStatus($id_user){
       $data =  UserModel::where('id', $id_user)->first();
        if($data){
            return array(
                'name'      => $data->name,
                'status'    => $data->status,
                'id_user'   => $data->id,
                'have_pass' => $data->password
            );
        }
    }

    private function cekHakAksesKeRuangan($rfid, $secret_key_room){
        $data = Api_access_rule::where('secret_key','=', $secret_key_room)->where('rfid_user', $rfid)->first();
        // echo $data;
        if($data){
            return array(
                'id_user'   => $data->id_user,
                'id_rule'   =>  $data->id_rules,
            );
        }else{
            return null;
        }
    }

    private function generateIDuserDoing(){
        $dateTime = Carbon::now()->format('HmsY');
        // echo $dateTime;
        $randomid = mt_rand(100000,999999);
        $data =  $randomid + $dateTime;
        return  (string) $data;
    }



    private function createTempForPassword($id_reference, $id_user, $secret_key_room, $id_project){
        $createTempPass = UserPasswordTemp::create([
            'id_refrence'   => $id_reference,
            'id_user'       => $id_user,
            'id_project'    => $id_project,
            'secret_key_room'   => $secret_key_room,
        ]);
    }



    private function filterDateTime($id_user, $secret_key_room){
        // return Rules_model::where('id_room', '=', $id_room)->whereNotNull('start_time')->orderBy('start_time', 'asc')->get();
        $room = $this->getRoomdata($secret_key_room);
        if($room){
            $nowDay     = Carbon::now()->format('l');
            // $roomRules  = $this->getRulesonRooom($room->id, $nowDay);
            $cekisEveryday = Rules_model::where('id_room', '=', $room->id)->where('day', '=', 'everyday')->first();
            if($cekisEveryday){
                if($cekisEveryday->status == 1){ // jika status rule aktif
                    if($cekisEveryday != null){
                        $nowTime    =  Carbon::now()->format('H:i');
                        if($cekisEveryday->start_time != null && $cekisEveryday->end_time != null){
                            if ($nowTime >= $cekisEveryday->start_time && $nowTime <= $cekisEveryday->end_time) {
                                return ['go_in_with_time' => $cekisEveryday];
                            }
                        }else{
                            return ['go_in_without_time' => $cekisEveryday];
                        }
                    }else{
                        $get =  Rules_model::where('id_room', '=', $room->id)
                            ->where('day', '=', $nowDay)
                            ->orderBy('start_time', 'asc')
                            ->get()->mapToGroups(function ($item, $key) {
                                $nowTime    =  Carbon::now()->format('H:i');
                                if ($nowTime >= $item->start_time && $nowTime <= $item->end_time) {
                                    return ['in' => $item];
                                } else {
                                    return ['not' => $item];
                                }
                            });
                        echo $get;
                        if($get != null && $get->status == 1){
                            return $get;
                        }
                        return null;
                    }
                }
            }
        }else{
            return null;
        }
    //pertama cek API pada Header
    //cek USER RFID dan SECRET KEY Ruangan
    //cek Project Aktif / tidak
    //cek User Aktif / tidak
    //cek Ruangan Aktfi / tidak
    //lalu schedule jika akrif
    //lalu amount jika aktif

    }

    public function doUserParameter($id_rules, $id_user){
        //pertama kita cari data dari connector rule parameter
        //lalu
        //cari data dari user yang bersangkutan dari table user to parameters
        $getRule =  ConnectorRuleToParameter::where('rule_id', '=' ,$id_rules)->first();
        if($getRule){
            $getUserParam = UserToParameters::where('user_model_id', $id_user)->where('rule_id', $getRule->rule_id)->sharedLock()->first();
            return array(
                'id_parameter'  =>  $getUserParam->id,
                'by_value'      =>  $getRule->by_value,
                'min_value'     =>  $getRule->min_value,
                'max_value'     =>  $getRule->max_value,
                'operator'      =>  $getRule->operator,
                'user_value'    =>  $getUserParam->values,
            );
        }

    }


    /*
        fungsi ini mereturn boolean
        jika true, proses berhasil
        jika false, proses gagal
    */
    public function doCalculateUserParameter($param, $userlog){

        $getParam = ConnectorRuleToParameter::where('rule_id', $userlog['id_rule'])->get();
        //peratama cek dulu, jika Rule mempunyai User Parameter, maka akan diproses, Jika tidak langsuns sukses
       if($getParam->isEmpty()){
            return true;
       }else{
        DB::beginTransaction();
        $result = 0;
        try {
            $id_parameter = $param['id_parameter'];
            $setOperator = $param['operator'];
            $setbyValue  = $param['by_value'];
            $set_min_value  = $param['min_value'];
            $set_max_value  = $param['max_value'];
            $set_user_value  = $param['user_value'];

            if($setOperator == "add"){
            $result = $set_user_value + $setbyValue;
            }elseif($setOperator ==  "sub"){
                $result = $set_user_value - $setbyValue;
            }elseif($setOperator ==  "perctpls"){
                $raw = ($set_user_value/100) * $setbyValue;
                $result = $raw + $set_user_value;
            }elseif($setOperator ==  "perctmin"){
                $raw = ($set_user_value/100) * $setbyValue;
                $result =  $set_user_value - $raw;

            }elseif($setOperator ==  "replc"){
                $result = $setbyValue;
            }

            if($result >= $set_min_value && $result <= $set_max_value){
                LogUserParameter::Create([
                    'user_id'       =>  $userlog['id_user'],
                    'parameter_id'  =>  $id_parameter,
                    'last_value'    =>  $set_user_value,
                    'new_value'     =>  $result,
                    'operator'      =>  $setOperator
                ]);
                UserToParameters::where('id', $id_parameter)->update(array(
                    'values' => $result
                ));

                $this->createlogData($userlog['userdoing'], $userlog['id_rule'], $userlog['id_user'], $userlog['id_room'], $userlog['mac_address'] , $userlog['ip_address'], $userlog['wifi_ssid'], $userlog['status'], $userlog['user'] , "");
            }else{
                //user parameter false
                $this->createlogData($userlog['userdoing'], $userlog['id_rule'], $userlog['id_user'], $userlog['id_room'], $userlog['mac_address'] , $userlog['ip_address'], $userlog['wifi_ssid'], "Unsuccessful", "User reach the limit parameters" , "");
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            // echo $e;
            return false;
        }
       }


    }

    /*

    pertama cek secret key dan project key, ada atau tidak
    jika ada lalu cek ruangan -> rule  -> in range time atau tidak-> user terdaftar atau tidak

    untuk rules, dipastikan tidak ada waktu yang bentrok di hari yang sama
    e.g 10:00 sd 12:00  -> 11:001 sd 13:00   SALAH !


    Jika ada 2 rules , user yang sama.

    */
    public function proccess($secret_key_room, $rfid, $mac_address, $ip_address, $wifi_ssid, Request $request){

        // return response()->json([
        //     'code'      => '103',
        //     'name'      => 'arif',
        //     'password'  => '0',
        //     'phone'     => 'null',
        //     'messages'  => 'bbbb'
        // ], 200);

        /*
         USER DOING
         1 = Tap Card
         2 = Insert Password

         CALLBACK
        1 = SUCCESS
        2 = FAILED
        3 = PENDING
        */

        $code_response_success = 104;
        $code_response_waiting_pass = 107;
        $code_response_not_in_rangetime = 105;
        $code_response_user_not_active = 102;
        $code_response_placement_not_active = 103;
        $code_response_project_not_active = 101;
        $code_response_param_limit = 108;


        $getIDUserDoing = $this->generateIDuserDoing();
        $cekHakAkses = $this->cekHakAksesKeRuangan($rfid, $secret_key_room);
        $cekUser = $this->cekUserStatus($this->cekHakAksesKeRuangan($rfid, $secret_key_room));
        $cekStatusRoom = $this->cekRoomStatus($secret_key_room);
        $project_key = $request->header('SECRET-KEY');
        $cekStatusProject = $this->cekProjectStatus($project_key);
        $doUserParam = $this->doUserParameter($cekHakAkses['id_rule'], $cekHakAkses['id_user']);
        $cekRuleWaktu = $this->filterDateTime($cekHakAkses['id_user'], $secret_key_room);
        $getDevice  = Devices::where('mac_address', '=', $mac_address)->first();
            // dd($doUserParam['user_value']);

        //variable ini untuk keperlua loggin data
        // echo $cekRuleWaktu['in'];
        //user terdaftar dalam ruangan
        if($getDevice){
            if($cekHakAkses){
                //cek status project
                if($cekStatusProject['status'] == 1){
                    //cek status ruangan
                    if($cekStatusRoom['status']){
                        if($cekUser['status']){
                            if(isset($cekRuleWaktu['in'])){
                                if($cekUser['have_pass'] != NULL){// menunggu user memasukan password
                                    $this->createlogData($getIDUserDoing, $cekRuleWaktu['in'][0]->id, $cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 3 , "");
                                    return response()->json([
                                        'code'      => $code_response_success,
                                        'reference' => $getIDUserDoing,
                                        'name'      => $cekUser['name'],
                                        'messages'  => 'pass'
                                    ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                                }else{ // user barhasil tapin
                                    $this->createlogData($getIDUserDoing, $cekRuleWaktu['in'][0]->id, $cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 1 , "");

                                    return response()->json([
                                        'code'      => $code_response_success,
                                        'reference' => $getIDUserDoing,
                                        'name'      => $cekUser['name'],
                                        'messages'  => 'success'
                                    ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                                }
                            }else if(isset($cekRuleWaktu['go_in_without_time'])){ // success & jika schedule tanpa timer
                                    if($cekUser['have_pass'] != NULL){
                                        DB::beginTransaction();
                                        try {
                                                $this->createlogData($getIDUserDoing, $cekRuleWaktu['go_in_without_time']->id, $cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 3 , "");
                                                $this->createTempForPassword($getIDUserDoing, $cekUser['id_user'], $secret_key_room,  $cekStatusProject['id_project']);
                                                DB::commit();
                                            return response()->json([
                                                'code'      => $code_response_waiting_pass,
                                                'reference' => $getIDUserDoing,
                                                'name'      => $cekUser['name'],
                                                'messages'  => 'Waiting Password'
                                            ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                                        }catch (\Exception $e){
                                            DB::rollback();
                                            // something went wrong
                                            // echo $e;
                                        }
                                    }else{ // user sukses tapin
                                        $userlog = array(
                                            'userdoing'     => $getIDUserDoing,
                                            'id_rule'       => $cekRuleWaktu['go_in_without_time']->id,
                                            'id_user'       => $cekUser['id_user'],
                                            'id_room'       => $cekStatusRoom['id_room'],
                                            'mac_address'   => $mac_address,
                                            'ip_address'    => $ip_address,
                                            'wifi_ssid'     => $wifi_ssid,
                                            'status'        => 1,
                                            'user'          => "1"
                                        );
                                        //jika semua proses berhasil, maka true
                                        if($this->doCalculateUserParameter($doUserParam, $userlog)){
                                            // $this->createlogData($getIDUserDoing, $cekRuleWaktu['go_in_without_time']->id, $cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 1 , "");
                                            return response()->json([
                                                'code'      => $code_response_success,
                                                'reference' => $getIDUserDoing,
                                                'name'      => $cekUser['name'],
                                                'messages'  => 'success'
                                            ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                                        }else{ //jika kalkulasi parameter tidak berhasil
                                            // $this->createlogData($getIDUserDoing, $cekRuleWaktu['go_in_without_time']->id, $cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 2 , "User reach the limit");
                                            return response()->json([
                                                'code'      => $code_response_param_limit,
                                                'reference' => $getIDUserDoing,
                                                'name'      => $cekUser['name'],
                                                'messages'  => 'user reach the limit'
                                            ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                                        }
                                    }

                            }else if(isset($cekRuleWaktu['go_in_with_time'])) { // sucess & jika schedule dengan timer
                                if($cekUser['have_pass'] != NULL){
                                    DB::beginTransaction();
                                        try {
                                                $this->createlogData($getIDUserDoing, $cekRuleWaktu['go_in_without_time']->id, $cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 3 , "");
                                                $this->createTempForPassword($getIDUserDoing, $cekUser['id_user'], $secret_key_room,  $cekStatusProject['id_project']);
                                                DB::commit();
                                            return response()->json([
                                                'code'      => $code_response_waiting_pass,
                                                'reference' => $getIDUserDoing,
                                                'name'      => $cekUser['name'],
                                                'messages'  => 'Waiting Password'
                                            ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                                        }catch (\Exception $e){
                                            // DB::rollback();
                                            // something went wrong
                                            // echo $e;
                                            return response()->json([
                                                'messages' => 'error no data'
                                            ], 404);
                                        }
                                }else{
                                    $this->createlogData($getIDUserDoing, $cekRuleWaktu['go_in_with_time']->id, $cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 1 , "");
                                    return response()->json([
                                        'code'      => $code_response_success,
                                        'reference' => $getIDUserDoing,
                                        'name'      => $cekUser['name'],
                                        'messages'  => 'success'
                                    ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                                }
                            }else{  //jika tidak dalam rentang waktu schedule
                                $this->createlogData($getIDUserDoing, null, $cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 2 , "Not in range time");
                                return response()->json([
                                    'code'      => $code_response_not_in_rangetime,
                                    'reference' => $getIDUserDoing,
                                    'name'      => null,
                                    'messages'  => 'fail'
                                ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                            }
                        }else{ //user not active
                            $this->createlogData($getIDUserDoing, null, $cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 2 , "User is not active");
                            return response()->json([
                                'code'      => $code_response_user_not_active,
                                'reference' => $getIDUserDoing,
                                'name'      => null,
                                'messages'  => 'User is not active'
                            ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                        }
                    }else{ // placement not active
                        $this->createlogData($getIDUserDoing, null, $cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 2 , "Room is not active");
                        return response()->json([
                            'code'      => $code_response_placement_not_active,
                            'reference' => $getIDUserDoing,
                            'name'      => null,
                            'messages'  => 'Placement is not active'
                        ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                    }
                }else{ // project not active
                    $this->createlogData($getIDUserDoing, null,$cekUser['id_user'], $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 2 , "Project is not active");
                    return response()->json([
                        'code'      => $code_response_project_not_active,
                        'reference' => $getIDUserDoing,
                        'name'      => null,
                        'messages'  => 'Project is not active'
                    ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                }
                }else{ // data tidak ditemukan
                    //user tidak terdaftar pada ruangan
                    if($cekStatusRoom['id_room'] != NULL){
                        $this->createlogData($getIDUserDoing, null, null, $cekStatusRoom['id_room'], $mac_address , $ip_address, $wifi_ssid, 1, 2 , "User not found");
                    }
                    return response()->json([
                        'messages' => 'error no data'
                    ], 200);
                }
        }
        return response()->json([
            'messages' => 'error no data'
        ], 404);
    }


    //validasi password
    public function proccess_password($secret_key_room, Request $request){
        //SALT = ASYARIF
        $header = $request->header('SECRET_KEY');
        $getData = Api_access_rule::where('secret_key','=', $secret_key_room)->where('rfid_user', $request->rfid)->first();
        $getUserData = UserModel::where('id', '=', $getData->id_user)->first();

        if($getUserData){
            if($getUserData->password === $request->pwd){
                $this->createlogData($request->traceid, null, $getData->id_user, $getData->id_room, $request->mac_address , $request->local_ip_address, $request->ssid, "1", "1" , "User Found");
                return response()->json([
                    'code'      => '104',
                    'reference' => $request->traceid,
                    'name'      => $getUserData->name,
                    'messages'  => 'success'
                ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
            }else{

                $this->createlogData($request->traceid, null, $getData->id_user, $getData->id_room, $request->mac_address , $request->local_ip_address, $request->ssid, "2", "2" , "Invalid Password");
                return response()->json([
                    'code'      => '106',
                    'reference' => $request->traceid,
                    'name'      => null,
                    'messages'  => 'Invalid Keys'
                ], 404)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
            }
        }

        // echo $getUserData->password;
        // $hased = sha1("ASYARIF".$password);
        // echo $hased;
        // echo "\n";

    }

    private function createlogData($id_refrence_user_doing, $id_rules, $id_user, $id_room, $mac_address, $ip_address, $wifi_ssid,  $user_doing, $callback, $note_schedule){

        /*
         USER DOING
         1 = Tap Card
         2 = Insert Password

         CALLBACK
        1 = SUCCESS
        2 = FAILED
        3 = Waiting Insert Password
        */

        $getRoom    = RoomModel::find($id_room);
        $getProject = Projects::where('id', $getRoom->id_project)->first();

        $getUser    = UserModel::find($id_user);
        $getDevice  = Devices::where('mac_address', '=', $mac_address)->first();

        $getRules   = Rules_model::where('id', '=', $id_rules)->first();



        if($getUser != null){
            $userid = $getUser->id;
            $username = $getUser->name;
        }else{
            $userid = null;
            $username = null;
        }


        if($id_rules != null){
           $rulesid  =  $getRules->id;
           $rulesname =  $getRules->rule_name;
        }else{
            $rulesid  =  null;
            $rulesname =  null;
        }

        $log = new Log_data();
        $log->id_reference_user_doing = $id_refrence_user_doing;
        $log->id_project        = $getRoom->id_project;
        $log->old_project_name  = $getProject->name;
        $log->id_room           = $getRoom->id;
        $log->old_room_name     = $getRoom->name;
        $log->id_device         = $getDevice->id;
        $log->old_device_name   = $getDevice->device_name;
        $log->mac_address       = $mac_address;
        $log->ip_address        = $ip_address;
        $log->wifi_ssid         = $wifi_ssid;
        $log->id_user           = $userid;
        $log->old_user_name     = $username;
        $log->id_rules          = $rulesid;
        $log->old_rule_name     = $rulesname;
        $log->user_doing        = $user_doing;
        $log->callback          = $callback;
        $log->note_schedule     = $note_schedule;
        $log->save();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(){



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
