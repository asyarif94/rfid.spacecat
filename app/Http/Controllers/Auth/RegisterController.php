<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\NotifyUser;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data){

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data){

        $userRegister = $this->createUser([
            'name'          => $data['name'],
            'email'         => $data['email'],
            'id_plan'       => env('PLAN_ID'),
            'id_level'      => env('LEVEL_ID'),
            'proficiency'   => "member",
            'password'      => Hash::make($data['password']),
            'token'         => $this->quickRandom(),
        ]);
        if($userRegister){
            $userRegister->notify(new NotifyUser($userRegister->token));
            session()->flash('message', 'An email verification has sent to '. $data['email']);
            return $userRegister;
        }
      return false;
    }


    public function createUser($params){
        $data = User::create($params);
        return $data;
    }

    public static function quickRandom($length = 100){
        $timeNow = Carbon::now()->format('dmYx');
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length).sha1($timeNow);
    }


    public function verifyEmail($token){

       $getUser = User::where('token', '=', $token)->first();
       if($getUser){
         $updating =  User::where('token', '=', $token)->update([
            'status' => '1',
            'token' => NULL,
            'email_verified_at' => Carbon::now()->toDateTimeString()
        ]);

        if($updating){
            if($getUser->provider == NULL){
                 session()->flash('message', 'Your email address has been verified, try to login.');
                return redirect('login')->with('success', 'Your email address has been verified, try to login.');
            }else{
                session()->flash('message', 'Your email address has been verified, try to login with Github.');
                return redirect('login')->with('success', 'Your email address has been verified, try to login with Github.');
            }
        }else{
            session()->flash('message', 'Error, Something wrong');
            return redirect('login')->with('error', 'Error, Something wrong');
        }
       }else{
            session()->flash('message', 'Error, Something wrong');
            return redirect('login')->with('error', 'Error, Something wrong');
       }
    }



}
