<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\User;
use Storage;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Image;
use App\Notifications\NotifyUser;
use Carbon\Carbon;

class ProviderUser extends Controller{
    //
    // protected $redirectTo = '/dashboard';
    protected $redirectTo = '/login';

    public static function quickRandom($length = 100){
        $timeNow = Carbon::now()->format('dmYx');
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length).sha1($timeNow);
    }

    public function create(Request $request){
        // dd($request);
        $path = $request->avatar;
        $content = file_get_contents($path);
        $name = substr($path,strrpos($path, '/') + 1);
        $user = User::create([
            'email'         => $request->email,
            'name'          => $request->name,
            'provider_id'   => $request->id_provider,
            'provider'      => 'github',
            'status'        => '1',
            'id_plan'       => env('PLAN_ID'),
            'id_level'      => env('LEVEL_ID'),
            'avatar'        =>$name,
            'token'         => $this->quickRandom(),
        ]);

        if($user){
            $user->notify(new NotifyUser($user->token));
            // Storage::disk('public')->put($name, $content);
            // Auth::login($user, true);
            session()->flash('message', 'An email verification has sent to '. $user['email']);
            return redirect($this->redirectTo);
        }
    }

    public function resendemail($id){
        $user = User::find($id)->first();
        if($user){
            $user->notify(new NotifyUser($user->token));
            return response()->json(['success'=>'Success.']);
        }
    }

}
