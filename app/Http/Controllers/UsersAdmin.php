<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use App\Projects;
use App\User;
use Redirect;

class UsersAdmin extends Controller{

    //function for showing users
    public function index(){
        $madeby = auth()->user();
        $data['projects_'] = $madeby->Projects();

        return view('user_admin.settings', $data);
    }

    public function chanagePassword(Request $request){

        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        return Redirect::back()->with('success', 'Password changed successfully');
    }

}
