<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserPasswordTemp;
use App\UserModel;
use DB;

class PasswordTemp extends Controller{


    public function clearSession($secret_key_room, $id_reference){
        $update = UserPasswordTemp::where('secret_key_room', $secret_key_room)
            ->where('used', 0)
            ->where('id_refrence', $id_reference)
            ->update(['used' => '1']);

            return true;
    }

    public function cekpassword($secret_key_room, Request $request){

        DB::beginTransaction();
        try {
        $getPwd = UserPasswordTemp::where('secret_key_room', $secret_key_room)
                                    ->where('used', 0)
                                    ->where('id_refrence', $request->refid)->first();
            if($getPwd){
                $getUser = UserModel::find($getPwd->id_user);
                if($getUser->password === $request->userpwd){
                    $this->clearSession($secret_key_room, $request->refid);
                    DB::commit();
                    return response()->json([
                        'code'      => '104',
                        'reference' => $request->refid,
                        'name'      => $getUser->name,
                        'messages'  => 'success'
                    ], 200)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');

                }else{
                    if($this->clearSession($secret_key_room, $request->refid)){
                        DB::commit();
                        return response()->json([
                            'code'      => '106',
                            'reference' => $request->refid,
                            'name'      => null,
                            'messages'  => 'Invalid Keys'
                        ], 404);
                    }
                }
            }else{
                if($this->clearSession($secret_key_room, $request->refid)){
                    DB::commit();
                    return response()->json([
                        'code'      => '106',
                        'reference' => $request->refid,
                        'name'      => null,
                        'messages'  => 'no data found'
                    ], 404)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');
                }
            }
        // echo $getUser;

        }catch (\Exception $e){
            // something went wrong
            echo $e;
            DB::rollback();
            return response()->json([
                'code'      => '106',
                'reference' => $request->refid,
                'name'      => null,
                'messages'  => 'no data found'
            ], 404)->header('Content-Type', 'json')->header('X-RETURN', 'Header Value');


        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
