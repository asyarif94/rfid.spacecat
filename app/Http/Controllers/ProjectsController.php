<?php

namespace App\Http\Controllers;
use App\Projects;
use App\UserModel;
use App\RoomModel;
use App\RulesAccessModel;
use App\PlanModel;
use App\Devices;
use App\User;
use App\Log_data;
use App\Helpers\Counterplan;
use App\Notifications\NotifyUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller{

    public function __construct(){
        $this->middleware(['auth','verified']);

    }

     //generating Uniqe number for Secret Key
     public static function quickRandom($length = 16){
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    public function index(){

        // User::find('25e02606-9936-48fb-b769-798af629b149')->notify(new NotifyUser());

        // return "ok";
        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "Dashboard";
        $madeby = auth()->user();
        $data['projects_'] = $madeby->projects();

        $data['name_logedin'] = $madeby->name;
        $data['project'] = $madeby->projects()->orderBy('created_at', 'DESC')->get();
        $data['name_plan'] = auth()->user()->plan->name;
        return view('dashboard', $data);

    }


    public function create(){
        $data['page_title'] = "Add New Projects";
        $data['subheader_title'] = "Add Project";
        $madeby = auth()->user();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        return view('addprojects', $data);
    }

    public function store(Request $request){

        if(is_null($request->status)){
            $_status = 0;
        }else{
            $_status = 1;
        };
        // dd($request);
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'image' => 'nullable',
            'description' => 'required|min:10',
        ]);

        $madeby = auth()->user();
        $countProject = Projects::where('id_pembuat',$madeby->id)->get()->count();

        $validatedData = array_merge($validatedData, [
            'status'        => $_status,
            'id_pembuat' => $madeby->id
        ]);

        $data = Projects::create($validatedData);
        return redirect('/dashboard')->with('success','Project created successfully!');
        // return redirect('/dashboard')->with('success', 'Project is successfully created');
    }

    public function show($id){
        //belum beres

        $log = Projects::find($id)->logs()->get()->sortByDesc('created_at')->groupBy(function($item){
            return Carbon::parse($item->created_at)->format('h:i');
        });

        // dd($log);
        $result = [];
        foreach ($log as $jam=>$v) {
            // dd($k);
            // $result[$jam] = [];
            // dd($v[0]);
            foreach ($v as $k2 => $v2) {
                // dd($v2->user_doing);
                $result[$jam][$v2->user_doing][$v2->callback][] = $v2;
            }
        }

        // dd($result);
        $data['logs'] = $log;
        $madeby = auth()->user();
        $data['id_projects'] = $id;
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        $data['page_title']         =   "Nama Projects";
        $data['subheader_title']    =   "Nama Project";
        $data['dataprojects']       =   Projects::find($id);
        $data["activity"] = $result;

        return view('showprojects', $data);

    }

    public function edit($id_project){
        $data['id_projects'] = $id_project;
        $data['page_title'] = "Edit Projects";
        $data['subheader_title'] = "Add Project";
        $data['project'] = Projects::findOrFail($id_project);
        $madeby = auth()->user();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        return view('editprojects', $data);
    }

    public function update(Request $request, $id){

        $validatedData = $request->validate([
            'name'          => 'required|string|max:255',
            'description'   => 'string|max:255|nullable',
        ]);

        if(is_null($request->status)){
            $_status = 0;
        }else{
            $_status = 1;
        };

        Projects::where('id', $id)->update(array(
            'name'          => $request->name,
            'status'        => $_status,
            'description'   => $request->description
        ));

        return redirect('/dashboard')->with('success', 'Project is successfully updated');
    }

    public function destroy(Projects $project){
       $project->delete();
       return redirect('/dashboard')->with('success', 'Project has been deleted!');
    }
}
