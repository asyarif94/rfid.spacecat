<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Datatables, Redirect,Response,File, Validator, DB;
use App\Projects;
use App\RulesAccessModel;
use App\Rules_model;
use App\RoomModel;
use App\UserModel;
use App\ScheduleModel;
use App\UserToParameters;
use App\UserParameters;
use App\ConnectorRuleToParameter;
use App\ConnectorRuletoPlacementParameter;

class RuleController extends Controller{

     ///index untuk menampilkan keseluruhan rule yang ada
    public function index($id){
        // $r = RulesAccessModel::firstorFail()->room->name;
        // $rule = RulesAccessModel::where('secret_key', '=' ,'r85j9rH0xFZdqJEIRESDfap66udkA5A5nLX7C2SJXttGiR1lafcW73TKMKSV')->firstorFail();
        // dd($r);

        $madeby = auth()->user();
        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "Users";
        $data['id'] = $id;
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        return view('rules_pages.rules_list', $data);
    }
    //untuk fungsi multi select user
    public function create($id_project, $id_room){

        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "Users";

        $madeby = auth()->user();
        $data['id_projects'] = $id_project;
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        $data['room'] = RoomModel::find($id_room);
        $data['project'] = Projects::find($id_project);
        $data['list_user'] = UserModel::where('id_project', $id_project)->pluck('name', 'id');
        $data['getselectedUser'] = RulesAccessModel::where('id_room', $id_room)->pluck('id_user')->toArray();
        return view('rules_pages.add_rules', $data);
    }

    protected function createRuleData(array $params){
       return Rules_model::create($params);
    }


    protected function everydayTimeCheck($id_room, $day){
        $getResult = Rules_model::where('id_room', '=', $id_room)
                                ->where('day', '=', $day)
                                ->where('start_time', NULL)
                                ->where('end_time', NULL)

                                ->get()->count();

        if($getResult >= 1){
            return true;
        }else{
            return false;
        }
    }


    protected function haveTimeCheck($id_room, $_time_start, $_time_end, $day){

         /*
            SET @tstart='10:00';
            SET @tend='11:00';

            INSERT INTO t_time_range (time_start, time_end)
            SELECT @tstart, @tend
            WHERE  NOT EXISTS
            ( SELECT 1
                FROM   t_time_range
                WHERE  time_start >= @tstart AND time_start < @tend
                OR     time_end > @tstart    AND time_end <= @tend
                OR     @tstart >= time_start AND @tstart < time_end
            )
            */

        $checkDayTime =  Rules_model::where(function ($query) use ($id_room, $day, $_time_start, $_time_end){
                $query->where('id_room', '=' , $id_room)
                        ->Where('day', '=' , $day)
                        ->where('start_time', '>=', $_time_start)
                        ->where('start_time', '<' , $_time_end );
                    })
                ->orwhere(function($query) use ($id_room, $day, $_time_start, $_time_end){
                    $query->where('id_room', '=' , $id_room)
                            ->Where('day', '=' , $day)
                            ->where('end_time', '>', $_time_start)
                            ->where('start_time', '<=', $_time_end);
                })
                ->orwhere(function($query) use ($id_room, $day, $_time_start, $_time_end){
                    $query->where('id_room', '=' , $id_room)
                            ->Where('day', '=' , $day)
                            ->where('start_time', '<=', $_time_start)
                            ->where('end_time', '>' , $_time_end);
                })->orwhere(function($query) use ($id_room, $day, $_time_start, $_time_end){
                    $query->where('id_room', '=' , $id_room)
                            ->Where('day', '=' , $day)
                            ->where('start_time', '=', $_time_start)
                            ->where('end_time', '=' , $_time_end);
                })->get()->count();
                // dd([$_time_start,
                //     $_time_end,
                //     $checkDayTime]);
        if($checkDayTime > 0){
            return true;
        }else{
            return false;
        }
    }


    protected function UpdateeverydayTimeCheck($id_room, $day, $id_rule){
        $getResult = Rules_model::where('id_room', '=', $id_room)
                                ->where('day', '=', $day)
                                ->where('start_time', NULL)
                                ->where('end_time', NULL)
                                ->where('id', '!=' , $id_rule)
                                ->get()->count();

        if($getResult >= 1){
            return true;
        }else{
            return false;
        }
    }

    protected function UpdatehaveTimeCheck($id_room, $_time_start, $_time_end, $day, $id_rule){

        /*
           SET @tstart='10:00';
           SET @tend='11:00';

           INSERT INTO t_time_range (time_start, time_end)
           SELECT @tstart, @tend
           WHERE  NOT EXISTS
           ( SELECT 1
               FROM   t_time_range
               WHERE  time_start >= @tstart AND time_start < @tend
               OR     time_end > @tstart    AND time_end <= @tend
               OR     @tstart >= time_start AND @tstart < time_end
           )
           */

       $checkDayTime =  Rules_model::where(function ($query) use ($id_room, $day, $_time_start, $_time_end, $id_rule){
        $query->where('id_room', '=' , $id_room)
                       ->Where('day', '=' , $day)
                       ->where('start_time', '>=', $_time_start)
                       ->where('start_time', '<' , $_time_end )
                       ->where('id', '!=' , $id_rule);
                   })
               ->orwhere(function($query) use ($id_room, $day, $_time_start, $_time_end, $id_rule){
                   $query->where('id_room', '=' , $id_room)
                           ->Where('day', '=' , $day)
                           ->where('end_time', '>', $_time_start)
                           ->where('start_time', '<=', $_time_end)
                           ->where('id', '!=' , $id_rule);
               })
               ->orwhere(function($query) use ($id_room, $day, $_time_start, $_time_end, $id_rule){
                   $query->where('id_room', '=' , $id_room)
                           ->Where('day', '=' , $day)
                           ->where('start_time', '<=', $_time_start)
                           ->where('end_time', '>' , $_time_end)
                           ->where('id', '!=' , $id_rule);
               })->orwhere(function($query) use ($id_room, $day, $_time_start, $_time_end, $id_rule){
                   $query->where('id_room', '=' , $id_room)
                           ->Where('day', '=' , $day)
                           ->where('start_time', '=', $_time_start)
                           ->where('end_time', '=' , $_time_end)
                           ->where('id', '!=' , $id_rule);
               })->get()->count();
            //    dd([$_time_start,
            //        $_time_end,
            //        $checkDayTime]);
       if($checkDayTime > 0){
           return true;
       }else{
           return false;
       }
   }


    //store schedule rule
    public function store($id_project, $id_room, Request $request){
        // dd($request);
        $afterTime = 'sometimes';
        $doValidateTime = false;
        $doValidateDay = false;
        if($request->time_start == "0:00" && $request->time_end == "0:00"){
            $_time_start = null;
            $_time_end  = null;
            $doValidateDay = true;
        }else{
            $_time_start = $request->time_start;
            $_time_end  = $request->time_end;
            $doValidateTime = true;
        }




        if($_time_end != null){
            $afterTime .= '|after:time_start';
        }

        $validator = $request->validate([
            'rule_name'     =>  'required:max:20',
            'time_start'    =>  'sometimes',
            'time_end'      =>  $afterTime,
        ]);

        if($doValidateDay){
            if($this->everydayTimeCheck($id_room, $request->day)){
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'field_name_1' => ['You have already 24 Hours on '.$request->day.' in your rule!'],
                ]);
                throw $error;
            }
        }

        if($doValidateTime){
            if($this->haveTimeCheck($id_room, $_time_start, $_time_end, $request->day)){
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'field_name_1' => ['The time cannot crossed with each other in same day!'],
                ]);
                throw $error;
            }
        }

        if($request->statusrule == null){
            $_status = false;
        }else{
            $_status = true;
        }

        if($request->en_schedule == null){
            $_schedule_enable = false;
        }else{
            $_schedule_enable = true;
        }

        // jika parameter tidak ada.
        if($request->en_uparam == null){
            $param_enable = false;
        }else {
            $param_enable = true;
        }

        $madeby = auth()->user();
        DB::beginTransaction();
        try{
            $data = $this->createRuleData([
                'id_room'               => $id_room,
                'rule_name'             => $request->rule_name,
                'status'                => $_status,
                'enable_schedule'       => $_schedule_enable,
                'sub_name'              => $request->sub_name,
                'description'           => $request->description,
                'day'                   => $request->day,
                'date'                  => $request->date_schedule,
                'start_time'            => $_time_start,
                'end_time'              => $_time_end,
                'offset_time'           => $request->offset_time,
            ]);


            if($param_enable){
                $getParameter = UserParameters::find($request->parameter);
            }

            $getProject = Projects::find($id_project);
            $room = RoomModel::find($id_room);
            // $deleteExsist = RulesAccessModel::where('id_room', $id_room)->delete();
        if($request->has('permission')){
                foreach($request->permission as $permission){
                    $user = UserModel::findOrFail($permission);
                    $rule = RulesAccessModel::create([
                        'project_key' => $getProject->project_key,
                        'id_rules'    => $data->id,
                        'id_project'  => $id_project,
                        'id_room'     => $id_room,
                        'secret_key'  => $room->secret_key,
                        'id_user'     => $user->id,
                        'rfid_user'   => $user->rfid,
                    ]);

                    if($param_enable){
                        $param = UserToParameters::create([
                            'id_projects' => $id_project,
                            'user_model_id' => $user->id,
                            'rule_id'   => $data->id,
                            'user_parameters_id' => $request->parameter,
                            'values'  => $getParameter->default_value,
                        ]);
                    }
                }
            }

            if($request->has('placeparam')){
                foreach($request->placeparam as $index =>$item){
                    $save = ConnectorRuletoPlacementParameter::create([
                        'id_rule'   => $data->id,
                        'id_placement_param' => $item
                    ]);
                }
            }

            if($param_enable){
                ConnectorRuleToParameter::create([
                    'id_projects'           => $id_project,
                    'rule_id'               => $data->id,
                    'enable'                => $param_enable,
                    'parameter_id'          => $getParameter->id,
                    'id_placement'          => $id_room,
                    'by_value'              => $request->by_value,
                    'max_value'             => $request->max_value,
                    'min_value'             => $request->min_value,
                    'operator' => $request->operator,
                ]);
            }

            DB::commit();
            return redirect()->route('placement.detail', [$id_project, $id_room])->with('success', 'Rule is successfully created');
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }


    public function show($id_project, $id_room, $id_rule){

        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "Users";
        $data['id_projects'] = $id_project;
        $data['rule'] = Rules_model::find($id_rule);
        $con = $data['rule'];
        // dd($con->conectorparameter->parameters->name);
        $data['id_room'] = $id_room;
        $data['project'] = Projects::find($id_project);

        $data['getselectedUser'] = RulesAccessModel::where('id_room', $id_room)->where('id_rules', $id_rule)->pluck('id_user')->toArray();
        $data['getselectedParameter'] = UserToParameters::where('rule_id', $id_rule)->first();
        $data['list_user'] = UserModel::where('id_project', $id_project)->pluck('name', 'id');
        $data['room'] = RoomModel::find($id_room);

        $madeby = auth()->user();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        return view('rules_pages.show_rule', $data);

    }


    public function storeschedule($id_project, $id_room, Request $request){
        dd($request);
        $validator = Validator::make($request->all(), [
            'name_schedule' => 'required',
        ]);

        if($validator->passes()){
            if($request->radio_schedule == "all-days"){
                $schedule = ScheduleModel::where('id_room', $id_room)->delete();
                if($request->ajax()){
                    if($request->status == null){
                        $_status = '0';
                    }else{
                        $_status = '1';
                    }
                    $data = ScheduleModel::create([
                         'id_project'            =>  $id_project,
                         'id_room'               =>  $id_room,
                         'is_schedule_active'    =>  $_status,
                         'name_schedule'         =>  $request->name_schedule,
                         'type_schedule'         =>  $request->radio_schedule,
                         'date'                  =>  $request->date_schedule,
                         'day'                   =>  $request->day_schedule,
                         'start_time'            =>  $request->time_start,
                         'end_time'              =>  $request->time_end,
                         'offset_time'           =>  $request->offset_time,
                     ]);
                //  return response()->json();
                    return Response::json(['success' => '1']);
                 }
            } elseif ($request->radio_schedule == "specific-days"){
                $schedule = ScheduleModel::where('id_room', $id_room)
                                            ->where('type_schedule', '=' , 'all-days')
                                            ->orWhere('type_schedule', '=' , 'date_schedule')
                                            ->delete();

                if($request->ajax()){
                    if($request->status == null){
                        $_status = '0';
                    }else{
                        $_status = '1';
                    }
                    $data = ScheduleModel::create([
                         'id_project'            =>  $id_project,
                         'id_room'               =>  $id_room,
                         'is_schedule_active'    =>  $_status,
                         'name_schedule'         =>  $request->name_schedule,
                         'type_schedule'         =>  $request->radio_schedule,
                         'date'                  =>  $request->date_schedule,
                         'day'                  =>  $request->day_schedule,
                         'start_time'            =>  $request->time_start,
                         'end_time'              =>  $request->time_end,
                         'offset_time'           =>  $request->offset_time,
                     ]);
                     return Response::json(['success' => '1']);
                }
            } elseif($request->radio_schedule == "date") { // jika schedule is date_schedule
                if($request->status == null){
                    $_status = '0';
                }else{
                    $_status = '1';
                }
                $schedule = ScheduleModel::where('id_room', $id_room)
                            ->where('type_schedule', '=' , 'all-days')
                            ->orWhere('type_schedule', '=' , 'date_schedule')
                            ->delete();

                if($request->ajax()){
                    $data = ScheduleModel::create([
                        'id_project'            =>  $id_project,
                        'id_room'               =>  $id_room,
                        'is_schedule_active'    =>  $_status,
                        'name_schedule'         =>  $request->name_schedule,
                        'type_schedule'         =>  $request->radio_schedule,
                        'date'                  =>  $request->date_schedule,
                        'day'                  =>  $request->day_schedule,
                        'start_time'            =>  $request->time_start,
                        'end_time'              =>  $request->time_end,
                        'offset_time'           =>  $request->offset_time,
                    ]);
                   //  return response()->json();
                   return Response::json(['success' => '1']);
                }

            }

            return Response::json(['errors' => $validator->errors()->all()]);

        }

    }

    //edit user access rules
    public function edit($id_room, $id_project, Request $request){
        //

        // $data['page_title'] = "Welcome Dashboard";
        // $data['subheader_title'] = "Users";
        // $data['id'] = $id_room;
        // $data['id_project'] = $id_project;
        // $madeby = auth()->user();
        // $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        // $data['list_room'] = RoomModel::find($id_room);
        // $data['getselectedUser'] = RulesAccessModel::where('id_room', $id_room)->pluck('id_user')->toArray();
        // $data['list_user'] = UserModel::where('id_project', $id_project)->pluck('name', 'id');

        // return view('rules_pages.edit_rule', $data);
    }


    public function update($id_project, $id_room, $id_rule, Request $request){
        // dd($request);

        $madeby = auth()->user();
        $afterTime = 'sometimes';
        $doValidateTime = false;
        $doValidateDay = false;

        if($request->time_start == "0:00" && $request->time_end == "0:00"){
            $_time_start = null;
            $_time_end  = null;
            $doValidateDay = true;
        }else{
            $_time_start = $request->time_start;
            $_time_end  = $request->time_end;
            $doValidateTime = true;
        }

        if($_time_end != null){
            $afterTime .= '|after:time_start';
        }

        if($request->en_schedule == null){
            $_schedule_enable = false;
        }else{
            $_schedule_enable = true;
        }

        if($request->en_uparam == null){
            $param_enable = false;
        }else {
            $param_enable = true;
        }

        $validator = $request->validate([
            'rule_name'     =>  'required:max:20',
            'time_start'    =>  'sometimes',
            'time_end'      =>  $afterTime,
        ]);


        if($doValidateDay){
            if($this->UpdateeverydayTimeCheck($id_room, $request->day, $id_rule)){
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'field_name_1' => ['You have already 24 Hours on '.$request->day.' in your rule!'],
                ]);
                throw $error;
            }
        }

        if($doValidateTime){
            if($this->UpdatehaveTimeCheck($id_room, $_time_start , $_time_end, $request->day, $id_rule)){

                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'field_name_1' => ['The time cannot crossed with each other in same day!'],
                ]);
                throw $error;
            }
        }

        DB::beginTransaction();
        try{

        $updateSchedule = [
            'rule_name'         => $request->rule_name,
            'sub_name'          => $request->sub_name,
            'status'            => $request->statusrule,
            'enable_schedule'   => $_schedule_enable,
            'description'       => $request->description,
            'day'               => $request->day,
            'start_time'        => $_time_start,
            'end_time'          => $_time_end,
            'offset_time'       => $request->offset_time,
        ];

            $getParameter = UserParameters::find($request->parameter);
            $getProject = Projects::where('id', '=', $id_project)->first();
            $room = RoomModel::find($id_room);
            $getData = RulesAccessModel::where('id_rules', $id_rule)->pluck('id_user');
            $selectedUser = $getData->diff($request->permission);
            $newUserSelected = collect($request->permission)->diff($getData);

            //deleted selected user
            RulesAccessModel::where('id_rules', $id_rule)->whereIn('id_user', $selectedUser)->delete();
            UserToParameters::where('rule_id', $id_rule)->whereIn('user_model_id', $selectedUser)->delete();

        if($request->has('permission')){
            foreach($newUserSelected as $index => $user_id){
                $users = UserModel::where('id', $user_id)->get();
                foreach($users as $user){
                //$user = UserModel::findOrFail($permission);
                    $rule = RulesAccessModel::create([
                        'project_key' => $getProject->project_key,
                        'id_rules'    => $id_rule,
                        'id_project'  => $id_project,
                        'id_room'     => $id_room,
                        'secret_key'  => $room->secret_key,
                        'id_user'     => $user->id,
                        'rfid_user'   => $user->rfid,
                    ]);
                    if($param_enable){
                     $create =  UserToParameters::create([
                            'id_projects'           => $id_project,
                            'user_model_id'         => $user->id,
                            'rule_id'               => $id_rule,
                            'user_parameters_id'    => $request->parameter,
                            'values'                => $getParameter->default_value,
                        ]);
                    }
                }
            }
        }

        if($request->has('placeparam')){
            foreach($request->placeparam as $index =>$item){
                $save = ConnectorRuletoPlacementParameter::create([
                    'id_rule'   => $id_rule,
                    'id_placement_param' => $item
                ]);
            }
        }

        if(!$_schedule_enable){
            $updateSchedule = [
                'enable_schedule'   => $_schedule_enable,
                'day'               => NULL,
                'start_time'        => NULL,
                'end_time'          => NULL,
                'offset_time'       => 0,
            ];
        }

        if($param_enable){
            //cek jika userToParameter kosong, maka akan dibuat baru
            $cekIfthere = UserToParameters::where('rule_id', $id_rule)->get();
            if(count($cekIfthere) > 0){ //jika ada tetapi user melakukan perubahan pada yang dipilih parameters
                foreach($cekIfthere as $item){
                    if($item->user_parameters_id != $request->parameter){
                        ConnectorRuleToParameter::where('rule_id', $id_rule)->update([
                            'parameter_id' => $request->parameter,
                        ]);
                        $param = UserToParameters::where('rule_id', '=' ,$id_rule)
                            ->update([
                                'user_parameters_id' => $request->parameter,
                                'values'             => $getParameter->default_value,
                            ]);
                    }
                }
            }else{
                if($request->has('permission')){
                    foreach($request->permission as $permission){
                        $user = UserModel::findOrFail($permission);
                        $rule = RulesAccessModel::updateOrCreate(
                        ['id_rules' => $id_rule],
                            [
                            'project_key' => $getProject->project_key,
                            'id_rules'    => $id_rule,
                            'id_project'  => $id_project,
                            'id_room'     => $id_room,
                            'secret_key'  => $room->secret_key,
                            'id_user'     => $user->id,
                            'rfid_user'   => $user->rfid,
                        ]);

                        if($param_enable){
                            $param = UserToParameters::create([
                                'id_projects' => $id_project,
                                'user_model_id' => $user->id,
                                'rule_id'   => $id_rule,
                                'user_parameters_id' => $request->parameter,
                                'values'  => $getParameter->default_value,
                            ]);
                        }
                    }
                }
            }
            ConnectorRuleToParameter::updateOrCreate(
                ['rule_id' => $id_rule],
                [
                'id_projects'           => $id_project,
                'rule_id'               => $id_rule,
                'enable'                => $param_enable,
                'parameter_id'          => $getParameter->id,
                'id_placement'          => $id_room,
                'by_value'              => $request->by_value,
                'max_value'             => $request->max_value,
                'min_value'             => $request->min_value,
                'operator'              => $request->operator,
            ]);
        }else{
            //jika userparameter di unchek, maka akan terhapus
            ConnectorRuleToParameter::where('rule_id', $id_rule)->delete();
            UserToParameters::where('rule_id', $id_rule)->delete();
        }

        Rules_model::where('id', $id_rule)->update($updateSchedule);
        DB::commit();
        return Redirect::back()->with('success', 'Rule Successfully Updated');
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id){
        // die('a');
        // $rule->delete();
        $delete = Rules_model::where('id', $id)->delete();
        return Redirect::back()->with('success', 'Rule has been deleted!');
        // if($delete){
        //     return Redirect::back()->with('success', 'User Profile Successfully Updated');
        // }

    }

}
