<?php

namespace App\Http\Controllers;

use App\log_data;
use Datatables;
use DB;
use App\Projects;
use App\User;
use App\UserModel;
use App\RoomModel;
use Illuminate\Http\Request;

class LogDataController extends Controller{

    public function index(Request $request, $id_project){

        // if ($request->ajax()) {

        //     $madeby = auth()->user();

        //     $getProjectid = Projects::where('id', $id_project)
        //                     ->where('id_pembuat', $madeby->id)->get();

        //     $getLog = log_data::where('log_datas.project_key',$getProjectid[0]->project_key)
        //                 ->join('usersdata', 'log_datas.rfid', '=', 'usersdata.rfid')
        //                 ->join('roomdata', 'log_datas.secret_key_room', '=', 'roomdata.secret_key')
        //                 ->join('devices', 'log_datas.mac_address', '=', 'devices.mac_address')
        //                 ->select('roomdata.name as nameroom','roomdata.id as id_room', 'usersdata.name as nameuser','usersdata.id as id_user' ,'log_datas.user_doing', 'log_datas.callback', 'log_datas.device_name', 'log_datas.created_at as created_log' ,'devices.mac_address','devices.local_ip_address' ,'devices.id as id_devices')
        //                 ->orderBy('created_log', 'desc');
        //     return Datatables::of($getLog)
        //     ->make(true);
        // }

        $madeby = auth()->user();
        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "Users";
        $data['id_project'] = $id_project;
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        // return view('log_pages.show_log', $data);
    }


    public function create()
    {
        //
    }


    public function store($secret_key_room,$rfid, Request $request){
        //
        $header = $request->header('SECRET_KEY');
        $getData = log_data::where('rfid', '=', $rfid);

        $data_ = log_data::create([
            'rfid'              => $rfid,
            'secret_key_room'   => $secret_key_room,
            'project_key'       => $header,
            'user_doing'        => $request->user_doing,
            'callback'          => $request->callback,
            'device_name'       => $request->device_name,
            'mac_address'       => $request->mac_address,
            'local_ip_address'  => $request->local_ip_address
        ]);

        return response()->json($data_);
    }

    public function getdetailschedule($id, Request $request){
        //untuk melihat 1 log pada dashboard
    }

    public function getCompleteLog($id_project, $id_room, Request $request){
        $data['id_projects'] = $id_project;
        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "Nama Project";
        $madeby = auth()->user();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        $log = Projects::find($id_project);
        $data['id_projects'] = $id_project;
        $data['room'] = RoomModel::find($id_room);
        $data['getLogData'] = $log->logs()->paginate(20);
        return view('log_pages.show_log', $data);
    }

    public function show(){
        //

    }

    public function edit(log_data $log_data)
    {
        //
    }

    public function update(Request $request, log_data $log_data)
    {
        //
    }

    public function destroy(log_data $log_data)
    {
        //
    }
}
