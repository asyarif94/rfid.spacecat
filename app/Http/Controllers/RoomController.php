<?php

namespace App\Http\Controllers;
use Datatables;
use DB;
use Illuminate\Http\Request;
use App\RoomModel;
use App\Projects;
use App\RulesAccessModel;
use App\UserModel;
use App\Rules_model;
use App\UserParameters;
use App\Log_data;
use Validator;
use Redirect;
use App\PlacementParameters;

class RoomController extends Controller{

    //generating Uniqe number for Secret Key
    public static function quickRandom($length = 16){
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    public function index($id_project){
        $data['id_projects'] = $id_project;
        $data['subheader_title'] = "Placement";
        $madeby = auth()->user();
        $data['projects_'] = $madeby->projects;
        $project = Projects::findOrFail($id_project);
        $data['page_title'] = $project->name;
        $data['getRoom'] = $project->rooms;

        return view('room_pages.room_list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_project){
        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "Add Placement";
        $data['id_projects'] = $id_project;
        $madeby = auth()->user();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        return view('room_pages.add_room', $data);
    }


    public function store(Request $request){
        $id_project = $request->id;
        $project = Projects::findOrFail($id_project);
        $room_secret_key =  $this->quickRandom(60);

        $validatedData = $request->validate([
            'name'  => 'required|string|max:255',
            'status'=>  'required',
            'description'   => 'string|max:255|nullable',
        ]);

        $validatedData = array_merge($validatedData, [
            'id_project' => $id_project,
            'secret_key' => $room_secret_key
        ]);

        $room = RoomModel::create($validatedData);
        return redirect()->route('daftar.placement', $id_project);
        // $room = RoomModel::create($validateData);

    }

    public function show($id_project, $id_room, Request $request){

        $data['id_projects'] = $id_project;
        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "Placement";
        $madeby = auth()->user();
        $getData =  RoomModel::find($id_room);
        $data['devices'] = $getData->device;
        $data['logdata'] = $getData->allTap()->paginate(10);
        $data['logdatas'] = $getData;
        $data['room'] = RoomModel::find( $id_room);
        $data['countTotalAccessRoom'] = RulesAccessModel::where('id_room', $id_room)->groupBy('id_user')->count();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        $data['getselectedUser'] = RulesAccessModel::where('id_room', $id_room)->pluck('id_user')->toArray();
        $data['list_user'] = UserModel::where('id_project', $data['room']->id_project)->pluck('name', 'id');

        return view('room_pages.show_room', $data);
    }


    public function edit($id_project, $id_room){
        $data['id_projects'] = $id_project;
        $madeby = auth()->user();
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        $data['page_title'] = "Welcome Dashboard";
        $data['subheader_title'] = "Add Room";
        $data['room'] = RoomModel::findOrFail($id_room);
        return view('room_pages.edit_room', $data);
    }


    public function update(Request $request, $id){
        // dd($request);
        $validatedData = $request->validate([
            'name'  => 'required|string|max:255',
            'description'   => 'string|max:255|nullable',
        ]);


        if(is_null($request->status)){
            $_status = 0;
        }else{
            $_status = 1;
        };

        RoomModel::where('id', $id)->update(array(
            'name'          => $request->name,
            'status'        => $_status,
            'description'   => $request->description
        ));

        // return redirect()->route('daftar.placement',  $request->id_project);
        return Redirect::back()->with('success', 'Placement successfully updated');

    }

    public function destroy(RoomModel $room){
     $room->delete();
     return redirect()->route('daftar.placement', $room->id_project)->with('success', 'Placement has been deleted!');
    //  return Redirect::back()->with('success', 'Room Successfully Deleted');
    }

    public function settings($id_project, $id_room){
        $data['id_projects'] = $id_project;
        $data['subheader_title'] = "Settings";
        $madeby = auth()->user();
        $data['projects_'] = $madeby->projects;
        $project = Projects::findOrFail($id_project);
        $data['page_title'] = $project->name;
        $data['room'] = RoomModel::findOrFail($id_room);

        return view('room_pages.settings', $data);
    }

    public function updatesettings($id_project, $id_room, Request $request){
        if($request->en_placement_parameters){
                DB::beginTransaction();
                try {
                    RoomModel::where('id', $id_room)->update(array('enable_parameters'=>'1'));
                    $getParam = PlacementParameters::where('id_placement', $id_room)->get();
                    if(sizeof($getParam) > 0){
                        //jika terdapat rules
                        // die('a');
                    }else{
                        for($i = 0; $i < 4; $i++){
                            PlacementParameters::create([
                                'id_placement'  => $id_room,
                                'name'          => "#param-".rand(pow(10, 4-1), pow(10, 4)-1),
                                'sub_name'      => "-",
                                'value'         => 0,
                                'unit'          => NULL,
                                'allow_remote'  => 0,
                                'min_value'     => 0,
                                'max_value'     => 0,
                                'operator'      => 'add',
                                'by_value'      => 0,
                            ]);
                        }
                    }
                    DB::commit();
                    return Redirect::back()->with('success', 'Placement successfully updated');
                } catch (\Exception $e) {
                    DB::rollback();
                    // something went wrong
                    dd($e);
                }

        }else{
            try {
                RoomModel::where('id', $id_room)->update(array('enable_parameters'=>'0'));
                PlacementParameters::where('id_placement', $id_room)->delete();
                DB::commit();
                return Redirect::back()->with('success', 'Placement has been deleted!');
            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
                // dd($e);
            }
       }
    }

    public function editsettings($id_project, $id_room, $id_parameter){
        $data['parameter'] = PlacementParameters::find($id_parameter);
        $data['id_projects'] = $id_project;
        $data['subheader_title'] = "Placement";
        $madeby = auth()->user();
        $data['projects_'] = $madeby->projects;
        $project = Projects::findOrFail($id_project);
        $data['page_title'] = $project->name;
        $data['room'] = RoomModel::find($id_room);

        return view('room_pages.edit_parameter', $data);
    }

    public function savesettings($id_project, $id_room, $id_parameter, Request $request){
        $request->validate([
            'name'      => 'required|string|max:255|regex:/^\S*$/u',
            'sub_name'  => 'string|max:255',
            'by_value'  => 'required|numeric',
            'min_value' => 'required|numeric',
            'max_value' => 'required|numeric|gt:min_value'
        ]);

        $update = [
            'name'          =>  $request->name,
            'sub_name'      =>  $request->sub_name,
            'allow_remote'  =>  $request->allow_remote,
            'operator'      =>  $request->operator,
            'by_value'      =>  $request->by_value,
            'unit'          =>  $request->unit,
            'min_value'     =>  $request->min_value,
            'max_value'     =>  $request->max_value,
        ];

        PlacementParameters::find($id_parameter)->update($update);
        return Redirect::back()->with('success', 'Settings Successfully Updated');
    }
}
