<?php

namespace App\Http\Controllers;

use App\UserParameters;
use Illuminate\Http\Request;
use App\Projects;
use App\UserToParameters;
use Redirect;

class ParametersUserController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_project){

        $data['id_projects'] = $id_project;
        $data['subheader_title'] = "Parameters";
        $madeby = auth()->user();
        $data['projects_'] = $madeby->projects;
        $data['project'] = Projects::find($id_project);
        $data['page_title'] = "Parameters" ." ".$data['project']->name;

        return view('parameters.parameter_list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_project){

        $data['page_title'] = "Add Parameter";
        $data['subheader_title'] = "Add New Parameter";
        $madeby = auth()->user();
        $data['id_projects'] = $id_project;
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        $data['project'] = Projects::find($id_project);
        return view('parameters.add_parameter', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_project){

        $validatedData = $request->validate([
            'name'         => 'required|string|max:255',
            'default_value'=> 'required|numeric',
            'units'         => 'required|max:10',

        ]);

        if($request->allow_remote == null){
            $allow_remote = '0';
        }else{
            $allow_remote = '1';
        }

        $validatedData = array_merge($validatedData,[
            'allow_remote_access'   => $allow_remote,
            'id_projects'    => $id_project,
        ]);

        $data = UserParameters::create($validatedData);
        if($data){
            return redirect()->route('parameter.list', $id_project)->with('success', 'Parameters is successfully created');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserParameters  $userParameters
     * @return \Illuminate\Http\Response
     */
    public function show($id_project, $id_parameter){
        $data['page_title'] = "Edit Parameters";
        $data['subheader_title'] = "Edit Parameters";
        $madeby = auth()->user();
        $data['id_projects'] = $id_project;
        $data['projects_'] = Projects::where('id_pembuat', $madeby->id)->get();
        $data['project'] = Projects::find($id_project);
        $data['parameters'] = UserParameters::find($id_parameter);

       return view('parameters.edit_parameter', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserParameters  $userParameters
     * @return \Illuminate\Http\Response
     */
    public function edit(UserParameters $userParameters){
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserParameters  $userParameters
     * @return \Illuminate\Http\Response
     */
    public function update($id_project, $id_parameter, Request $request){

        $validatedData = $request->validate([
            'name'         => 'required|string|max:255',
            'default_value'=> 'required|numeric',
            'units'         => 'required|max:10',

        ]);

        if($request->allow_remote == null){
            $allow_remote = '0';
        }else{
            $allow_remote = '1';
        }

        $validatedData = array_merge($validatedData,[
            'allow_remote_access'   => $allow_remote,
            'id_projects'    => $id_project,
        ]);

        $data = UserParameters::where('id', $id_parameter)->update(array(
            'name' => $request->name,
            'default_value'         => $request->default_value,
            'allow_remote_access'   => $allow_remote,
            'units'                 => $request->units,
        ));

        if($data){
            return redirect()->route('parameter.list', $id_project)->with('success', 'Parameters is successfully saved');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserParameters  $userParameters
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_parameter){

        $delete = UserParameters::where('id', $id_parameter)->delete();
        if($delete){
            return Redirect::back()->with('success', 'Parameter has been deleted!');
        }
    }
}
