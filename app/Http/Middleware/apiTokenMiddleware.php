<?php

namespace App\Http\Middleware;

use Closure;
use App\Projects; //Model Projects
use Illuminate\Http\Request;

class apiTokenMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
    //   echo $request->header('Secret_key');
        // echo "api key : ";
        $authorization = $request->header('SECRET-KEY');
        // echo $authorization;
        //pertama cek Project Key pada setiap request lalu cek room key
        $getApiKey = Projects::where('project_key', $authorization);
        if($getApiKey->count() <= 0){
             return response()->json([
                 'message' => 'Invalid api token'
             ], 404);
        }
        return $next($request);
    }

}
