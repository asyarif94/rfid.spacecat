<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class UserToParameters extends Model{

    protected $table = "users_to_parameters";
    protected $primaryKey = 'id';

    protected $casts = [
        'id' => 'string',
    ];

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'user_model_id',
        'user_parameters_id',
        'rule_id',
        'id_projects',
        'values',
    ];

    public function parameters(){
        return $this->belongsTo(UserParameters::class, 'user_parameters_id');
    }


}
