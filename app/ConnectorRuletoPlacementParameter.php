<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class ConnectorRuletoPlacementParameter extends Model{
    protected $table = "connector_rule_to_placement_parameter";

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id',
        'id_rule',
        'id_placement_param',
    ];
}
