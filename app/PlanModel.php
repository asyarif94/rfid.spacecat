<?php

namespace App;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;


class PlanModel extends Model{
    protected $table = 'user_plan';

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
        self::creating(function ($model) {
            $model->project_key = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
                'name',
                'total_project',
                'total_users',
                'total_rooom',
                'total_rules',
                'total_devices',
                'total_request',
                'price',
                'description'
            ];

    public function plan(){
        return $this->belongsTo(User::class);
    }
}
