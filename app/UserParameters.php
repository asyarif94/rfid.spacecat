<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

//model unutk paramaters
class UserParameters extends Model{
    protected $table = "parameters";
    protected $casts = ['id' => 'string'];

    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }
    protected $fillable = [
        'id_projects',
        'name',
        'default_value',
        'units',
        'allow_remote_access'
    ];
    //relation to projects

    //relation to user
    public function users(){
        return $this->belongsToMany(UserModel::class, 'users_to_parameters', 'user_model_id', 'user_parameters_id');
    }



}
