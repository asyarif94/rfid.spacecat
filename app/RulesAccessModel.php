<?php

namespace App;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class RulesAccessModel extends Model{

    protected $table = 'rules_access';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $casts = ['id' => 'string',
                    '   id_rules' => 'string'
                        ];
    public static function boot(){
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $fillable = [
        'id',
        'id_project',
        'id_rules',
        'project_key',
        'id_room',
        'secret_key',
        'id_user',
        'rfid_user'

    ];


    public function project(){
        return $this->belongsTo(Projects::class, 'id_project');
    }

    public function room(){
        return $this->belongsTo(RoomModel::class, 'id_room');
    }

    public function roomAccess(){
        return $this->belongsTo(RoomModel::class, 'id_project');
    }

    public function user(){
        return $this->belongsTo(UserModel::class, 'id_user');
    }


    //test
    public function placements(){
        return $this->belongsToMany(RoomModel::class, 'id_room');
    }

    public function users(){
        return $this->hasMany(UserModel::class, 'id_user');
    }


}
