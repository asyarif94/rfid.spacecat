@extends('layouts.mainlayout')
@section('title', 'Profile')
@section('content')

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">

    </div>
</div>

<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="kt-portlet kt-portlet--tabs">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        User Profile
                                    </h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                    <ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#tab_basic" role="tab">
                                                Basic Info
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#tab_change_password" role="tab">
                                                Change Password
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_basic">
                                        <div class="kt-section kt-section--first ml-5">
                                            <div class="kt-section__body ">
                                                <div class="row">
                                                    <label class="col-xl-3"></label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <h3 class="kt-section__title kt-section__title-sm">User Info:</h3>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <a href="#" class="kt-media kt-media--xl kt-media--circle kt-media--info">
                                                            <span>{{auth()->user()->name[0] }}</span>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Name</label>
                                                    <div class="col-lg-9 col-xl-7">
                                                        <input class="form-control" type="text" value="{{Auth::user()->name}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Email</label>
                                                    <div class="col-lg-9 col-xl-7">
                                                        <input class="form-control" type="text" value="{{Auth::user()->email}}" disabled>
                                                    </div>
                                                </div>
                                                @if(Auth::user()->provider)
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Provider</label>
                                                        <div class="col-lg-9 col-xl-7">
                                                            <input class="form-control" type="text" value="Github" disabled>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Since</label>
                                                    <div class="col-lg-9 col-xl-7">
                                                        <input class="form-control" type="text" value="{{Carbon\Carbon::parse(Auth::user()->created_at)->format('M d, Y')}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_change_password">
                                        <form class="kt-form kt-form--label-right" id="formpwd" method="POST" action="{{ route('change.password') }}">
                                            @csrf
                                            <div class="kt-portlet__body">
                                                <div class="kt-section kt-section--first">
                                                    <div class="kt-section__body">
                                                        <div class="row">
                                                            <label class="col-xl-3"></label>
                                                            <div class="col-lg-9 col-xl-8">
                                                                <h3 class="kt-section__title kt-section__title-sm">Change Or Recover Your Password:</h3>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Current Password</label>
                                                            <div class="col-lg-9 col-xl-8">
                                                                <input type="password" class="form-control" value="" name="current_password" placeholder="Current password" autocomplete="current-password"
                                                                @if(Auth::user()->provider)
                                                                    disabled
                                                                @endif
                                                                >
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">New Password</label>
                                                            <div class="col-lg-9 col-xl-8">
                                                                <input type="password" class="form-control" value="" name="new_password" placeholder="New password" autocomplete="current-password"
                                                                @if(Auth::user()->provider)
                                                                    disabled
                                                                @endif
                                                                >
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-last row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Verify Password</label>
                                                            <div class="col-lg-9 col-xl-8">
                                                                <input type="password" class="form-control" value="" name="new_confirm_password" placeholder="Verify password" autocomplete="current-password"
                                                                @if(Auth::user()->provider)
                                                                    disabled
                                                                @endif
                                                                >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__foot">
                                                <div class="kt-form__actions">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-xl-3">
                                                        </div>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <button onclick="submit()" id="submitpwd" class="btn btn-brand btn-bold">Change Password</button>&nbsp;
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function submit(){
        $('#submitpwd').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        $('#formpwd').submit();
    }
</script>

@endsection
