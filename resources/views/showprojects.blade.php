
@extends('layouts.mainlayout')
@section('title',"Project ".$dataprojects->name)
@section('content')

    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Project {{$dataprojects->name}}
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <ul class="breadcrumb">
                    <li ><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li ><a href="{{route('dashboard')}}">Projects</a></li>
                    <li >{{$dataprojects->name}}</li>
                </ul>
            </div>
            <div class="kt-subheader__toolbar">
                <span class="kt-subheader__desc mr-3" id="kt_subheader_total">Created at {{ \Carbon\Carbon::parse($dataprojects->created_at)->format('l, j \\ F Y ') }}</span>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-12">

                <div class="kt-portlet ">
                    <div class="kt-portlet__body">
                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top">
                                <div class="kt-widget__media kt-hidden-">
                                    <img src="{{ asset('media/project-logos/project-default.png') }}" alt="image">
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head">
                                        <div class="kt-widget__user">
                                        @if($dataprojects->status == '1')
                                            <span class="kt-badge kt-badge--success kt-badge--dot kt-badge--lg"></span>
                                        @elseif($dataprojects->status == '3')
                                            <span class="kt-badge kt-badge--warning kt-badge--dot kt-badge--lg"></span>
                                        @else
                                            <span class="kt-badge kt-badge--danger kt-badge--dot kt-badge--lg"></span>
                                        @endif
                                            &nbsp;<a href="{{ route('projects.show', $dataprojects->id) }}" class="kt-widget__username text-uppercase">{{$dataprojects->name}} <small class="text-muted text-capitalize">{{ getNamePlan() }}</small></a>
                                        </div>
                                        <div class="kt-widget__action">
                                            <div class="dropdown">
                                                <button type="button" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Options
                                                </button>
                                                <div class="dropdown-menu">
                                                    <ul class="kt-nav">
                                                        <div class="dropdown-divider"></div>
                                                        <li class="kt-nav__item">
                                                            <a href="{{ route('projects.edit', $dataprojects->id)  }}" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                                <span class="kt-nav__link-text">Edit</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="javascript:;" class="kt-nav__link" onclick="deleteModal('{{$dataprojects->id}}')" data-toggle="modal" data-target="#deleteModal">
                                                                <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                                <span class="kt-nav__link-text">Delete</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kt-widget__info">
                                        <div class="kt-widget__desc">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    {{$dataprojects->description}}
                                                </div>
                                                <div class="col-lg-6">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="kt-widget__bottom align-self-center">
                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <a href="{{route('user.list', $dataprojects->id)}}"><i class="flaticon-users"></i></a>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title"><a href="{{route('user.list', $dataprojects->id)}}" class="kt-widget__title">Users</a></span>
                                        <span class="kt-widget__value"><a href="{{route('user.list', $dataprojects->id)}}" class-="kt-widget__value">{{ $dataprojects->users->count() }}</a><small class="text-muted"> /{{Auth::user()->plan->total_users}}</small></span>
                                    </div>
                                </div>

                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <a href="{{route('daftar.placement', $dataprojects->id)}}"><i class="flaticon-placeholder-1"></i></a>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title"><a href="{{route('daftar.placement', $dataprojects->id)}}" class="kt-widget__title">Placements</a></span>
                                        <span class="kt-widget__value"><a href="{{route('daftar.placement', $dataprojects->id)}}" class-="kt-widget__value">{{ $dataprojects->rooms->count()}} </a><small class="text-muted"> /{{Auth::user()->plan->total_room}}</small></span>
                                    </div>
                                </div>
                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <a href="{{route('parameter.list', $dataprojects->id)}}"><i class="flaticon-menu-button"></i></a>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title"><a href="{{route('parameter.list', $dataprojects->id)}}" class="kt-widget__title">Parameters</a></span>
                                        <span class="kt-widget__value"><a href="{{route('daftar.placement', $dataprojects->id)}}" class-="kt-widget__value">{{ $dataprojects->parameters->count()}} </a><small class="text-muted"> /{{Auth::user()->plan->total_parameter}}</small></span>
                                    </div>
                                </div>
                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <a href="{{route('list.device', $dataprojects->id)}}"><i class="flaticon-symbol"></i></a>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title"><a href="{{url('/devices', $dataprojects->id)}}" class="kt-widget__title">Devices</a></span>
                                        <span class="kt-widget__value"><a href="{{url('/devices', $dataprojects->id)}}" class-="kt-widget__value">{{$dataprojects->device->count()}} </a><small class="text-muted"> /{{Auth::user()->plan->total_devices}}</small></span>
                                    </div>
                                </div>
                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-upload-1"></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title"><a href="" class="kt-widget__title">Request</a></span>
                                        <span class="kt-widget__value"><a href="" class-="kt-widget__value">{{$dataprojects->logs->count()}}</a><small class="text-muted"> /{{Auth::user()->plan->total_request}}</small></span>
                                    </div>
                                </div>
                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-interface-7"></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title"><a href="" class="kt-widget__title">Settings</a></span>
                                        <span class="kt-widget__value"><a href="" class-="kt-widget__value"> </a><small class="text-muted">Preferences</small></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Portlet-->
            </div>
            <!--end:: Portlet-->
            <div class="row">
                @if($dataprojects->status == '1')
                @else
                <div class="col-12 ">
                    <div class="alert alert-light alert-elevate fade show" role="alert">
                        <div class="alert-icon "><i class="flaticon-warning kt-font-danger"></i></div>
                        <div class="alert-text">
                            <strong>Attention!</strong>&nbsp; User can't access the room if project is inactive,
                            <br>
                            For more info please visit the page's <a class="kt-link kt-font-bold" href="https://www.flaticon.com/" target="_blank">Guide</a>.
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-lg-6 col-sm-12">
                    <!--Begin::Portlet-->
                    <div class="kt-portlet kt-portlet--height-fluid">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Recent Activities <small>latest event</small>
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="flaticon-more-1"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md">
                                        <!--begin::Nav-->
                                        <ul class="kt-nav">
                                            <li class="kt-nav__head">
                                            Log Menu
                                                <span data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand kt-svg-icon--md1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect id="bound" x="0" y="0" width="24" height="24"/>
                                                            <circle id="Oval-5" fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                                                            <rect id="Rectangle-9" fill="#000000" x="11" y="10" width="2" height="7" rx="1"/>
                                                            <rect id="Rectangle-9-Copy" fill="#000000" x="11" y="7" width="2" height="2" rx="1"/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </li>

                                            <li class="kt-nav__foot">
                                                <a class="btn btn-brand btn-bold btn-sm" href="{{url('get/log', $id_projects)}}" ">View More...</a>
                                            </li>
                                        </ul>
                                        <!--end::Nav-->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kt-portlet__body">
                            @if(count($activity) == 0)
                                <p class="text-center"> We didn't find anything here...</p>
                            @else
                            @endif
                            <div class="kt-scroll " data-scroll="true" data-height="380" data-mobile-height="300">
                                <div class="kt-timeline-v2">
                                    <div class="kt-timeline-v2__items  kt-padding-top-30 kt-padding-bottom-30">
                                        @foreach ($activity as $jam => $items)
                                            @foreach ($items as $user_doing => $item)
                                                <div class="kt-timeline-v2__item">
                                                    <span class="kt-timeline-v2__item-time">
                                                        {{ $jam }}
                                                    </span>
                                                    @foreach ($item as $callback => $data)
                                                        <div class="kt-timeline-v2__item-cricle" >
                                                            @foreach ($data as $users)
                                                                @if($user_doing == "1" && $users->callback == "1")
                                                                    <i class="fa fa-genderless kt-font-primary"></i>
                                                                @elseif($user_doing == "1" && $users->callback == "3")
                                                                    <i class="fa fa-genderless kt-font-warning"></i>
                                                                @elseif($user_doing == "1" && $users->callback == "2")
                                                                    <i class="fa fa-genderless kt-font-danger"></i>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    <div class="kt-timeline-v2__item-text">
                                                        <div class="kt-media-group d-flex justify-content-center kt-padding-l-10 ">
                                                            @foreach(array_slice($data,0,10)  as $user)
                                                                @if(is_null($user->id_user))
                                                                    <a class="kt-media kt-media--sm kt-media--circle kt-media kt-media--dark" data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="Unknown User" href="JavaScript:Void(0);"><span><i class="fas fa-question"></i></span></a>
                                                                @else
                                                                    <a class="kt-media kt-media--sm kt-media--circle" data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="{{$user->user->name}}" href="{{ route('lihat.user', [$user->id_project, $user->id_user]) }}"><img src="{{ asset($user->user->url_avatar)}}" title=""></a>
                                                                @endif

                                                                @if(count($data) > 10)
                                                                    <a href="#" class="kt-media kt-media--sm kt-media--circle kt-media--dark" data-toggle="kt-tooltip" data-skin="dark" data-placement="top" title="" data-original-title="{{(count($data) - 10 ) }} More Users">
                                                                        <span>+{{(count($data) - 10 )}}</span>
                                                                    </a>
                                                                @endif
                                                                @endforeach
                                                                @if($user_doing == "1" && $users->callback == "1")
                                                                    <span class="kt-badge kt-badge--primary  kt-badge--inline kt-badge--pill kt-font-bold">Success</span>&nbsp;<i class="fas fa-chevron-right fa-xs"></i>&nbsp;
                                                                @elseif($user_doing == "3")
                                                                    <span class="kt-badge kt-badge--warning  kt-badge--inline kt-badge--pill kt-font-bold">Pending</span>&nbsp;<i class="fas fa-chevron-right fa-xs"></i>&nbsp;
                                                                @elseif($user_doing == "1" && $users->callback == "2")
                                                                    <span class="kt-badge kt-badge--danger  kt-badge--inline kt-badge--pill kt-font-bold">Failed</span>&nbsp;<i class="fas fa-chevron-right fa-xs"></i>&nbsp;
                                                                @endif

                                                               <a href="{{ route('placement.detail', [$user->id_project , $user->id_room]) }}" class="kt-link kt-link--dark kt-label-font-color-3 kt-font-bold text-capitalize">{{$user->log->rooms[0]->name}}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <!--End::Section-->
                </div>
                <div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
                    <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Device Activities <small>latest event</small>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <p class="text-center"> No device activity so far, like empty space.</p>
                        <div class="kt-timeline-v2">

                            <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">

                                <!-- <div class="kt-timeline-v2__item">
                                    <span class="kt-timeline-v2__item-time">12:45</span>
                                    <div class="kt-timeline-v2__item-cricle">
                                        <i class="fa fa-genderless kt-font-success"></i>
                                    </div>
                                    <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
                                        RFID-221134 Powered On
                                    </div>
                                </div>
                                <div class="kt-timeline-v2__item">
                                    <span class="kt-timeline-v2__item-time">12:45</span>
                                    <div class="kt-timeline-v2__item-cricle">
                                        <i class="fa fa-genderless kt-font-success"></i>
                                    </div>
                                    <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
                                        RFID-221134 Powered On
                                    </div>
                                </div> -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>

    </div>




    <!-- end:: Content -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deletemodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" id="formDeleteProject" method="post" class="remove-record-model">
                    {{method_field('delete')}}
                    {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    All data related to this project will be deleted and can't be undone!
                    <input type="hidden", name="project_id" id="project_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" data-dismiss="modal" onclick="formProjectSubmit()" >Delete</button>
                </div>
            </form>
            </div>
        </div>
    </div>

@endsection

