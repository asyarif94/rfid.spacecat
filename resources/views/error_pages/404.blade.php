
<!DOCTYPE html>
<html lang="en" >

    <head>
        <meta charset="utf-8"/>

        <title>Uups.. | Spacecat</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--begin::Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
            <link href="{{ asset('css/pages/error/error-6.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
            <link rel="shortcut icon" href="{{ asset('media/logos/favicon.ico') }}" />

   </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body  class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >

	<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
		 <div class="kt-grid__item kt-grid__item--fluid kt-grid  kt-error-v6" style="background-image: url({{ asset('media/error/bg6.jpg') }});">
	<div class="kt-error_container">
		<div class="kt-error_subtitle kt-font-light">
			<h1>Owalah...</h1>
		</div>
		<p class="kt-error_description kt-font-light">
			Looks like something went wrong.<br>
			We're working on it
        </p>
        {{-- <p>{{$exception->getMessage() ?? ''}}</p> --}}
	</div>
</div>

				 	</div>

        <script>
            var KTAppOptions = {"colors":{"state":{"brand":"#374afb","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};
        </script>
    	    	   <script src="{{ asset('js/scripts.bundle.js')}}" type="text/javascript"></script>

            </body>

</html>
