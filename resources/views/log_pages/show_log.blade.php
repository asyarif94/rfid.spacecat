@extends('layouts.mainlayout')
@section('title', 'Log Data')
@section('content')

 <!-- begin:: Content Head -->
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">

                </h3>
                {{-- <span class="kt-subheader__separator kt-subheader__separator--v"></span> --}}
                <ul class="breadcrumb">
                    <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ route('projects.show', $id_projects) }}">Projects</a></li>
                    <li><a href="{{route('daftar.placement', $id_projects)}}">Placement</a></li>
                    <li><a href="{{route('placement.detail', [$id_projects, $room->id])}}">{{$room->name}}</a></li>
                    <li><a href="{{route('placement.detail', [$id_projects, $room->id])}}">Log</a></li>
                    <li>List</li>
                </ul>
            </div>
            <div class="kt-subheader__toolbar">
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Section-->
        <div class="row">
        <!--End::Section-->
            <div class="col-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon-signs-1"></i>
                        </span>
                            <h3 class="kt-portlet__head-title">
                            Log Data {{$room->name}}
                        </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">

                            </div>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
                        <!--begin::Section-->
                            <div class="kt-section__content">
                                    <table class="table table-hover table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>User</th>
                                                <th>Placement</th>
                                                <th>Device <small>[Name / Mac / IP Addr]</small></th>
                                                <th>Action User</th>
                                                <th>Status</th>
                                                <th>Result</th>
                                                <th>Add Info</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($getLogData->sortByDesc('created_at') as $log)
                                            <tr>
                                                <td>
                                                  <button  class=" btn btn-sm  btn-elevate btn-bold btn-upper" data-toggle="modal" data-target="#modalDetailLog"><i class="fas fa-caret-right"></i></button>
                                                </td>
                                                @if($log->id_user == null)
                                                    <td><span class="kt-font-danger kt-font-bold">Unknown User</span></td>
                                                @else
                                                    <td>
                                                        <a class="kt-link kt-font-bold" href="{{ route('lihat.user', [$log->id_project,$log->id_user]) }}">
                                                            <span class="kt-font-bolder">{{$log->old_user_name}}</span>
                                                        </a>

                                                    </td>
                                                @endif
                                                <td>
                                                    <a class="kt-link kt-font-bolder" href="{{route('placement.detail', [$log->id_project , $log->id_room]) }}">
                                                        <span class="kt-font-bolder">{{$log->old_room_name}}</span>
                                                    </a>

                                                </td>

                                                <td>{{$log->device->device_name}} \ {{$log->wifi_ssid}} \ {{$log->mac_address}} \ {{$log->ip_address}}</td>
                                                <td>
                                                    @if($log->user_doing == "Success Tap Card" || $log->user_doing == "Success")
                                                        <span class=" kt-font-bold">Tap Card</span>
                                                    @elseif($log->user_doing == "Rejected")
                                                        <span class=" kt-font-bold">Tap Card</span>
                                                    @elseif($log->user_doing == "Unsuccessful")
                                                        <span class=" kt-font-bold">Tap Card</span>
                                                    @endif
                                                </td>

                                                <td>
                                                    @if($log->callback == "User Found")
                                                        <span class="kt-badge kt-font-bold kt-badge--success kt-badge--inline">SUCCESS</span>
                                                    @elseif($log->callback == "Not in range time" || $log->callback == "Project Not Active" || $log->callback == "User Not Active" || $log->callback == "Room Not Active" || $log->callback == "Invalid Password")
                                                        <span class="kt-badge kt-font-bold kt-badge--warning kt-badge--inline">UNSUCCESSFUL</span>
                                                    @elseif($log->callback == "Waiting Password Confirmation")
                                                        <span class="kt-badge kt-font-bold kt-badge--primary kt-badge--inline">PENDING</span>
                                                    @elseif($log->id_user == null || $log->callback)
                                                        <span class="kt-badge kt-font-bold kt-badge--danger kt-badge--inline">REJECTED</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($log->callback == "User Found")
                                                        <span class="kt-font-bold">Access Granted</span>
                                                    @elseif($log->callback == "Not in range time")
                                                        <span class="kt-font-dark kt-font-bold">{{$log->callback}}</span>
                                                    @elseif($log->callback == "Rejected")
                                                        <span class="kt-font-dark kt-font-bold kt-font-danger">Don't have access to the room</span>
                                                    @elseif($log->callback == "Waiting Password Confirmation")
                                                        <span class="kt-font-dark kt-font-bold">Waiting Password Confirmation</span>
                                                    @elseif($log->callback == "Invalid Password")
                                                        <span class="kt-font-dark kt-font-bold">Invalid Password</span>
                                                    @else
                                                    <span class="kt-font-danger kt-font-bold">{{$log->callback}}</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    -
                                                </td>
                                                <td>
                                                    {{ Carbon\Carbon::parse($log->created_a)->format('D, d M Y ') }}
                                                </td>
                                                <td>
                                                    {{ Carbon\Carbon::parse($log->created_at)->format('H:i:s') }}
                                                </td>

                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                <div class="clearfix">
                                    <div class="float-left ml-2">
                                        Page :&nbsp;<span class="kt-font-brand kt-font-bold">{{$getLogData->currentPage()}}</span></br>
                                        Total log :&nbsp;<span class="kt-font-brand kt-font-bold">{{$getLogData->total()}}</span></br>
                                        Show per page :&nbsp;<span class="kt-font-brand kt-font-bold">{{$getLogData->perPage()}}</span>
                                    </div>
                                    <div class="float-right mr-2">
                                        {{$getLogData->links()}}
                                    </div>
                                </div>

                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetailLog" tabindex="-1" role="dialog" aria-labelledby="modalDetailLog" aria-hidden="true">
        <div class="modal-dialog modal-dialog modal-xl" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Log</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="ml-3 mb-3">
                    <h5 class="kt-font-boldest kt-font-primary">#1746328746</h5>
                    <h6 class="kt-font-bold kt-font-dark"> Kamar 1</h6>
                    <h6 class="kt-font-bold kt-font-dark"> Arif</h6>
                </div>
                <table class="table">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col">Time</th>
                        <th scope="col">Date</th>
                        <th scope="col">Action</th>
                        <th scope="col">Status</th>
                        <th scope="col">Result</th>
                        <th scope="col">Add Info</th>
                        <th scope="col">Device</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                      </tr>
                    </tbody>
                  </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-label-brand btn-bold" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        "use strict";



    </script>

@endsection
