<!--

   SSSSSSSSSSSSSSS PPPPPPPPPPPPPPPPP                  AAA                       CCCCCCCCCCCCCEEEEEEEEEEEEEEEEEEEEEE        CCCCCCCCCCCCC               AAA               TTTTTTTTTTTTTTTTTTTTTTT
 SS:::::::::::::::SP::::::::::::::::P                A:::A                   CCC::::::::::::CE::::::::::::::::::::E     CCC::::::::::::C              A:::A              T:::::::::::::::::::::T
S:::::SSSSSS::::::SP::::::PPPPPP:::::P              A:::::A                CC:::::::::::::::CE::::::::::::::::::::E   CC:::::::::::::::C             A:::::A             T:::::::::::::::::::::T
S:::::S     SSSSSSSPP:::::P     P:::::P            A:::::::A              C:::::CCCCCCCC::::CEE::::::EEEEEEEEE::::E  C:::::CCCCCCCC::::C            A:::::::A            T:::::TT:::::::TT:::::T
S:::::S              P::::P     P:::::P           A:::::::::A            C:::::C       CCCCCC  E:::::E       EEEEEE C:::::C       CCCCCC           A:::::::::A           TTTTTT  T:::::T  TTTTTT
S:::::S              P::::P     P:::::P          A:::::A:::::A          C:::::C                E:::::E             C:::::C                        A:::::A:::::A                  T:::::T
 S::::SSSS           P::::PPPPPP:::::P          A:::::A A:::::A         C:::::C                E::::::EEEEEEEEEE   C:::::C                       A:::::A A:::::A                 T:::::T
  SS::::::SSSSS      P:::::::::::::PP          A:::::A   A:::::A        C:::::C                E:::::::::::::::E   C:::::C                      A:::::A   A:::::A                T:::::T
    SSS::::::::SS    P::::PPPPPPPPP           A:::::A     A:::::A       C:::::C                E:::::::::::::::E   C:::::C                     A:::::A     A:::::A               T:::::T
       SSSSSS::::S   P::::P                  A:::::AAAAAAAAA:::::A      C:::::C                E::::::EEEEEEEEEE   C:::::C                    A:::::AAAAAAAAA:::::A              T:::::T
            S:::::S  P::::P                 A:::::::::::::::::::::A     C:::::C                E:::::E             C:::::C                   A:::::::::::::::::::::A             T:::::T
            S:::::S  P::::P                A:::::AAAAAAAAAAAAA:::::A     C:::::C       CCCCCC  E:::::E       EEEEEE C:::::C       CCCCCC    A:::::AAAAAAAAAAAAA:::::A            T:::::T
SSSSSSS     S:::::SPP::::::PP             A:::::A             A:::::A     C:::::CCCCCCCC::::CEE::::::EEEEEEEE:::::E  C:::::CCCCCCCC::::C   A:::::A             A:::::A         TT:::::::TT
S::::::SSSSSS:::::SP::::::::P            A:::::A               A:::::A     CC:::::::::::::::CE::::::::::::::::::::E   CC:::::::::::::::C  A:::::A               A:::::A        T:::::::::T
S:::::::::::::::SS P::::::::P           A:::::A                 A:::::A      CCC::::::::::::CE::::::::::::::::::::E     CCC::::::::::::C A:::::A                 A:::::A       T:::::::::T
 SSSSSSSSSSSSSSS   PPPPPPPPPP          AAAAAAA                   AAAAAAA        CCCCCCCCCCCCCEEEEEEEEEEEEEEEEEEEEEE        CCCCCCCCCCCCCAAAAAAA                   AAAAAAA      TTTTTTTTTTT
by : arif 2018,
Bandung, Indonesia;
-->
<!DOCTYPE html>
<html lang="en" >

    <head>
    <base href="../">
    <meta charset="utf-8"/>
    <title>@yield('title')</title>
    <meta name="description" content="iot data">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @include('layouts.partials.head')
</head>
    <body  class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
    @include('layouts.partials.header')
        <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                @yield('content')
            </div>
        </div>
    @include('layouts.partials.footer')
    @include('layouts.partials.footer-scripts')
    </body>
</html>
