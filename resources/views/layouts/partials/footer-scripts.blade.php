

<!--begin:: Global Mandatory Vendors -->
<script src="{{ asset('vendors/general/popper.js/dist/umd/popper.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/js-cookie/src/js.cookie.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/moment/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/tooltip.js/dist/umd/tooltip.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/sticky-js/dist/sticky.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/wnumb/wNumb.js') }}" type="text/javascript"></script>
<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->

<script src="{{ asset('vendors/general/jquery-form/dist/jquery.form.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/custom/js/vendors/bootstrap-datepicker.init.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/custom/js/vendors/bootstrap-timepicker.init.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/bootstrap-daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/bootstrap-select/dist/js/bootstrap-select.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/custom/js/vendors/bootstrap-switch.init.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/select2/dist/js/select2.full.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/dual-listbox/dist/dual-listbox.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/bootstrap-notify/bootstrap-notify.js')}}" type="text/javascript"></script>
<!--end:: GlobalOptional Vendors -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{ asset('js/scripts.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/multi.min.js') }}" type="text/javascript"></script>
<!--end::Global Theme Bundle -->

{{-- <script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('vendors/custom/gmaps/gmaps.js') }}" type="text/javascript"></script> --}}
<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
@stack('notify')
{{-- <script src="{{ asset('js/pages/dashboard.js') }}" type="text/javascript"></script> --}}
<!--end::Page Scripts -->

   {{-- <!--begin::Page Scripts(used by this page) -->
   <script src="{{ asset('js/pages/components/extended/dual-listbox.js') }}" type="text/javascript"></script>
   <!--end::Page Scripts --> --}}
  <!-- begin::Global Config(global config for global JS sciprts) -->
  <script type="text/javascript">

    @if(Session::has('success'))
        $.notify({
            title: 'Success',
            message: '  {!! session()->get('success') !!}',
        });
    @endif

    var KTAppOptions = {"colors":{"state":{"brand":"#374afb","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};

    // function deleteModal(id){
    //     var id = id;
    //     var url = '{{ route("projects.destroy", ":id")}}';
    //     url = url.replace(':id', id);
    //     $("#formDeleteuser").attr('action', url);
    // }

    // function formSubmit(){
    //     $("#formDeleteuser").submit();
    // }
    //     // Private functions



    $('#list_room').multi({
        'search_placeholder': 'Search Users ...',
        'non_selected_header': 'Users',
        'selected_header': 'Selected'
    });

    function deleteModal(id){
        var id = id;
        var url = '{{ route("projects.destroy", ":id")}}';
        url = url.replace(':id', id);
        $("#formDeleteProject").attr('action', url);
    }

    function formProjectSubmit(){
        $("#formDeleteProject").submit();
    }
/////////////////////
    function deleteModalUser(id){
        var id = id;
        var url = '{{ route("user.destroy", ":id")}}';
        url = url.replace(':id', id);
        $("#formDeleteUser").attr('action', url);
    }

    function formDeletetSubmit(){
        $("#formDeleteUser").submit();
    }
    ///////////////////////
    function deleteModalRoom(id){
        var id = id;
        var url = '{{ route("room.destroy", ":id")}}';
        url = url.replace(':id', id);
        $("#formDeleteUser").attr('action', url);
    }

    function formDeletetSubmit(){
        $("#formDeleteUser").submit();
    }
    //////////////////


    ////////////////
    //delete schedule
    function deletrule(id){
        var id = id;
        var url = '{{ route("delete.rules", ":id")}}';
        url = url.replace(':id', id);
        $("#formDeleteRule").attr('action', url);
    }

    function formDeleteRuleSubmit(){
        console.log("deleting");
        $("#formDeleteRule").submit();
    }


    //delete user parameter
    function deleteUserParameter(id){
        console.log(id);
        var id = id;
        var url = '{{ route("parameter.destroy", ":id")}}';
        url = url.replace(':id', id);
        console.log(url);
        $("#formDeleteParam").attr('action', url);
    }

    function formDeleteParamSubmit(){
        console.log("deleting");
        $("#formDeleteParam").submit();
    }

///////////////
    $('#time_end , #time_start').timepicker({
        minuteStep: 1,
        defaultTime: '0',
        showSeconds: false,
        showMeridian: false,
        snapToStep: true
    });


    $("#kt_datepicker_2, #kt_datepicker_2_validate").datepicker( {
         todayHighlight: !0, orientation: "bottom left",
         format: 'dd/M/yyyy'
        }
    ),
    $("#kt_datepicker_2_modal").datepicker( {
         todayHighlight: !0, orientation: "bottom left",
         format: 'dd/M/yyyy'
    });

    $("#kt_datetimepicker_6").datetimepicker( {
        format: "yyyy/mm/dd", todayHighlight: !0, autoclose: !0, startView: 2, minView: 2, forceParse: 0, pickerPosition: "bottom-left"
    });

        // Dual Listbox
    var listBoxes = $('.kt-dual-listbox');

    listBoxes.each(function(){
        var $this = $(this);
        var id = '#' + $this.attr('id');
        // get titles
        var availableTitle = ($this.attr('data-available-title') != null) ? $this.attr('data-available-title') : 'Available options';
        var selectedTitle = ($this.attr('data-selected-title') != null) ? $this.attr('data-selected-title') : 'Selected options';

        // get button labels
        var addLabel = ($this.attr('data-add') != null) ? $this.attr('data-add') : 'Add';
        var removeLabel = ($this.attr('data-remove') != null) ? $this.attr('data-remove') : 'Remove';
        var addAllLabel = ($this.attr('data-add-all') != null) ? $this.attr('data-add-all') : 'Add All';
        var removeAllLabel = ($this.attr('data-remove-all') != null) ? $this.attr('data-remove-all') : 'Remove All';

        // get options
        var options = [];
        $this.children('option').each(function(){
            var value = $(this).val();
            var label = $(this).text();
            var selected = ($(this).is(':selected')) ? true : false;
            options.push({ text: label, value: value, selected: selected });
        });

      // get search option
      var search = ($this.attr('data-search') != null) ? $this.attr('data-search') : "";

      // clear duplicates
      $this.empty();
            // init dual listbox
      let dualListBox = new DualListbox('#kt-dual-listbox',{
          addEvent: function(value) {
            //   console.log(value);
          },
          removeEvent: function(value) {
            //   console.log(value);
          },
          availableTitle: availableTitle,
          selectedTitle: selectedTitle,
          addButtonText: addLabel,
          removeButtonText: removeLabel,
          addAllButtonText: addAllLabel,
          removeAllButtonText: removeAllLabel,
          options: options
      });

      if (search == "false"){
        dualListBox.search.classList.add('dual-listbox__search--hidden');
      }
    });


</script>
      <!-- end::Global Config -->
