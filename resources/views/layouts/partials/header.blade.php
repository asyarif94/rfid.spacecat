
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
            <a href="{{route('dashboard')}}">
            <img alt="Logo" src="{{ asset('media/logos/logo-2.png') }}" />
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">

        <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
    </div>
</div>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
                <div class="kt-header__top">
                    <div class="kt-container ">
                        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                            <div class="kt-header__brand-logo">
                                <a href="{{route('dashboard')}}">
                                    <img alt="Logo" src="{{ asset('media/logos/logo-2-large.png') }}" class="kt-header__brand-logo-default" />
                                    <img alt="Logo" src="{{ asset('media/logos/logo-2.png') }}" class="kt-header__brand-logo-sticky" />
                                </a>
                            </div>
                            <div class="kt-header__brand-nav">
                                <div class="dropdown">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Your Projects
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-md">
                                        <ul class="kt-nav kt-nav--bold kt-nav--md-space">
                                            @forelse ($projects_ as $item)
                                                <li class="kt-nav__item">
                                                    <a class="kt-nav__link active" href="{{ route('projects.show', $item->id) }}">
                                                        <span class="kt-nav__link-icon"><i class="fas fa-angle-right"></i></span>
                                                        <span class="kt-nav__link-text text-uppercase">{{ $item->name }}
                                                            @if($item->status)
                                                            <span class="small text-muted">&nbsp;(Active)</span>
                                                            @else
                                                            <span class="small text-muted">&nbsp;(Inactive)</span>
                                                            @endif
                                                        </span>
                                                    </a>
                                                </li>
                                            @empty
                                            <li class="kt-nav__item">
                                                <span class=" ml-5" href="javascript:;">
                                                    <span class="text-muted">You don't have any project</span>
                                            </li>
                                            @endforelse
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kt-header__topbar">

                            <div class="kt-header__topbar-item kt-header__topbar-item--search dropdown kt-hidden-desktop" id="kt_quick_search_toggle">
                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
                                    <span class="kt-header__topbar-icon">

                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--info">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect id="bound" x="0" y="0" width="24" height="24"/>
                                                <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"/>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
                                    <div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" id="kt_quick_search_dropdown">
                                        <form method="get" class="kt-quick-search__form">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
                                                <input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
                                                <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
                                            </div>
                                        </form>
                                        <div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="325" data-mobile-height="200">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="kt-header__topbar-item dropdown">

                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                                    <form>

                                        <div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b" style="background-image: url({{ asset('media/bg-1.jpg') }} )">
                                            <h3 class="kt-head__title">
                                                User Notifications
                                                &nbsp;
                                                <span class="btn btn-success btn-sm btn-bold btn-font-md">23 new</span>
                                            </h3>

                                        </div>

                                    </form>
                                </div>
                            </div>

                            <div class="kt-header__topbar-item kt-header__topbar-item--quick-panel" data-toggle="kt-tooltip" title="Quick panel" data-placement="left">
                                <div class="kt-header__topbar-wrapper">
                                    <span class="kt-header__topbar-icon" id="kt_quick_panel_toggler_btn">
                                        <!--<i class="flaticon2-cube-1"></i>-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect id="bound" x="0" y="0" width="24" height="24"/>
                                                    <rect id="Rectangle-7" fill="#000000" x="4" y="4" width="7" height="7" rx="1.5"/>
                                                    <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" id="Combined-Shape" fill="#000000" opacity="0.3"/>
                                                </g>
                                        </svg>
                                    </span>
                                </div>
                            </div>

                            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px" aria-expanded="false">
                                    <span class="kt-header__topbar-welcome">Hi,</span>
                                    <span class="kt-header__topbar-username">
                                        {{ auth()->user()->name}}
                                    </span>


                                    <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden-">{{auth()->user()->name[0] }}</span>
                                </div>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                                    <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({{ asset('media/bg/bg-4.jpg') }}">
                                        <div class="kt-user-card__avatar">
                                            <img class="kt-hidden" alt="Pic" src="{{ asset('media/users/user-default.jpg') }}" />

                                            <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{auth()->user()->name[0] }}</span>
                                        </div>
                                        <div class="kt-user-card__name">
                                            {{ auth()->user()->name}}
                                        </div>
                                        <div class="kt-user-card__badge">
                                            <span class="btn btn-success btn-sm btn-bold btn-font-md">0 messages</span>
                                        </div>
                                    </div>

                                    <div class="kt-notification">
                                        <a href="{{route('user.setting')}}" class="kt-notification__item">
                                            <div class="kt-notification__item-icon">
                                                <i class="flaticon2-calendar-3 kt-font-success"></i>
                                            </div>
                                            <div class="kt-notification__item-details">
                                                <div class="kt-notification__item-title kt-font-bold">
                                                    My Profile
                                                </div>
                                                <div class="kt-notification__item-time">
                                                    Account settings
                                                </div>
                                            </div>
                                        </a>
                                        @if(auth()->user()->level->name_level == 'Administrator')
                                        <a href="#" class="kt-notification__item">
                                            <div class="kt-notification__item-icon">
                                                <i class="flaticon-black kt-font-primary"></i>
                                            </div>
                                            <div class="kt-notification__item-details">
                                                <div class="kt-notification__item-title kt-font-bold">
                                                    Administrator
                                                </div>
                                                <div class="kt-notification__item-time">
                                                    Manage & Analyze the App's
                                                </div>
                                            </div>
                                        </a>
                                        @else
                                        @endif
                                        <div class="kt-notification__custom kt-space-between">
                                            <a href="{{ route('logout') }}" class="btn btn-label btn-label-brand btn-sm btn-bold"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Sign Out</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                            {{-- <a href="demo2/custom/user/login-v2.html" target="_blank" class="btn btn-clean btn-sm btn-bold">Upgrade Plan</a> --}}
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="kt-header__bottom">
                        <div class="kt-container ">

                            <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                            <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                                <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                                    <ul class="kt-menu__nav ">
                                        <li class=" kt-menu__item "><a href="https://github.com/AsyaSyarif/RFID-Spacecat" target="_blank" class="kt-menu__link"><span class="kt-menu__link-text">Github</span></a></li>
                                        <li class=" kt-menu__item "><a href="" class="kt-menu__link"><span class="kt-menu__link-text">API</span></a></li>
                                        <li class="kt-menu__item kt-menu__item--here kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a href="javascript:void(0)" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Applications&nbsp;&nbsp;<i class="fas fa-caret-down"></i></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                            <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                                <ul class="kt-menu__subnav">
                                                    <li class="kt-menu__item  kt-menu__item--submenu" data-ktmenu-submenu-toggle="hover" aria-haspopup="true">
                                                        <a href="{{route('dashboard')}}" class="kt-menu__link">
                                                            <span"><i class=" kt-menu__link-bullet flaticon2-menu-2"></i></span">
                                                            <span class="kt-menu__link-text">&nbsp;My Projects</span>
                                                        </a>
                                                    </li>
                                                    @if(request()->is('dashboard') || request()->is('project/add') || request()->is('profile'))
                                                    @else
                                                    <li class="kt-menu__item  kt-menu__item--submenu" data-ktmenu-submenu-toggle="hover" aria-haspopup="true">
                                                        <a href="{{url('/projects', $id_projects)}}" class="kt-menu__link">
                                                            <span><i class=" kt-menu__link-bullet flaticon2-dashboard"></i></span>
                                                            <span class="kt-menu__link-text">&nbsp;Dashboard</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-menu__item  kt-menu__item--submenu" data-ktmenu-submenu-toggle="hover" aria-haspopup="true">
                                                        <a href="javascript::void(0)" class="kt-menu__link kt-menu__toggle">
                                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                                            <span class="kt-menu__link-text">Users</span>
                                                            <i class="kt-menu__hor-arrow la la-angle-right"></i>
                                                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                                        </a>
                                                        <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
                                                            <ul class="kt-menu__subnav">
                                                            @if(getTotaluserCreated($id_projects) >= countPlanuser())
                                                                <span class="kt-menu__item" aria-haspopup="true"><a href="{{route('user.list', $id_projects)}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Users</span></a></span>
                                                            @else
                                                                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('tambah.user', $id_projects)}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Add New</span></a></li>
                                                                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('user.list', $id_projects)}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Users</span></a></li>
                                                            @endif
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="kt-menu__item  kt-menu__item--submenu" data-ktmenu-submenu-toggle="hover" aria-haspopup="true">
                                                        <a href="javascript::void(0)" class="kt-menu__link kt-menu__toggle">
                                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                                            <span class="kt-menu__link-text">Placements</span>
                                                            <i class="kt-menu__hor-arrow la la-angle-right"></i>
                                                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                                        </a>
                                                        <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
                                                            <ul class="kt-menu__subnav">
                                                            @if(getTotalroomCreated($id_projects) >= countPlanRoom())
                                                                <span class="kt-menu__item" aria-haspopup="true"><a href="{{route('daftar.placement', $id_projects)}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Placements</span></a></span>
                                                            @else
                                                                <li class="kt-menu__item" aria-haspopup="true"><a href="{{route('placement.addnew', $id_projects)}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Add New</span></a></li>
                                                                <li class="kt-menu__item" aria-haspopup="true"><a href="{{route('daftar.placement', $id_projects)}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Placements</span></a></li>
                                                            @endif
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="kt-menu__item  kt-menu__item--submenu" data-ktmenu-submenu-toggle="hover" aria-haspopup="true">
                                                        <a href="javascript::void(0)" class="kt-menu__link kt-menu__toggle">
                                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                                            <span class="kt-menu__link-text">Manage</span>
                                                            <i class="kt-menu__hor-arrow la la-angle-right"></i>
                                                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                                        </a>
                                                        <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
                                                            <ul class="kt-menu__subnav">
                                                                <span class="kt-menu__item" aria-haspopup="true"><a href="{{route('parameter.list', $id_projects)}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Parameters</span></a></span>
                                                                <span class="kt-menu__item" aria-haspopup="true"><a href="{{route('list.device', $id_projects)}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Devices</span></a></span>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
            </div>



