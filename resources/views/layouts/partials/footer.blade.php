	<!-- begin:: Footer -->

<div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer">
		<div class="kt-footer__top">
		<div class="kt-container ">
			<div class="row">
				<div class="col-lg-4">
					<div  class="kt-footer__section">
						<h3 class="kt-footer__title">About</h3>
						<div class="kt-footer__content">
											I'm Arif, i deciding to making this web app and arduino libraries to public with a bit hope that all this can be useful for many people.
											<br>
											So, any help or contribution will be appreciated. <a href="https://github.com/AsyaSyarif/RFID-Spacecat/issues" target="_blank">This way</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div  class="kt-footer__section">
						<h3 class="kt-footer__title">Quick Links</h3>
						<div class="kt-footer__content">
							<div class="kt-footer__nav">
								<div class="kt-footer__nav-section">
									<a href="#">Documentation</a>
									<a href="https://github.com/AsyaSyarif/RFID-Spacecat">Library</a>
									<a href="#">API</a>
								</div>
								<div class="kt-footer__nav-section">
									<a href="https://github.com/AsyaSyarif/RFID-Spacecat/issues">Report Bug</a>
									<a href="#">Buy Me A Coffee</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div  class="kt-footer__section">
						<h3 class="kt-footer__title">Get In Touch</h3>
						<div class="kt-footer__content">
							<form action="" class="kt-footer__subscribe">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Enter Your Email">
									<div class="input-group-append">
										<button class="btn btn-brand" type="button">Join</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-footer__bottom">
	<div class="kt-container ">
		<div class="kt-footer__wrapper">
			<div class="kt-footer__logo">
				<a href="demo2/index.html">
					<img alt="Logo" src="{{ asset('media/logos/logo-2.png') }}">
				</a>
				<div class="kt-footer__copyright">
                    <?php
                    $fromYear = 2018;
                    $thisYear = (int)date('Y');
                    echo $fromYear . (($fromYear != $thisYear) ? '-' . $thisYear : '');?>
					&nbsp;&copy;&nbsp;
					<span>Developed and Maintained with&nbsp;</span class="hred">❤<span>&nbsp;by </span>
					<a href="https://www.asyarif.net" target="_blank">Arif</a>
				</div>
			</div>
			<div class="kt-footer__menu">
				<a href="http://t.me/asya_syarif" target="_blank">Telegram</a>
				<a href="https://github.com/AsyaSyarif/RFID-Spacecat" target="_blank">GitHub</a>
				<a href="#" class="text-muted" target="_blank">App Version beta</a>
			</div>
		</div>
	</div>
</div>

</div><!-- end:: Page -->
</div>
</div>
