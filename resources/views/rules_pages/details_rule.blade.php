
@extends('layouts.mainlayout')
@section('title', 'Dashboard')
@section('content')
       
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">

                <h3 class="kt-subheader__title">
                <?php echo $data['subheader_title'] ?>                 
                </h3>

                <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                <div class="kt-subheader__group" id="kt_subheader_search">
                    <span class="kt-subheader__desc" id="kt_subheader_total">
                        54 Total                            
                    </span>

                    <form class="kt-margin-l-20" id="kt_subheader_search_form">
                        <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect id="bound" x="0" y="0" width="24" height="24"/>
                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"/>
                                        </g>
                                    </svg>                                    <!--<i class="flaticon2-search-1"></i>-->
                                </span>
                            </span>
                        </div>
                    </form>
                </div>

                <div class="kt-subheader__group kt-hidden" id="kt_subheader_group_actions">

                    <div class="kt-subheader__desc"><span id="kt_subheader_group_selected_rows"></span> Selected:</div>

                    <div class="btn-toolbar kt-margin-l-20">
                        <div class="dropdown" id="kt_subheader_group_actions_status_change">
                            <button type="button" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown">
                                Update Status
                            </button>
                            <div class="dropdown-menu">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">Change status to:</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="1">
                                            <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-success kt-badge--inline kt-badge--bold">Approved</span></span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="2">
                                            <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--bold">Rejected</span></span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="3">
                                            <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--bold">Pending</span></span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="4">
                                            <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-info kt-badge--inline kt-badge--bold">On Hold</span></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <button class="btn btn-label-success btn-bold btn-sm btn-icon-h" id="kt_subheader_group_actions_fetch" data-toggle="modal" data-target="#kt_datatable_records_fetch_modal">
                            Fetch Selected
                        </button>
                        <button class="btn btn-label-danger btn-bold btn-sm btn-icon-h" id="kt_subheader_group_actions_delete_all">
                            Delete All
                        </button>
                    </div>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="dropdown">
                    	<button type="button" class="btn btn-sm btn-primary">Add / Edit</button>
                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       Rule Kostan 1
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      
                        <a class="dropdown-item" href="#" data-toggle="kt-tooltip" title="Tooltip title" data-placement="right" data-skin="dark" data-container="body">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#" data-toggle="kt-tooltip" title="Tooltip title" data-placement="left">Something else here</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-2">
                <div class="kt-portlet">     
                    <div class="kt-portlet__body">
                        <div class="kt-section__content kt-section__content--border kt-section__content--fit">
                            <ul class="kt-nav kt-nav--active-bg" id="kt_nav" role="tablist">
                                <li class="kt-nav__item kt-nav__item--active">
                                <a href="{{url('/projects', $id)}}" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon-analytics"></i>
                                    <span class="kt-nav__link-text">Overview</span>
                                </a>
                                </li>
                                <li class="kt-nav__item ">
                                    <a class="kt-nav__link" role="tab" id="kt_nav_link_1" data-toggle="collapse" href="#kt_nav_sub_1" aria-expanded=" false">
                                        <i class="kt-nav__link-icon flaticon-users"></i>
                                        <span class="kt-nav__link-text">Users</span>
    
                                        <span class="kt-nav__link-arrow"></span>
                                    </a>
                                    <ul class="kt-nav__sub collapse" id="kt_nav_sub_1" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#kt_nav">
                                        <li class="kt-nav__item">
                                            <a href="{{url('/adduser', $id)}}" class="kt-nav__link">
                                                <span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
                                                <span class="kt-nav__link-text">New</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="{{url('/users', $id)}}" class="kt-nav__link">
                                                <span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
                                                <span class="kt-nav__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="kt-nav__item ">
                                    <a class="kt-nav__link" role="tab" id="kt_nav_link_1" data-toggle="collapse" href="#kt_nav_sub_2" aria-expanded=" false">
                                        <i class="kt-nav__link-icon flaticon-home"></i>
                                        <span class="kt-nav__link-text">Room</span>
    
                                        <span class="kt-nav__link-arrow"></span>
                                    </a>
                                    <ul class="kt-nav__sub collapse" id="kt_nav_sub_2" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#kt_nav">
                                        <li class="kt-nav__item">
                                        <a href="{{url('/addroom', $id)}}" class="kt-nav__link">
                                                <span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
                                                <span class="kt-nav__link-text">New</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="{{url('/room', $id)}}" class="kt-nav__link">
                                                <span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
                                                    <span class="kt-nav__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="kt-nav__item ">
                                    <a class="kt-nav__link" role="tab" id="kt_nav_link_1" data-toggle="collapse" href="#kt_nav_sub_3" aria-expanded=" false">
                                        <i class="kt-nav__link-icon flaticon-layers"></i>
                                        <span class="kt-nav__link-text">Rules</span>
    
                                        <span class="kt-nav__link-arrow"></span>
                                    </a>
                                    <ul class="kt-nav__sub collapse" id="kt_nav_sub_3" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#kt_nav">
                                        <li class="kt-nav__item">
                                            <a href="{{url('/addrule', $id)}}" class="kt-nav__link">
                                                <span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
                                                <span class="kt-nav__link-text">New</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="{{url('/rules', $id)}}" class="kt-nav__link">
                                                <span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
                                                <span class="kt-nav__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="{{url('/devices')}}" class="kt-nav__link">
                                        <i class="kt-nav__link-icon flaticon2-notepad"></i>
                                        <span class="kt-nav__link-text">Devices</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="{{url('/devices')}}" class="kt-nav__link">
                                        <i class="kt-nav__link-icon flaticon2-paper"></i>
                                        <span class="kt-nav__link-text">Logs</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon flaticon2-gear"></i>
                                        <span class="kt-nav__link-text">Settings</span>
                                    </a>
                                </li>
                            </ul>
                        </div>     
                    </div>
                </div>
            </div>
            <div class="col-lg-10">
                <div class="kt-portlet kt-portlet--tabs">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-toolbar">
                            <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_1_3_tab_content" role="tab">
                                        <i class="flaticon-home" aria-hidden="true"></i> Access
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_3_2_tab_content" role="tab">
                                        <i class="flaticon-calendar-with-a-clock-time-tools" aria-hidden="true"></i>Schedule
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_3_3_tab_content" role="tab">
                                        <i class="flaticon-price-tag" aria-hidden="true"></i>Amount
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kt-portlet__body">                   
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_portlet_base_demo_1_3_tab_content" role="tabpanel">
                                <span class="float-left">  
                                    <div class="kt-section__desc">
                                            Here is a list for users who have access to the room <a href=""><strong>nama ruangan</strong></a>.
                                    </div>
                                </span>
                              
                                <br>
                                <div class="kt-datatable" id="rules"></div>
                            </div>
                            <div class="tab-pane" id="kt_portlet_base_demo_3_2_tab_content" role="tabpanel">
                               
                            </div>
                            <div class="tab-pane" id="kt_portlet_base_demo_3_3_tab_content" role="tabpanel">
                                
                            </div>
                        </div>      
                    </div>
                </div>
		    </div>
        </div>    
    </div>
    <!-- end:: Content -->
    <script>
            "use strict";
        // Class definition
        
              $(document).ready(function() {
               var  datatable = $('#rules').KTDatatable({
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: '{{ route('datatable.getrule') }}',
                      method: 'GET',
                    },
                  },
                  pageSize: 10, // display 20 records per page
                  serverPaging: true,
                  serverFiltering: true,
                  serverSorting: true,
                },
                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                        footer: false, // display/hide footer
                    },
                    // column sorting
                    sortable: true,
                    pagination: true,
                    search: {
                        input: $('#cariUser'),
                        delay: 400,
                    },
              columns: [{
                  field: '#',
                  title: '#',
                  sortable: false,
                  width: 5,
                  selector: {
                    class: 'kt-checkbox--solid'
                        },
                          textAlign: 'center', 
                },{
                    field: 'user_rfid',
                    title: 'Name',
                    sortable: false,
                    width: 100,
                    template: function(row) {
                      return row.user_rfid;
                    },
                    textAlign: 'center', 
                },{
                    field: 'last_taping',
                    title: 'Last Tapping',
                    sortable: false,
                    width: 100,
                    textAlign: 'center',
                    type: 'date',
                            format: 'MM/DD/YYYY',
                }, {
                    field: 'created_at',
                    title: 'Created',
                    sortable: false,
                    width: 100,
                    textAlign: 'center',
                    type: 'date',
                            format: 'MM/DD/YYYY',
                },{
                  field: "Actions",
                  width: 80,
                  title: "Actions",
                  sortable: false,
                  autoHide: false,
                  overflow: 'visible',
                  template: function(row) {
                      console.log(row.id);
                    return '\
                        <div class="dropdown">\
                          <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">\
                            <i class="flaticon-more-1"></i>\
                          </a>\
                          <div class="dropdown-menu dropdown-menu-right">\
                            <ul class="kt-nav">\
                              <li class="kt-nav__item">\
                                <a href="{{ url('rules') }}/'+row.room_secret_key+'" class="kt-nav__link">\
                                  <i class="kt-nav__link-icon flaticon2-expand"></i>\
                                  <span class="kt-nav__link-text">View</span>\
                                </a>\
                              </li>\
                              <li class="kt-nav__item">\
                                <a href="#" class="kt-nav__link">\
                                  <i class="kt-nav__link-icon flaticon2-contract"></i>\
                                  <span class="kt-nav__link-text">Edit</span>\
                                </a>\
                              </li>\
                              <li class="kt-nav__item">\
                                <a href="#" class="kt-nav__link">\
                                  <i class="kt-nav__link-icon flaticon2-trash"></i>\
                                  <span class="kt-nav__link-text">Delete</span>\
                                </a>\
                              </li>\
                            </ul>\
                          </div>\
                        </div>\
                      ';
                  }, 
                }]
            });
        });
    </script>
@endsection