
@extends('layouts.mainlayout')
@section('title', 'Dashboard')
@section('content')

    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">

                
            </div>
            <div class="kt-subheader__toolbar">

            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="row">
            <!-- Isi Content disini -->
                <div class="col-lg-12">
                    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Add Schedule<small>Fill out forms below</small></h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                            <a href="{{ url()->previous() }}" class="btn btn-clean btn-bold kt-margin-r-10">
                                    <i class="la la-arrow-left"></i>
                                    <span class="kt-hidden-mobile">Back</span>
                                </a>
                                <div class="btn-group">
                                    <button type="button" id="btnSaveRule" class="btn btn-brand btn-bold" onclick="addSubmit()">
                                        <i class="la la-check"></i> 
                                        <span class="kt-hidden-mobile">Save</span>
                                    </button>
                                    <button type="button" class="btn btn-brand dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-edit-interface-symbol-of-pencil-tool"></i>
                                                    <span class="kt-nav__link-text">Save & edit</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <span class="kt-nav__link-text">Save & add new</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                            @endif
                            <form class="kt-form" id="addrule" method="post" action="{{ route('rule.store') }}">
                                @method('POST')
                                {{ csrf_field() }}
                            
                                <div class="row">
                                    <div class="col-xl-2"></div>
                                    <div class="col-xl-9">
                                        <!--begin::Portlet-->
                            
                                        <!--end::Portlet-->
                                    </div>
                                    <div class="col-xl-2"></div>
                                </div>
                            </form>
                        </div>
                    </div>	
            <!--end::Portlet-->
            </div>
        </div>
    <!--End::Section-->
    </div>
    <!-- end:: Content -->

        <!--begin::Modal-->
        <div class="modal fade" id="scheduleModal" name="scheduleModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Schedule</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="la la-remove"></span>
                        </button>
                    </div>
                    <form class="kt-form kt-form--fit kt-form--label-right">
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="example-number-input" class="col-3 col-form-label">Name</label>
                                <div class="col-8">
                                    <input class="form-control" type="text" value="" placeholder="Enter Schedule Name" name="name_schedule" id="name_schedule">
                                </div>
                            </div>
                            <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Day</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" id="dayschedule" name="dayschedule">
                                            <option value="none" selected disabled hidden>  Select Day </option> 
                                            <option value="Monday">Monday</option>
                                            <option value="Tuesday">Tuesday</option>
                                            <option value="Wednesday">Wednesday</option>
                                            <option value="Thrusday">Thrusday</option>
                                            <option value="Friday">Friday</option>
                                            <option value="Saturday">Saturday</option>
                                            <option value="Sunday">Sunday</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="kt-divider mb-4"><span></span>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Time Start</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                    <div class="input-group timepicker">
                                        <input class="form-control" id="time_start" name="time_start" readonly placeholder="Select time" type="text"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">format :  hh/mm</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Time End</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                    <div class="input-group timepicker">
                                        <input class="form-control" id="time_end" name="time_end" readonly placeholder="Select time" type="text"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-clock-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">format :  hh/mm</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-3 col-form-label">Offset Time</label>
                                <div class="col-8">
                                    <input class="form-control" type="number" name="offset_time" id="offset_time" value="0" placeholder="e.g. 15" id="example-number-input">
                                    <span class="form-text text-muted">format :  minutes</span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" id="applySchedule" class="btn btn-brand">Apply</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--end::Modal-->
@endsection