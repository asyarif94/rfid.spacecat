
@extends('layouts.mainlayout')
@section('title', 'Dashboard')
@section('content')

    <div class="kt-container  kt-grid__item kt-grid__item--fluid mt-1">
        <div class="row">
        <!-- Isi Content disini -->
            <div class="col-lg-3"></div>
            <div class="col-lg-7">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">

                    </h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <ul class="breadcrumb mb-3">
                        <li ><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li ><a href="{{route('dashboard')}}">Projects</a></li>
                        <li><a href="{{route('daftar.placement', $id_projects )}}">Placement</a></li>
                        <li><a href="{{route('placement.detail', [$id_projects, $id_room])}}">{{$room->name}}</a></li>
                        <li><a class="text-capitalize" href="{{route('show.rule', [$id_projects, $id_room, $rule->id])}}">{{$rule->rule_name}}</a></li>
                        <li>Edit</li>
                    </ul>
                </div>
                <div class="kt-portlet kt-portlet--tabs">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Rules <small>{{$room->name}}</small></h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab_general" role="tab">
                                        General
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab_permission" role="tab">
                                        Permission
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab_schedule" role="tab">
                                        Schedule
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab_user_param" role="tab">
                                        User
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form class="kt-form" id="updaterule" method="post" action="{{route('update.rules', [$id_projects, $id_room, $rule->id])}}">
                            @method('PUT')
                            @csrf
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-12">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_general" role="tabpanel">
                                            <div class="kt-section kt-section--first">
                                                <div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                        <label for="rule_name" class="col-3 col-form-label">Rule Name :</label>
                                                        <div class="col-8">
                                                            <input class="form-control" name="rule_name" type="text" placeholder="Enter Rule Name" value="{{$rule->rule_name}}" id="rule_name">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="rule_name" class="col-3 col-form-label">Sub Name :</label>

                                                        <div class="col-8">
                                                            <input class="form-control" name="sub_name" type="text" placeholder="Sub Name" value="{{$rule->sub_name}}" id="rule_sub_name">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label" for="statusrule">Status :</label>
                                                        <div class="col-8">
                                                            <span class="kt-switch">
                                                                <label>
                                                                    <input type="checkbox" value='1' @if($rule->status == 1){{'checked'}} @endif name="statusrule"/>
                                                                    <span></span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    {{-- <div class="kt-divider mb-4"><span></span></div> --}}

                                                    {{-- <div class="kt-divider mb-4"><span></span></div> --}}
                                                    <div class="form-group form-group-last row">
                                                        <label class="col-3 col-form-label">Description</label>
                                                        <div class="col-8">
                                                            <textarea  name="description" class="form-control" id="description" rows="5">{{$rule->description}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_schedule" role="tabpanel">
                                            <div class="kt-section kt-section--first">
                                                <div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                        <div class="col-3"></div>
                                                        <div class="col-8">
                                                            <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                                <input type="checkbox" name="en_schedule"
                                                                @if($rule->enable_schedule)
                                                                checked
                                                                @else
                                                                @endif
                                                                > Enable
                                                                <span></span>
                                                            </label>
                                                            <span class="form-text text-muted">Set the user's access time by enabling this rule.</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Day</label>
                                                        <div class="col-lg-8">
                                                            <select class="form-control" id="day" name="day">

                                                                <option value="none" disabled hidden  @if ($rule->day == "Everyday") {{ 'selected' }} @endif>None</option>
                                                                <option @if ($rule->day == "Everyday") {{ 'selected' }} @endif value="everyday">Everyday</option>
                                                                <option @if ($rule->day == "sunday") {{ 'selected' }} @endif value="sunday">Sunday</option>
                                                                <option @if ($rule->day == "monday") {{ 'selected' }} @endif value="monday">Monday</option>
                                                                <option @if ($rule->day == "tuesday") {{ 'selected' }} @endif value="tuesday">Tuesday</option>
                                                                <option @if ($rule->day == "wednesday") {{ 'selected' }} @endif value="wednesday">Wednesday</option>
                                                                <option @if ($rule->day == "thrusday") {{ 'selected' }} @endif value="thrusday">Thrusday</option>
                                                                <option @if ($rule->day == "friday") {{ 'selected' }} @endif value="friday">Friday</option>
                                                                <option @if ($rule->day == "saturday") {{ 'selected' }} @endif value="saturday">Saturday</option>
                                                            </select>
                                                        </div>
                                                </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Time Start - End</label>
                                                        <div class="col-4">
                                                            <div class="input-group timepicker">
                                                                <input class="form-control" id="time_start" name="time_start" readonly placeholder="Select time" value="{{$rule->start_time}}" type="text"/>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-clock-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <span class="form-text text-muted">format :  hh/mm</span>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="input-group timepicker">
                                                                <input class="form-control" id="time_end" name="time_end" readonly value="{{$rule->end_time}}" placeholder="Select time" type="text"/>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-clock-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <span class="form-text text-muted">format :  hh/mm</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Offset</label>
                                                       <div class="col-4">
                                                       <input class="form-control" type="number" name="offset_time" id="offset_time" min="0" value="{{$rule->offset_time}}" placeholder="e.g: 15" id="example-number-input">
                                                           <span class="form-text text-muted">format :  minutes</span>
                                                       </div>
                                                   </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_permission" role="tabpanel">
                                            <div class="kt-section kt-section--first">
                                                <div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Permission :</label>
                                                        <div class="col-8">
                                                            <div class="kt-margin-t-10 mb-4">
                                                                <span>As you can see the two box below, the left box is all users you have and the other is used to provide access.<br></span>
                                                                <br>
                                                                <span class="paragraf-text-mt-2"> by clicking username you provide a user to access to this placement <a href="{{route('placement.detail', [$id_projects, $room->id])}}" class="kt-link">{{$room->name}}</a>.</span>
                                                            </div>
                                                            <select multiple id="list_room" name="permission[]" class="form-control">
                                                                @foreach ($list_user as $key => $value)
                                                                    <option value={{$key}} {{ in_array($key, $getselectedUser) ? 'selected' : ''}}>{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="tab_user_param" role="tabpanel">
                                            <div class="kt-section kt-section--first">
                                                <div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                        <div class="col-3"></div>
                                                        <div class="col-8">
                                                            <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                                <input type="checkbox" name="en_uparam" value="1"
                                                                @if(isset($rule->conectorparameter->enable))
                                                                    checked
                                                                @else
                                                                @endif
                                                                > Enable
                                                                <span></span>
                                                            </label>
                                                            <span class="form-text text-muted">Set the user's parameters by enabling for this rule.</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Parameters</label>
                                                        <div class="col-8">
                                                            <select class="form-control" id="parameter" name="parameter">
                                                                <option  disabled hidden>Choose here . . .</option>
                                                                    @foreach ($project->parameters as $key => $value)
                                                                        @if(isset($rule->conectorparameter->user_parameters_id))
                                                                            <option value="{{$value->id}}"> {{$value->name}}</option>
                                                                        @else
                                                                            <option value="{{$value->id}}"
                                                                                @if(isset($value->id, $rule->conectorparameter->parameter_id))
                                                                                    {{'selected'}}
                                                                                 @else
                                                                                 @endif>
                                                                                    {{ $value->name }}
                                                                            </option>
                                                                        @endif
                                                                    @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Operator</label>
                                                        <div class="col-3">
                                                            <select class="form-control" id="operator" name="operator">
                                                                <option selected disabled hidden>-</option>
                                                                @if(isset($rule->conectorparameter))
                                                                    <option value="add" @if($rule->conectorparameter->operator == 'add') {{ 'selected' }} @endif >[+] Addition</option>
                                                                    <option value="sub" @if($rule->conectorparameter->operator == 'sub') {{ 'selected' }} @endif>[-] Subtraction</option>
                                                                    <option value="perctpls" @if($rule->conectorparameter->operator == 'perctpls') {{ 'selected' }} @endif>[%] Percent++</option>
                                                                    <option value="perctmin" @if($rule->conectorparameter->operator == 'perctmin') {{ 'selected' }} @endif>[%] Percent--</option>
                                                                    <option value="replc" @if($rule->conectorparameter->operator == 'replc') {{ 'selected' }} @endif>[X] Replace</option>
                                                                @else
                                                                    <option value="add">[+] Addition</option>
                                                                    <option value="sub">[-] Subtraction</option>
                                                                    <option value="perctpls">[%] Percent++</option>
                                                                    <option value="perctmin">[%] Percent--</option>
                                                                    <option value="replc">[=] Equals</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <label for="by_value" class="col-1 col-form-label">By</label>

                                                        <div class="col-4">
                                                            <input class="form-control" name="by_value" type="text" placeholder="Enter the value"
                                                                @if(isset($rule->conectorparameter))
                                                                    value= "{{$rule->conectorparameter->by_value}}"
                                                                @else
                                                                    value= ""
                                                                @endif
                                                                 id="by_value">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Min - Max Value</label>
                                                        <div class="col-4">
                                                            <div class="input-group">
                                                                <div class="input-group-append"><span class="input-group-text" id="unit">Min</span></div>
                                                                @if(empty($rule->conectorparameter->min_value))
                                                                    <input type="text" name="min_value" class="form-control" placeholder="Min Value" aria-describedby="unit" value="{{ old('min_value', 0) }}">
                                                                @else
                                                                    <input type="text" name="min_value" class="form-control" placeholder="Min Value" aria-describedby="unit" value="{{ old('min_value', $rule->conectorparameter->min_value) }}">
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="input-group">
                                                                <div class="input-group-append"><span class="input-group-text" id="unit">Max</span></div>
                                                                @if(empty($rule->conectorparameter->max_value))
                                                                    <input type="text" name="max_value" class="form-control" placeholder="Max Value" aria-describedby="unit" value="{{ old('max_value', 0) }}">
                                                                @else
                                                                    <input type="text" name="max_value" class="form-control" placeholder="Max Value" aria-describedby="unit" value="{{ old('max_value', $rule->conectorparameter->max_value) }}">
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-divider mb-4"><span></span></div>
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-3"></div>
                                                <div class="col-9 col-6">
                                                    <button href="#" id="btnSaveRule" class="btn btn-label-brand btn-bold" onclick="updateSubmit()">Save Changes</button>
                                                    <button href="#" class="btn btn-clean btn-bold">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </form>

                     </div>
                <!--end::Portlet-->
                </div>
            </div>
        <!--End::Section-->
        </div>
    </div>


	<script type = "text/javascript">
    "use strict";

    function updateSubmit(){
            $('#btnSaveRule').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $('#updaterule').submit();
        }
    </script>
@endsection
