
@extends('layouts.mainlayout')
@section('title', 'Add Rules')
@section('content')

    <div class="kt-container  kt-grid__item kt-grid__item--fluid mt-1">
        <div class="row">
        <!-- Isi Content disini -->
            <div class="col-lg-3"></div>
            <div class="col-lg-7">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">

                    </h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <ul class="breadcrumb mb-3">
                        <li ><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li ><a href="{{route('dashboard')}}">Projects</a></li>
                        <li><a href="{{route('placement.detail', [$id_projects, $room->id])}}">{{$room->name}}</a></li>
                        <li>Add Rule</li>
                    </ul>
                </div>

                <div class="kt-portlet kt-portlet--tabs">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Rules <small>{{$room->name}}</small></h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <ul class="nav nav-tabs nav-tabs-bold nav-tabs-line  nav-tabs-line-right nav-tabs-line-brand" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab_general" role="tab">
                                        General
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab_permission" role="tab">
                                        Permission
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab_schedule" role="tab">
                                        Schedule
                                    </a>
                                </li>
                                @if($room->enable_parameters)
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab_placement" role="tab">
                                        Placement
                                    </a>
                                </li>
                                @else
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab_user_param" role="tab">
                                        User
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br/>
                        @endif
                        <form class="kt-form" id="saverule" method="post" action="{{ route('save.rules', [$id_projects, $room->id])}}">
                            @method('POST')
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-12">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_general" role="tabpanel">
                                            <div class="kt-section kt-section--first">
                                                <div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                        <label for="rule_name" class="col-3 col-form-label">Rule Name :</label>
                                                        <div class="col-8">
                                                            <input class="form-control" name="rule_name" type="text" placeholder="Enter Rule Name" value="{{ old('rule_name') }}" id="example-text-input">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="rule_name" class="col-3 col-form-label">Sub Name :</label>

                                                        <div class="col-8">
                                                            <input class="form-control" name="sub_name" type="text" placeholder="Enter Sub Name" value="{{ old('sub_name') }}" id="example-text-input">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label" for="statusrule">Status :</label>
                                                        <div class="col-8">
                                                            <span class="kt-switch">
                                                                <label>
                                                                    <input type="checkbox" value='1' checked="checked" name="statusrule"/>
                                                                    <span></span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    {{-- <div class="kt-divider mb-4"><span></span></div> --}}
                                                    <div class="form-group form-group-last row">
                                                        <label class="col-3 col-form-label">Description</label>
                                                        <div class="col-8">
                                                            <textarea  name="description" class="form-control" id="description" rows="5">{{ old('description') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_schedule" role="tabpanel">
                                            <div class="kt-section kt-section--first">
                                                <div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                        <div class="col-3"></div>
                                                        <div class="col-8">
                                                            <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                                <input type="checkbox" name="en_schedule" value="1" {{(!empty(old('en_schedule')) ? 'checked' : '')}}> Enable
                                                                <span></span>
                                                            </label>
                                                            <span class="form-text text-muted">Set the user's access time by enabling this rule.</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Day</label>
                                                        <div class="col-8">
                                                            <select class="form-control" id="day" name="day">
                                                                <option value="none" disabled hidden  selected>None</option>
                                                                <option value="everyday" @if(old('day') == 'everyday')  {{ 'selected' }} @endif>Everyday</option>
                                                                <option value="monday" @if(old('day') == 'monday')  {{ 'selected' }} @endif>Monday</option>
                                                                <option value="tuesday" @if(old('day') == 'tuesday')  {{ 'selected' }} @endif>Tuesday</option>
                                                                <option value="wednesday" @if(old('day') == 'wednesday')  {{ 'selected' }} @endif>Wednesday</option>
                                                                <option value="thrusday" @if(old('day') == 'thrusday')  {{ 'selected' }} @endif>Thrusday</option>
                                                                <option value="friday" @if(old('day') == 'friday')  {{ 'selected' }} @endif>Friday</option>
                                                                <option value="saturday" @if(old('day') == 'saturday')  {{ 'selected' }} @endif>Saturday</option>
                                                                <option value="sunday" @if(old('day') == 'sunday')  {{ 'selected' }} @endif>Sunday</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Time Start - End</label>
                                                        <div class="col-4">
                                                            <div class="input-group timepicker">
                                                                <input class="form-control" id="time_start" name="time_start" readonly placeholder="Select time" value=({{ old('time_start') }}) type="text"/>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-clock-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <span class="form-text text-muted">format :  hh/mm</span>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="input-group timepicker">
                                                                <input class="form-control" id="time_end" name="time_end" readonly placeholder="Select time" value=({{ old('time_end') }}) type="text"/>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-clock-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <span class="form-text text-muted">format :  hh/mm</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                         <label class="col-3 col-form-label">Offset</label>
                                                        <div class="col-4">
                                                            <input class="form-control" type="number" name="offset_time" id="offset_time" min="0" value="{{ old('offset_time', 0) }}" placeholder="e.g: 15" id="example-number-input">
                                                            <span class="form-text text-muted">format :  minutes</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_permission" role="tabpanel">
                                            <div class="kt-section kt-section--first">
                                                <div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Permission :</label>
                                                        <div class="col-9">
                                                            <div class="kt-margin-t-10 mb-4">
                                                                <span>As you can see the two box below, the left box is all users you have and the other is used to provide access.<br></span>
                                                                <br>
                                                                <span class="paragraf-text-mt-2"> by clicking username you provide a user to access to this placement <a href="{{route('placement.detail', [$id_projects, $room->id])}}" class="kt-link">{{$room->name}}</a>.</span>
                                                            </div>
                                                            <select multiple id="list_room" name="permission[]" class="form-control " >
                                                                @foreach ($list_user as $key => $value)
                                                                    <option class="kt-font-bold" value={{$key}}>{{$value}}</option>
                                                                    {{-- <option value={{$key}} {{ in_array($key, $getselectedUser) ? 'selected' : ''}}>{{$value}}</option> --}}
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_user_param" role="tabpanel">
                                            <div class="kt-section kt-section--first">
                                                <div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                        <div class="col-3"></div>
                                                        <div class="col-8">
                                                                <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                                    <input type="checkbox" name="en_uparam" value="1" {{(!empty(old('en_uparam')) ? 'checked' : '')}}> Enable
                                                                    <span></span>
                                                                </label>
                                                                <span class="form-text text-muted">Set the user's parameters by enabling for this rule.</span>
                                                            </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Parameters</label>
                                                        <div class="col-8">
                                                            <select class="form-control" id="parameter" name="parameter">
                                                                <option selected disabled hidden>Choose here . . .</option>
                                                                @foreach ($project->parameters as $key => $value)
                                                                    <option value="{{$value->id}}"> {{$value->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Operator</label>
                                                        <div class="col-3">
                                                            <select class="form-control" id="operator" name="operator">
                                                                <option selected disabled hidden>-</option>
                                                                <option value="add">[+] Addition</option>
                                                                <option value="sub">[-] Subtraction</option>
                                                                <option value="perctpls">[%] Percent++</option>
                                                                <option value="perctmin">[%] Percent--</option>
                                                                <option value="replc">[=] Equals</option>
                                                            </select>
                                                        </div>
                                                        <label for="by_value" class="col-1 col-form-label">By</label>
                                                        <div class="col-4">
                                                            <input class="form-control" name="by_value" type="text" placeholder="Enter the value" value="{{ old('by_value') }}" id="by_value">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Min - Max Value</label>
                                                        <div class="col-4">
                                                            <div class="input-group">
                                                                <div class="input-group-append"><span class="input-group-text" id="unit">Min</span></div>
                                                                <input type="text" name="min_value" class="form-control" placeholder="Min Value" aria-describedby="unit" value="{{ old('min_value', '0') }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="input-group">
                                                                <div class="input-group-append"><span class="input-group-text" id="unit">Max</span></div>
                                                                <input type="text" name="max_value" class="form-control" placeholder="Max Value" aria-describedby="unit" value="{{ old('max_value', '0') }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_placement" role="tabpanel">
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Placement Parameters</label>
                                                <div class="col-9">
                                                    <div class="kt-margin-t-10 mb-4">
                                                        <span>As you can see the two box below, the left box is all users you have and the other is used to provide access.<br></span>
                                                        <br>
                                                        <span class="paragraf-text-mt-2"> by clicking username you provide a user to access to this placement <a href="{{route('placement.detail', [$id_projects, $room->id])}}" class="kt-link">{{$room->name}}</a>.</span>
                                                    </div>
                                                    <div class="kt-checkbox-inline ">
                                                        @foreach ($room->placementparameters as $item)
                                                            <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand" >
                                                                <input type="checkbox" value="{{$item->id}}" name="placeparam[]"> {{$item->name }}
                                                                <span></span>
                                                            </label>
                                                        @endforeach
                                                    </div>
                                                    <span class="form-text text-muted">Some help text goes here</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-divider mb-4"><span></span></div>
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-3"></div>
                                                <div class="col-9 col-6">
                                                    <button  id="btnSaveRule" class="btn btn-label-brand btn-bold" onclick="addSubmit()">Save Rule</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                     </div>
                <!--end::Portlet-->
                </div>
            </div>
        <!--End::Section-->
        </div>
    </div>

    <script>

        function addSubmit(){
            $('#btnSaveRule').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $('#saverule').submit();
        }

    </script>

@endsection
