
@extends('layouts.mainlayout')

@section('content')

  <!-- begin:: Content Head -->
  <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">

            <h3 class="kt-subheader__title">
                {{ $subheader_title }}
            </h3>

            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        </div>
        <div class="kt-subheader__toolbar">

        </div>
    </div>
    </div>

    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                    <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__body">
                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-portlet__body kt-portlet__body--fit">
                                <div class="kt-widget kt-widget--user-profile-3">
                                    <div class="kt-widget__top">
                                        <div class="kt-widget__media kt-hidden-">
                                            <img src="/metronic/themes/metronic/theme/default/demo2/dist/assets/media/users/100_2.jpg" alt="image">
                                        </div>
                                        <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                            ChS
                                        </div>
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__head">
                                                <a href="#" class="kt-widget__username">
                                                    Charlie Stone
                                                    <i class="flaticon2-correct kt-font-success"></i>
                                                </a>

                                                <div class="kt-widget__action">
                                                    <button type="button" class="btn btn-label-success btn-sm btn-upper">ask</button>&nbsp;
                                                    <button type="button" class="btn btn-brand btn-sm btn-upper">hire</button>
                                                </div>
                                            </div>

                                            <div class="kt-widget__subhead">
                                                <a href="#"><i class="flaticon2-new-email"></i>charlie@stone.com</a>
                                                <a href="#"><i class="flaticon2-calendar-3"></i>Web Developer</a>
                                                <a href="#"><i class="flaticon2-placeholder"></i>London</a>
                                            </div>

                                            <div class="kt-widget__info">
                                                <div class="kt-widget__desc">
                                                    I distinguish three main text objektive could be merely to inform people.<br>
                                                    A second could be persuade people.You want people to bay objective
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-widget__bottom">
                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <i class="flaticon-piggy-bank"></i>
                                            </div>
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__title">Earnings</span>
                                                <span class="kt-widget__value"><span>$</span>542,500</span>
                                            </div>
                                        </div>

                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <i class="flaticon-confetti"></i>
                                            </div>
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__title">Expenses</span>
                                                <span class="kt-widget__value"><span>$</span>675,500</span>
                                            </div>
                                        </div>

                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <i class="flaticon-pie-chart"></i>
                                            </div>
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__title">Net</span>
                                                <span class="kt-widget__value"><span>$</span>412,400</span>
                                            </div>
                                        </div>

                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <i class="flaticon-file-2"></i>
                                            </div>
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__title">35 Tasks</span>
                                                <a href="#" class="kt-widget__value kt-font-brand">View</a>
                                            </div>
                                        </div>

                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <i class="flaticon-chat-1"></i>
                                            </div>
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__title">598 Comments</span>
                                                <a href="#" class="kt-widget__value kt-font-brand">View</a>
                                            </div>
                                        </div>

                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <i class="flaticon-network"></i>
                                            </div>
                                            <div class="kt-widget__details">
                                                <div class="kt-section__content kt-section__content--solid">
                                                    <div class="kt-media-group">
                                                        <a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="John Myer">
                                                            <img src="/metronic/themes/metronic/theme/default/demo2/dist/assets/media/users/100_1.jpg" alt="image">
                                                        </a>
                                                        <a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Alison Brandy">
                                                            <img src="/metronic/themes/metronic/theme/default/demo2/dist/assets/media/users/100_10.jpg" alt="image">
                                                        </a>
                                                        <a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Selina Cranson">
                                                            <img src="/metronic/themes/metronic/theme/default/demo2/dist/assets/media/users/100_11.jpg" alt="image">
                                                        </a>
                                                        <a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Micheal York">
                                                            <img src="/metronic/themes/metronic/theme/default/demo2/dist/assets/media/users/100_3.jpg" alt="image">
                                                        </a>
                                                        <a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Micheal York">
                                                            <span>+5</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!--end::Portlet-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content -->

        <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deletemodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                    {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    All data related to this rules will be deleted and can't be undone!
                    {{-- <input type="hidden" name="id" id="id"> --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" id="btndeleteRules" >Delete</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    "use strict";
// Class definition


    </script>
@endsection

