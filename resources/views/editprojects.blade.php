
@extends('layouts.mainlayout')
@section('title', 'Dashboard')
@section('content')

    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">

        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid mb-3">

        <!--Begin::Section-->
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <ul class="breadcrumb">
                    <li class="mb-3"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li ><a href="{{route('dashboard')}}">Project</a></li>
                    <li ><a href="{{route('projects.show', $project->id)}}">{{$project->name}}</a></li>
                    <li>Edit</li>
                </ul>
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Edit Project<small>Fill out forms below</small></h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="javascript:history.back()" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a>
                            <div class="btn-group">
                                <button type="button" class="btn btn-brand" id="save" onclick="addSubmit()">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Save</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif

                        <form class="kt-form" id="addproject" method="post" action="{{ route('projects.update', $project->id) }}">
                            @method('PUT')
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-2"></div>
                                <div class="col-8">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <h3 class="kt-section__title kt-section__title-lg">Basic Info:</h3>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Projects Name</label>
                                                <div class="col-9">
                                                <input class="form-control" name="name" type="text" value="{{old('name', $project->name)}}" placeholder="Enter your project name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label" for="statusrule">Status :</label>
                                                <div class="col-9">
                                                    <span class="kt-switch">
                                                        <label>
                                                            @if($project->status)
                                                                <input type="checkbox" checked="checked" name="status" value ="1">
                                                            @else
                                                                <input type="checkbox"  name="status" value="1">
                                                            @endif
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-3 col-form-label">Description</label>
                                                <div class="col-9">
                                                    <textarea  name="description" class="form-control"  id="description" rows="3">{{ old('description', $project->description)}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-2"></div>
                            </div>
                            <div class="kt-portlet__foot"> </div>
                        </form>
                    </div>
                </div>
            <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
        <!--End::Section-->
</div>
    <!-- end:: Content -->

    <script>
        function addSubmit(){
            $('#save').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $('#addproject').submit();
        }
    </script>

@endsection
