
@extends('layouts.mainlayout')
@section('title', 'Devices Connected')
@section('content')

    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">

                <h3 class="kt-subheader__title">
                 Devices
                </h3>

                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <ul class="breadcrumb">
                    <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('dashboard') }}">Projects</a></li>
                    <li><a href="{{route('projects.show', $id_projects) }}">{{$project->name}}</a></li>
                    <li><a href="{{route('projects.show', $id_projects)}}">Device</a></li>
                    <li>List</li>
                </ul>
                <div class="kt-subheader__group" id="kt_subheader_search">

                </div>
            </div>
            <div class="kt-subheader__toolbar">
                @if($count_device == 0)

                @else
                    <span class="kt-subheader__desc kt-font-bold" id="kt_subheader_total">{{$count_device}}&nbsp; Device Total</span>
                @endif
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                @if($count_device === 0)
                <div class="kt-section__content kt-section__content--solid text-center">
                    <div class="text-center">
                        <h5>
                            <p> No Device Connected! </p>
                        </h5>
                    </div>
                </div>
                @endif
                <div class="row">
                    @foreach ($devices as $items)

                        <div class="col-xl-3 col-lg-3">
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            {{-- empty --}}
                                        </h3>
                                    </div>
                                    <div class="kt-portlet__head-toolbar">
                                        <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                            <i class="flaticon-more-1 kt-font-brand"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">
                                                </hr>
                                                <li class="kt-nav__item">
                                                    <a href="javascript:void(0)" class="kt-nav__link" id="deletDevices" onclick="" data-toggle="modal" data-target="#deleteModal">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Delete</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-widget kt-widget--user-profile-2">
                                        <div class="kt-widget__head">
                                            <div class="kt-widget__media">
                                                <img class="kt-widget__img kt-hidden-" src="{{ asset('media/device/default.png') }}" alt="image">
                                                <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                   image
                                                </div>
                                            </div>
                                            <div class="kt-widget__info">
                                                <a href="#" class="kt-widget__username text-capitalize">
                                                    {{$items->device_name}}
                                                </a>
                                                <span class="kt-widget__desc">
                                                    {{$items->type_devices}}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="kt-widget__body">
                                            <div class="kt-widget__section">
                                                Thsssse devices connected to room <a href="#" class="kt-font-brand kt-link kt-font-transform-u kt-font-bold">#xrs-54pq</a> and powered up for <span class="kt-font-primary">{{$items->uptime}}</span>.
                                            </div>

                                            <div class="kt-widget__content">
                                                <div class="kt-widget__stats kt-margin-r-20">
                                                    <div class="kt-widget__icon">
                                                        <i class="flaticon2-poll-symbol"></i>
                                                    </div>
                                                    <div class="kt-widget__details">
                                                        <span class="kt-widget__title">Logs</span>
                                                        <span class="kt-widget__value"><span>$</span>2 Hrs Ago</span>
                                                    </div>
                                                </div>

                                                <div class="kt-widget__stats">
                                                    <div class="kt-widget__icon">
                                                        <i class="flaticon2-zig-zag-line-sign"></i>
                                                    </div>
                                                    <div class="kt-widget__details">
                                                        <span class="kt-widget__title">Graph</span>
                                                        <span class="kt-widget__value"><span>$</span>84,060</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="kt-widget__item">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Wi-Fi SSID:</span>
                                                    <a href="#" class="kt-widget__data">{{$items->ssid}} / -{{$items->wifi_strength}}dBm</a>
                                                </div>
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Local IP:</span>
                                                    <a href="#" class="kt-widget__data">{{$items->local_ip_address}}</a>
                                                </div>
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Mac Address:</span>
                                                    <span class="kt-widget__data">{{$items->mac_address}}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="kt-widget__footer">
                                            <button type="button" class="btn btn-label-brand btn-lg btn-upper">Log</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deletemodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" id="formDeleteDevices" method="post" class="remove-record-model">
                    {{method_field('delete')}}
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        All data related to this device will be deleted and can't be undone!
                        <input type="hidden" name="id" id="id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger" onclick="formdeleteDevices() id="btndeleteDevices" >Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        "use strict";

    function deleteModal(id){

        var id = id;
        console.log(id);
        // var url = '{{ route("projects.destroy", ":id")}}';
        // url = url.replace(':id', id);
        // $("#formDeleteDevices").attr('action', url);
    }

    function formdeleteDevices(){
        // $("#formDeleteDevices").submit();
    }

    </script>

@endsection

