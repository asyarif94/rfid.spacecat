@extends('layouts.mainlayout')
@section('title', 'My Projects')

@section('content')


    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
               Welcome {{ $name_logedin}}!
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                <div class="kt-subheader__group" id="kt_subheader_search">
                    <span class="kt-subheader__desc" id="kt_subheader_total">
                        @if(countPlanProject() != 0)
                            You have {{$project->count()}} Project
                        @else
                            You don't have any project.
                        @endif
                    </span>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <a href="#" class="">

                </a>
                <form class="kt-margin-l-20 mr-2" id="kt_subheader_search_form">
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                        <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                        <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                        <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"/>
                                    </g>
                                </svg>                                    <!--<i class="flaticon2-search-1"></i>-->
                            </span>
                        </span>
                    </div>
                </form>

                @if($project->count() >= countPlanProject() || is_null(auth()->user()->email_verified_at))
                    <a href="#" class="btn btn-label-brand btn-bold disabled">Create New Project</a>
               @else
                    <a href="{{ route('tambah.project') }}" class="btn btn-label-brand btn-bold">Create New Project</a>
               @endif
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">

        <div class="col">
            @if(is_null(auth()->user()->email_verified_at))
            <div class="alert alert-light alert-elevate fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-warning kt-font-danger"></i></div>
                <div class="alert-text">
                    <h6 class="">Your email <span class="kt-font-brand">{{ auth()->user()->email}}</span> is not verified !</h6>
                    <p>if you don't see email verification, click re-send email button.</p>
                </div>
                <button id="btn-resend" onclick="resendmail()" type="button" class="btn btn-sm btn-bold btn-brand"><i class="fas fa-paper-plane"></i> Resend Email Verification</button>&nbsp;
            </div>
            @else
            <div class="alert alert-light alert-elevate fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-tea-cup kt-font-brand"></i></div>
                    <div class="alert-text">
                            Hello wassup! You can only create {{Auth::user()->plan->total_project}} project, because current state is  <i>beta</i> testing,
                        <br>
                            Feel free if you want to help me with pull requests, but issues are good too. <a class="kt-link kt-font-bold" href="https://github.com/AsyaSyarif/RFID-Spacecat/issues" target="_blank">click here</a>.
                    </div>
            </div>
            @endif
        </div>
    </div>

        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                {{--
                @if ($message = Session::get('success'))
                <div class="kt-section__content">
                    <div class="alert alert-solid-brand alert-bold ml-5 mr-5 mt-1" role="alert">
                        <div class="alert-text">{{ $message }}</div>
                    </div>
                </div>
                @endif

                 --}}
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Your Projects
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="#" class="btn btn-clean btn-icon" data-toggle="dropdown">

                            </a>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget5_tab1_content" aria-expanded="true">
                                @if($project->isEmpty())
                                    <p class="text-center">No data found!</p><br>
                                    <p class="text-center"> click <b class="kt-font-info">Create New Project</b> button to create your first project.</p>
                                @endif
                                @foreach($project as $p)
                                <div class="kt-widget5">
                                    <div class="kt-widget5__item">
                                        <div class="kt-widget5__content">
                                            <div class="kt-widget5__pic">
                                                <img src="{{ asset('media/project-logos/project-default.png') }}" alt="image">
                                            </div>
                                            <div class="kt-widget5__section">
                                                @if($p->status == '1')
                                                <span class="kt-badge kt-badge--success kt-badge--dot kt-badge--lg"></span>
                                                @elseif($p->status == '3')
                                                <span class="kt-badge kt-badge--warning kt-badge--dot kt-badge--lg"></span>
                                                @else
                                                <span class="kt-badge kt-badge--danger kt-badge--dot kt-badge--lg"></span>
                                                @endif
                                                <a href="{{ route('projects.show', $p->id) }}" class="kt-widget5__title text-capitalize">
                                                    <span class="kt-font-bolder">{{ $p->name }}</span>
                                                </a>
                                                <div class="dropdown dropdown-inline">
                                                    <button type="button" class="btn btn-clean btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="flaticon-more-1"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="{{ route('projects.show', $p->id) }}"><i class="la la-eye"></i> View</a>
                                                        <a class="dropdown-item" href="{{url('edit', $p->id)}}"><i class="la la-pencil"></i> Edit</a>
                                                        <a class="dropdown-item" href="javascript:;" onclick="deleteModal('{{$p->id}}')" data-toggle="modal" data-target="#deleteModal"><i class="la la-trash-o"></i> Delete</a>
                                                        <div class="dropdown-divider"></div>
                                                        <a disabled class="dropdown-item" href="#"><i class="la la-cog"></i> Upgrade</a>
                                                    </div>
                                                </div>
                                                <p class="kt-widget5__desc">
                                                        {{ $p->description }}
                                                </p>
                                                <div class="kt-widget5__info">
                                                    <span>Author:</span>
                                                <span class="kt-font-info">{{Auth::user()->name}}</span>
                                                    <span>Created:&nbsp;</span>
                                                    <span class="kt-font-info">{{ $p->created_at->diffForHumans() }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget5__content">
                                            <div class="kt-widget5__stats">
                                                <span class="kt-widget5__number">{{ $p->usermodels->count() }}</span>
                                                <span class="kt-widget5__sales">Users</span>
                                            </div>
                                            <div class="kt-widget5__stats">
                                            <span class="kt-widget5__number">{{ $p->rooms->count()}}</span>
                                                <span class="kt-widget5__votes">Placement</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        <!--End::Section-->

        <!-- Modal -->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deletemodal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="" id="formDeleteProject" method="post" class="remove-record-model">
                        {{method_field('delete')}}
                        {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        All data related to this project will be deleted and can't be undone!
                        <input type="hidden", name="project_id" id="project_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger" data-dismiss="modal" onclick="formProjectSubmit()" >Delete</button>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <!--End::Section-->
    </div>
    <!-- end:: Content -->

    <script type="text/javascript">
        function resendmail(){

            $('#btn-resend').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $.ajax({
               type:'POST',
               url:"{{route('resend.mail', Auth::user()->id)}}",
               data: {"_token": "{{ csrf_token() }}"},
               success:function(data) {
                    $('#btn-resend').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                    $('#btn-resend').html('Success Resend The Email');
               },
               error: function (data) {
                // console.log("error");
                // console.log('Error:', data);
               }
            });

        }
    </script>

@endsection
@push('scripts')

<script src="{{ asset('js/pages/dashboard.js') }}" type="text/javascript"></script>



@endpush
