
@extends('layouts.mainlayout')
@section('title', 'Parameters')
@section('content')

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">

            <h3 class="kt-subheader__title">
                {{$subheader_title}}
            </h3>

            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <ul class="breadcrumb">
                <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                <li><a href="{{route('dashboard') }}">Projects</a></li>
                <li><a href="{{route('projects.show', $project->id) }}">{{$project->name}}</a></li>
                <li>Parameters</li>
            </ul>
            <div class="kt-subheader__group" id="kt_subheader_search">

            </div>
        </div>
        <div class="kt-subheader__toolbar">

            <a href="{{route('parameter.add', $project->id) }}" class="btn btn-label-brand btn-sm btn-bold ">Add Parameter</a>

        </div>
    </div>
</div>


<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">

        <div class="col-lg-12 col-xl-12">
        <!--begin:: Widgets/Sale Reports-->
        <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        List
                    </h3>
                </div>

            </div>
            <div class="kt-portlet__body">
                <div class="kt-widget11">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr class="kt-align-center">
                                    <td style="width:1%">#</td>
                                    <td style="width:15%" class="kt-align-left">Parameter Name</td>
                                    <td style="width:14%">Default Value</td>
                                    <td style="width:15%">Unit</td>
                                    <td style="width:15%">Allow Remote</td>
                                    <td style="width:15%">Options</td>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($project->parameters as $item)
                                <tr>
                                    <td>
                                        <label class="kt-checkbox kt-checkbox--single">
                                        <input type="checkbox"><span></span>
                                        </label>
                                    </td>
                                    <td>
                                        <a href="{{ route('parameter.edits', [$project->id , $item->id]) }}" class="kt-widget11__title">{{$item->name}}</a>
                                        <span class="kt-widget11__sub">Parameters</span>
                                    </td>
                                    <td class="kt-align-center kt-font-brand kt-font-bold">{{$item->default_value}}</td>
                                    <td class="kt-align-center kt-font-bold">{{$item->units}}</td>
                                    <td class="kt-align-center kt-font-brand kt-font-bold">
                                        @if($item->allow_remote_access == "0")
                                            <span class="kt-badge kt-badge--inline kt-font-dark"> No  </span>
                                        @else
                                            <span class="kt-badge kt-badge--inline kt-badge--primary"> Yes </span>
                                        @endif
                                    </td>
                                    <td class="kt-align-center ">
                                        <a href="#" class="btn btn-clean btn-icon" data-toggle="dropdown">
                                            <i class="flaticon-more-1 "></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href=" {{ route('parameter.edits', [$project->id , $item->id]) }}" class="kt-nav__link" id="viewparam">
                                                        <i class="kt-nav__link-icon flaticon-eye"></i>
                                                        <span class="kt-nav__link-text">View</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                <a href="javascript:void(0)" class="kt-nav__link" id="deletDevices" onclick="deleteUserParameter('{{$item->id}}')" data-toggle="modal" data-target="#deleteModalParam">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Delete</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Sale Reports-->
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModalParam" tabindex="-1" role="dialog" aria-labelledby="deleteModalParam" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" id="formDeleteParam" method="post" class="remove-record-model">
                {{method_field('DELETE')}}
                {{ csrf_field() }}
            <div class="modal-header">
                <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                All data related to this room will be deleted and can't be undone!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger"  data-dismiss="modal" onclick="formDeleteParamSubmit()" >Delete</button>
            </div>
         </form>
        </div>
    </div>
</div>

@endsection
