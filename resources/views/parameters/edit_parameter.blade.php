@extends('layouts.mainlayout')
@section('title', 'Edit Parameters')
@section('content')
<div class="row">
    <div class="col-2"></div>
    <div class="col-10">
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <ul class="breadcrumb ml-3">
                        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li><a href="{{route('dashboard') }}">Projects</a></li>
                        <li><a href="{{route('projects.show', $project->id) }}">{{$project->name}}</a></li>
                        <li><a href="{{route('parameter.list', $project->id) }}">Parameters</a></li>
                        <li>Edit Parameter {{$parameters->name}}</li>
                    </ul>
                    <div class="kt-subheader__group" id="kt_subheader_search">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid mb-4">

        <!--Begin::Section-->
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Add New Field<small>Fill out forms below</small></h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                {{-- <a href="javascript:history.back()" class="btn btn-clean kt-margin-r-10">
                                    <i class="la la-arrow-left"></i>
                                    <span class="kt-hidden-mobile">Back</span>
                                </a> --}}
                                <div class="btn-group">
                                    <button type="button" id="save" class="btn btn-brand" onclick="addSubmit()">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Save</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="kt-portlet__body">
                            <div class="row">
                                <div class="col-1"></div>
                                <div class="col-9">
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                    @endif
                                </div>
                            </div>

                        <form class="kt-form" id="editparam" method="post" action="{{route('parameter.updates', [$id_projects, $parameters->id])}}">
                            @method('PUT')
                            @csrf
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-10">
                                        <div class="kt-section kt-section--first">
                                            <div class="kt-section__body">
                                                {{-- <h3 class="kt-section__title kt-section__title-sm">Basic Info:</h3> --}}
                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label">Allow Remote</label>
                                                    <div class="col-lg-3">
                                                        <span class="kt-switch">
                                                            <label>
                                                                @if($parameters->allow_remote_access == '1')
                                                                    <input type="checkbox" checked="checked" name="allow_remote" value ="1">
                                                                @else
                                                                    <input type="checkbox"  name="allow_remote" value="1">
                                                                @endif
                                                                <span></span>
                                                            </label>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 col-form-label">Parameter Name</label>
                                                    <div class="col-6">
                                                        <input class="form-control" name="name" type="text" placeholder="Enter a parameter name" value="{{ old('parameter_name', $parameters->name)}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 col-form-label">Value <span class="text-form-default"> [default] </span></label>
                                                    <div class="col-3">
                                                        <input class="form-control" name="default_value" type="text" placeholder="Enter value"  value="{{$parameters->default_value}}">
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="input-group">
                                                            <div class="input-group-append"><span class="input-group-text" id="unit">$</span></div>
                                                            <input type="text" name="units" class="form-control" placeholder="Unit" aria-describedby="units" value="{{$parameters->units}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <!--end::Portlet-->
                </div>
            </div>
        <!--End::Section-->
        </div>
        <!-- end:: Content -->

        <script>
            function addSubmit(){
                $('#save').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                $('#editparam').submit();
            }
        </script>

@endsection
