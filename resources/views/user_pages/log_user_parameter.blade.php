@extends('layouts.mainlayout')
@section('title', 'Dashboard')
@section('content')

{{-- {{dd($datauser->logparameters)}} --}}
<!--Begin::Section-->
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">

    </div>
</div>
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid mb-3">
    <div class="row">
        <div class="col-2"></div>
        <div class="col-10">
            <ul class="breadcrumb">
                <li class="mb-3"><a href="{{route('dashboard')}}">Dashboard</a></li>
                <li><a href="{{route('dashboard')}}">Projects</a></li>
                <li><a href="{{route('user.list', $id_projects)}}">Users</a></li>
                <li><a href="{{route('lihat.user', [$id_projects, $datauser->id])}}">{{$datauser->name}}</a></li>
                <li>Parameters</li>
            </ul>
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Striped Rows
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-more-1"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-google-drive-file"></i>
                                            <span class="kt-nav__link-text">Export PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table table-striped table-hover">
                                    <thead>
                                    <tr class="text-left">
                                            <th class="text-left">Date</th>
                                            <th>By [Operator]</th>
                                            <th>Before</th>
                                            <th>After</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($datauser->logparameters->sortByDesc('created_at') as $item)
                                        <tr class="text-left">
                                            <td scope="row" class="text-left">{{Carbon\Carbon::parse($item->created_at)->format('D, M d, Y.   H:m')}}</td>
                                            <td> {{$item->new_value - $item->last_value}}
                                                <span class="text-center kt-label-font-color-2">
                                                    @if($item->operator == "add")
                                                        [+]
                                                    @elseif($item->operator == "sub")
                                                        [-]
                                                    @elseif($item->operator == "perctpls")
                                                        [%+]
                                                    @elseif($item->operator == "perctmin")
                                                        [%-]
                                                    @elseif($item->operator == "replc")
                                                        [=]
                                                    @endif
                                                </span>
                                            </td>
                                            <td>{{$item->last_value}}</td>
                                            <td>
                                                @if($item->new_value >= $item->last_value)
                                                    <i class="fas fa-caret-up kt-font-success"></i>
                                                @else
                                                    <i class="fas fa-caret-down kt-font-danger"></i>
                                                @endif
                                                    <span class="kt-font-info kt-font-bold ">
                                                        {{$item->new_value}}
                                                    </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                            </table>

                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
</div>



@endsection
