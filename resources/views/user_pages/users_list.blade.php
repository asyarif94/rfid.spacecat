
@extends('layouts.mainlayout')
@section('title', "Users ".$page_title)
@section('content')


<style>

.breadcrumb-dot .breadcrumb-item+.breadcrumb-item::before {

 content: ">";

 color: #408080;

}
.breadcrumb {
    background-color: #f8f9fa !important;
}

</style>
  <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                {{-- <div id="crouton">
                    <ul class="kt-font-bold">
                        <li><a href="#">Dashboard</a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="#">Pla</a></li>
                        <li><a href="#">Four</a></li>
                    </ul>
                </div> --}}
                <h3 class="kt-subheader__title">
                    {{$subheader_title }}
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <ul class="breadcrumb">
                    <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ route('projects.show', $id_projects) }}">Projects</a></li>
                    <li><a href="{{route('user.list', $id_projects)}}">Users</a></li>
                    <li>List</li>
                </ul>
            </div>
            <div class="kt-subheader__toolbar">
                <form class="kt-margin-r-10" id="kt_subheader_search_form" method="GET" action="{{route('user.list', $id_projects)}}">
                    {{ csrf_field() }}
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                        <input type="text" class="form-control" placeholder="Search..." id="search" name="search" value={{ old('search') }}>
                            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                        <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                        <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"/>
                                    </g>
                                </svg>                                    <!--<i class="flaticon2-search-1"></i>-->
                            </span>
                        </span>
                    </div>
                </form>
                @if($countUser >= auth()->user()->plan->total_users)
                    <a href="" class="btn btn-label-brand btn-sm btn-bold disabled">Add User</a>
                @else
                    <a href="{{ route('tambah.user', $id_projects)}}" class="btn btn-label-brand btn-sm btn-bold ">Add User</a>
                @endif
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Section-->
        <div class="row">
            @if($users->isEmpty())
            <div class="kt-block-center">
                <p class="text-center kt-font-bold">No users found!</p><br>
                <p class="text-center"> Click <b class="kt-font-info">Add User</b> button to Add the user into your project.</p>
            </div>
            @endif
            @foreach ($users->sortByDesc('created_at') as $user)
            <div class="col-lg-3">
                <!--Begin::Portlet-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                <i class="flaticon-more-1 kt-font-brand"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="{{ route('lihat.user', [$user->id_project,$user->id]) }}" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon-eye"></i>
                                            <span class="kt-nav__link-text">View</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="" onclick="deleteModalUser('{{$user->id}}')" data-toggle="modal" data-target="#deleteModalUser" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Delete</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="kt-portlet__body ">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-2">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="kt-widget__img kt-hidden-" src="{{ asset($user->url_avatar)}}" alt="image">
                                    <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden">
                                        image
                                    </div>
                                </div>

                                <div class="kt-widget__info">
                                    @if($user->status == '1')
                                        <span class="kt-badge kt-badge--success kt-badge--dot kt-badge--lg"></span>
                                    @elseif($user->status == '3')
                                        <span class="kt-badge kt-badge--warning kt-badge--dot kt-badge--lg"></span>
                                    @else
                                        <span class="kt-badge kt-badge--danger kt-badge--dot kt-badge--lg"></span>
                                    @endif
                                    <a href="{{ route('lihat.user', [$user->id_project,$user->id]) }}" class="kt-widget__username">
                                      {{$user->name}}
                                    </a>
                                    @if($user->password != null)
                                    <i class="kt-font-primary flaticon2-shield"></i>
                                    @else

                                    @endif
                                    <span class="kt-widget__desc">
                                        Members
                                    </span>
                                </div>
                            </div>

                            <div class="kt-widget__body">
                                <div class="kt-widget__section">
                                   @if($user->description)
                                   {{$user->description }}
                                   @else
                                   -
                                   @endif
                                </div>
                                <div class="kt-widget__item">
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label">Email:</span>
                                        @if($user->email)
                                            <a href="#" class="kt-widget__data prevent-text-over ">{{$user->email}}</a>
                                        @else
                                        -
                                        @endif

                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label">Phone:</span>
                                        @if($user->phonenr)
                                        <a href="#" class="kt-widget__data">{{$user->phonenr}}</a>
                                        @else
                                        -
                                        @endif

                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label">Last Tap:</span>
                                        @if($user->last_taping == null)
                                        <span class="kt-widget__data">-</span>
                                        @else
                                        <span class="kt-widget__data">{{ \Carbon\Carbon::parse($user->last_taping)->diffForHumans() }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="kt-widget__footer">
                                <div class="kt-widget__section">
                                <a href="{{ route('lihat.user', [$user->id_project,$user->id]) }}" class="btn btn-label-brand btn-upper">Details</a>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
                <!--End::Portlet-->
            </div>
            @endforeach
        <!-- Isi Content disini -->

        </div>
        <!--End::Section-->
        <!--Begin::Section-->
    {{-- <div class="row">
        <div class="col-xl-12">
            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <ul class="kt-pagination__links">
                            <li class="kt-pagination__link--first">
                                <a href="#"><i class="fa fa-angle-double-left kt-font-brand"></i></a>
                            </li>
                            <li class="kt-pagination__link--next">
                                <a href="#"><i class="fa fa-angle-left kt-font-brand"></i></a>
                            </li>

                            <li>
                                <a href="#">...</a>
                            </li>
                            <li>
                                <a href="#">29</a>
                            </li>
                            <li>
                                <a href="#">30</a>
                            </li>
                            <li class="kt-pagination__link--active">
                                <a href="#">31</a>
                            </li>
                            <li>
                                <a href="#">32</a>
                            </li>
                            <li>
                                <a href="#">33</a>
                            </li>
                            <li>
                                <a href="#">34</a>
                            </li>
                            <li>
                                <a href="#">...</a>
                            </li>

                            <li class="kt-pagination__link--prev">
                                <a href="#"><i class="fa fa-angle-right kt-font-brand"></i></a>
                            </li>
                            <li class="kt-pagination__link--last">
                                <a href="#"><i class="fa fa-angle-double-right kt-font-brand"></i></a>
                            </li>
                        </ul>

                        <div class="kt-pagination__toolbar">
                            <select class="form-control kt-font-brand" style="width: 60px">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span class="pagination__desc">
                                Displaying 10 of 230 records
                            </span>
                        </div>
                    </div>
                    <!--end: Pagination-->
                </div>
            </div>
    <!--end:: Components/Pagination/Default-->
        </div>
    </div> --}}
    <!--End::Section-->
    </div>
    <!-- end:: Content -->


    <!-- Modal -->
    <div class="modal fade" id="deleteModalUser" tabindex="-1" role="dialog" aria-labelledby="deleteModalUser" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" id="formDeleteUser" method="post" class="remove-record-model">
                    {{method_field('delete')}}
                    {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    All data related to this user will be deleted and can't be undone!
                    <input type="hidden" name="id" id="id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger"  data-dismiss="modal" onclick="formDeletetSubmit()" >Delete</button>
                </div>
                </form>
            </div>
        </div>
    </div>




<script type="text/javascript">
    "use strict";



</script>



@endsection

