
@extends('layouts.mainlayout')
@section('title', $page_title)
@section('content')

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid mt-3 mb-3">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="kt-subheader__main mb-3">
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <ul class="breadcrumb">
                        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li><a href="{{ route('projects.show', $id_projects) }}">Projects</a></li>
                        <li><a href="{{route('user.list', $id_projects)}}">User</a></li>
                        <li>Add New</li>
                    </ul>
                </div>
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Add New User<small>Fill out forms below</small></h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            {{-- <a href="javascript:history.back()" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a> --}}
                            <div class="btn-group">
                                <button type="button" class="btn btn-brand" id="save" onclick="addSubmit()">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Save</span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-8">
                                {{-- <div id="result"> </div> --}}

                                <div style="display:none" class="alert alert-solid-danger alert-bold"  role="alert">
                                </div>
                            </div>
                        </div>

                        <form class="kt-form" id="adduser" method="post" action="javascript:void(0)"  enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-2"></div>
                                <div class="col-8">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <input type="hidden" name="id" id="id" value="{{ $id_projects }}">
                                            <h3 class="kt-section__title kt-section__title-sm">Basic Info:</h3>
                                            <div class="form-group row">
                                                <label class="col-3 col-lg-3 col-form-label">Avatar</label>
                                                <div class="col-9 col-lg-9">
                                                    <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                                        <img class="kt-avatar__holder" id="imgcontainer" src= "{{ asset('media/users/default.jpg') }}"></img>
                                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Upload Avatar">
                                                            <i class="fa fa-pen"></i>
                                                            <input type="file" name="profile_avatar" id="profile_avatar">
                                                        </label>
                                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                            <i class="fa fa-times"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-3 col-lg-3 col-form-label">Name</label>
                                                <div class="col-9 col-lg-9">
                                                    <input class="form-control" name="name" type="text" placeholder="Full Name" value="{{ old('name') }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3  col-form-label">Email</label>
                                                <div class="col-9">
                                                    <input class="form-control" name="email" type="text" placeholder="Enter your Email" value="{{ old('email') }}">
                                                    <span class="form-text text-muted">We'll never share your email with anyone else.</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Phone Nr</label>
                                                <div class="col-9">
                                                    <input class="form-control" name="phonenr" type="text" placeholder="08123456789" value="{{ old('phonenr') }}">
                                                    <span class="form-text text-muted">We'll never share your phone number with anyone else.</span>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-3 col-lg-3 col-form-label" for="statusrule">Status :</label>
                                                <div class="col-9 col-lg-9">
                                                    <span class="kt-switch">
                                                        <label>
                                                            <input type="checkbox" value='1' checked="checked" name="status"/>
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <h3 class="kt-section__title kt-section__title-sm">Security:</h3>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">RFID</label>
                                                <div class="col-9">
                                                    <input class="form-control" name="rfid" type="text"  min="0" max="99999999999999999" placeholder="Enter RFID Number">
                                                    <span class="form-text text-muted">Click <a class="kt-link" href="#">here</a> for help how to get RFID number.</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-lg-3 col-form-label">Password</label>
                                                <div class="col-9 col-lg-9">
                                                    <input class="form-control" name="password" type="password" placeholder="Enter Password">
                                                    <span class="form-text text-muted">The password is optional and make sure your devices have keypad!</span>
                                                </div>
                                                </br>
                                                </br>
                                                </br>

                                            </div>
                                            <div class="form-group row">
                                                <div class="col-3"></div>
                                                <div class="col-9 col-lg-9">
                                                    <input class="form-control" name="repassword" type="password" placeholder="Repeat Password">
                                                </div>
                                            </div>
                                            <h3 class="kt-section__title kt-section__title-sm">Miscellaneous:</h3>
                                            <div class="form-group form-group-last row">
                                                <label class="col-3 col-form-label">About User</label>
                                                <div class="col-9">
                                                    <textarea  name="description" class="form-control" id="description" rows="5">{{ old('description') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-2"></div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
    <!-- end:: Content -->
<script>


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imgcontainer').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#profile_avatar").change(function(){
    readURL(this);
});


$(document).ready(function(e){

$('#adduser').on('submit',(function(e) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});

e.preventDefault();

var formData = new FormData(this);
        $.ajax({
            type:'POST',
            url: "{{ route('user.store')}}",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                window.location = '{{ route('user.list', $id_projects) }}'
                $('#original').attr('src', 'public/media/users/'+ data.profile_avatar);
                $('#thumbImg').attr('src', 'public/media/users/thumbnail/'+ data.profile_avatar);
            },
            error: function(request, status, error){

                json  = $.parseJSON(request.responseText);
                // console.log(json);

                $('#save').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                // window.location = '{{ route('tambah.user', $id_projects) }}'
                $.each(json.errors, function(key, value){
                    $('.alert-solid-danger').show();
                    $('.alert-solid-danger').css('display','block');
                    $('.alert-solid-danger').append('<li class="alert-text" style:block>'+value+'</li>');
                });
                $("#result").html('');

            }
        });


    }));
});

function addSubmit(){
    $('.alert-solid-danger').hide();
    $('.alert-solid-danger').html('');

    $('#adduser').submit();
    $('#save').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
}

</script>

@endsection

