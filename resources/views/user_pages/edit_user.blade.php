
@extends('layouts.mainlayout')
@section('title', 'Dashboard')
@section('content')
       
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
               
            </div>
            <div class="kt-subheader__toolbar">

                <a href="#" class="">

                </a>

 
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Section-->
        
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Edit User<small>Fill out forms below</small></h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                        <a href="javascript:history.back()" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a>
                            <div class="btn-group">
                                <button type="button" class="btn btn-brand" onclick="addSubmit()">
                                    <i class="la la-check"></i> 
                                    <span class="kt-hidden-mobile">Save</span>
                                </button>
                                <button type="button" class="btn btn-brand dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-edit-interface-symbol-of-pencil-tool"></i>
                                                <span class="kt-nav__link-text">Save & edit</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <span class="kt-nav__link-text">Save & add new</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="kt-portlet__body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form class="kt-form" id="adduser" method="post" action="{{ route('user.update', $id) }}">
                                @method('PUT')
                            @csrf
                           
                            <div class="row">
                                <div class="col-xl-2"></div>
                                <div class="col-xl-8">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <input type="hidden" name="id" id="id" value="{{ $id }}">
                                            <input type="hidden" name="id_project" id="id_project" value="{{ $users->id_project}}">
                                            <h3 class="kt-section__title kt-section__title-sm">Basic Info:</h3>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Name</label>
                                                <div class="col-9">
                                                    <input class="form-control" name="name" type="text" placeholder="Full Name" value="{{ old('nama', $users->name) }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Email</label>
                                                <div class="col-9">
                                                    <input class="form-control" name="email" type="text" placeholder="Enter your Email" value="{{ old('email', $users->email) }}">
                                                    <span class="form-text text-muted">We'll never share your email with anyone else.</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Phone Nr</label>
                                                <div class="col-9">
                                                    <input class="form-control" name="phonenr" type="text" placeholder="08123456789" value="{{ old('phonenr', $users->phonenr) }}">
                                                    <span class="form-text text-muted">We'll never share your phone number with anyone else.</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Status</label>
                                                <div class="col-4" >
                                                    <select class="form-control" id="status" name="status">
                                                        <option value="1">Active</option>
                                                        <option value="0">InActive</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <h3 class="kt-section__title kt-section__title-sm">Security:</h3>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">RFID</label>
                                                <div class="col-9">
                                                    <input class="form-control" name="rfid" type="text"  min="0" max="99999999999999999" placeholder="Enter RFID Number">
                                                    <span class="form-text text-muted">Click <a class="kt-link" href="#">here</a> for help how to get RFID number.</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Password</label>
                                                <div class="col-4">
                                                    <input class="form-control" name="password" type="password" placeholder="Enter Password">
                                                </div>
                                                <div class="col-5">
                                                    <input class="form-control" name="repassword" type="password" placeholder="Repeat Password">
                                                </div>
                                            </div>
                                            <h3 class="kt-section__title kt-section__title-sm">Miscellaneous:</h3>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Image</label>
                                                <div></div>
                                                <div class="col-9 ">
                                                    <div class="col-12">
                                                        <label class="custom-file-label" for="customFile">Choose Image</label>
                                                        <input type="file" class="custom-file-input" id="image" name="image">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-3 col-form-label">Description</label>
                                                <div class="col-9">
                                                <textarea  name="description" class="form-control" id="description" rows="5">{{old('description', $users->description)}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-2"></div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                   
                                </div>
                            </div>
                        </form>
                    </div>
                </div>	
            <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
    <!-- end:: Content -->


<script>

    function addSubmit(){
        $('#adduser').submit();
    }

</script>

@endsection