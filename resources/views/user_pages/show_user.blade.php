
@extends('layouts.mainlayout')
@section('title', $datauser->project->name." - ". $datauser->name)
@section('content')

    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    {{$subheader_title }}
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <ul class="breadcrumb">
                    <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ route('projects.show', $id_projects) }}">Projects</a></li>
                    <li><a href="{{route('user.list', $id_projects)}}">User</a></li>
                    <li>{{$datauser->name}}</li>
                </ul>
            </div>
            <div class="kt-subheader__toolbar">
                <span class="kt-subheader__desc mr-3" id="kt_subheader_total">Created at {{ \Carbon\Carbon::parse($datauser->created_at)->format('l, j \\ F Y ') }}</span>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->

    {{-- {{dd($datauser->rules)}} --}}
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-7">
                <div class="kt-portlet kt-portlet--tabs">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">User information</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_1" role="tab">
                                        Basic Info
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_2" role="tab">
                                        Notification
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_3" role="tab">
                                        Security
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " data-toggle="tab"  href="#tab_access" role="tab" >Access</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " data-toggle="tab"  href="#tab_setting" role="tab" >Settings</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if ($errors->any())
                    <div class="row mt-5">
                        <div class="col-2 "></div>
                        <div class="col-7">
                            <div class="alert alert-solid-danger alert-bold">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li class="alert-text">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                    {{-- {{dd($datauser->logparameters)}} --}}
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="tab-content mt-3">
                                    <div class="tab-pane active" id="kt_portlet_tab_1_1">
                                        <div class="row">
                                            <label class="col-3 col-form-label"></label>
                                            <div class="col-lg-9 col-xl-6">
                                                <h3 class="kt-section__title kt-section__title-sm">User Basic Info:</h3>
                                            </div>
                                        </div>
                                        <form class="kt-form kt-form--label-right" id="basicinfo_form"  action="{{ route('update.basicinfo', [$datauser->id_project ,$datauser->id]) }}" method="post" enctype="multipart/form-data">
                                            @method('PUT')
                                            @csrf
                                        <input type="hidden" name="id" id="id" value="{{ $datauser->id }}">
                                        <div class="form-group row">
                                            <label class="col-3 col-lg-3 col-form-label">Avatar</label>
                                            <div class="col-9 col-lg-9">
                                                <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                                    <img class="kt-avatar__holder" name="img_avatar"  id="imgcontainer" src= "{{ asset($datauser->url_avatar)}}"></img>
                                                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Upload Avatar">
                                                        <i class="fa fa-pen"></i>
                                                        <input type="file" name="profile_avatar" id="profile_avatar">
                                                    </label>
                                                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                        <i class="fa fa-times"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-3 col-lg-3 col-form-label">Name</label>
                                            <div class="col-9 col-lg-6">
                                                <input class="form-control"  name="name" placeholder="Full Name" type="text" value="{{old('name', $datauser->name)}}">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-sm row">
                                            <label class="col-3 col-lg-3 col-form-label">Status</label>
                                            <div class="col-9 col-lg-9">
                                                <span class="kt-switch">
                                                        <label>
                                                        @if($datauser->status)
                                                            <input type="checkbox" checked="checked" name="status" value ="1">
                                                        @else
                                                            <input type="checkbox"  name="status" value="1">
                                                        @endif
                                                        <span></span>
                                                    </label>
                                                </span>
                                                <span class="form-text text-muted">If user status is not active, user not be able tap doing tap card. </span>
                                            </div>
                                        </div>
                                        <div class="row mt-5">
                                            <label class="col-3 col-form-label"></label>
                                            <div class="col-6">
                                                <h3 class="kt-section__title kt-section__title-sm">Contact Info:</h3>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">Phone</label>
                                            <div class="col-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                    <input type="text" class="form-control" name="phonenr" placeholder="eg: 08123456789" value="{{ old('phonenr', $datauser->phonenr) }}" placeholder="Phone" aria-describedby="basic-addon1">
                                                </div>
                                                <span class="form-text text-muted">We'll never share your email with anyone else.</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">Email</label>
                                            <div class="col-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                                    <input type="text" class="form-control" name="email" value="{{ old('email', $datauser->email) }}" placeholder="Email" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">RFID</label>
                                            <div class="col-6 ">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-credit-card"></i></span></div>
                                                    <input type="text" class="form-control" name="rfid" min="0" max="99999999999999999" value="{{ old('rfid', $datauser->rfid) }}" placeholder="Enter RFID Number" aria-describedby="basic-addon1" readonly="readonly" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-5">
                                            <label class="col-3 col-form-label"></label>
                                            <div class="col-6">
                                                <h3 class="kt-section__title kt-section__title-sm">Miscellaneous:</h3>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">About User</label>
                                            <div class="col-6">
                                                <textarea  name="description" class="form-control" id="description" rows="5">{{ old('description', $datauser->description) }}</textarea>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__foot">
                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-lg-3 col-xl-3">
                                                    </div>
                                                    <div class="col-9">
                                                        <button onclick="updateBasicInfoSubmit()" id="btnSaveBasicInfo" class="btn btn-label-brand btn-bold">Save Changes</button>&nbsp;
                                                        <button type="reset" class="btn btn-clean btn-bold">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    <div class="tab-pane" id="kt_portlet_tab_1_2">
                                        <form class="kt-form kt-form--label-right">
                                            <div class="kt-form__body">

                                                <div class="kt-section kt-section--first">
                                                    <div class="alert alert-light alert-elevate fade show" role="alert">
                                                        <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                                                        <div class="alert-text">
                                                          <span class="kt-font-bold"> Experimental features!</span> <br>
                                                           may this function will not work for now.
                                                        </div>
                                                    </div>
                                                    <div class="kt-section__body">
                                                        <div class="row mt-4">
                                                            <label class="col-3"></label>
                                                            <div class="col-9">
                                                                <h3 class="kt-section__title kt-section__title-sm">Setup Notification:</h3>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-sm row">
                                                            <label class="col-3 col-form-label">Email Notification</label>
                                                            <div class="col-9">
                                                                <span class="kt-switch">
                                                                        <label>
                                                                        <input type="checkbox" name="email_notification_1">
                                                                        <span></span>
                                                                </label>
                                                                </span>
                                                                <span class="form-text text-muted">An email will be sent with this account's details, make sure your email is correct and active.</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 col-form-label">Receive Email When</label>
                                                            <div class="col-9">
                                                                <div class="kt-checkbox-list">
                                                                    <label class="kt-checkbox  kt-checkbox--brand">
                                                                        <input type="checkbox"> Success Tap Card
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="kt-checkbox  kt-checkbox--brand">
                                                                        <input type="checkbox"> Fail Tap Card
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="kt-checkbox  kt-checkbox--brand">
                                                                        <input type="checkbox">Incorrect password entered exceeded after reach maximum limit
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-last row">
                                                            <label class="col-3 col-form-label">SMS Notification</label>
                                                            <div class="col-9">
                                                                <span class="kt-switch">
                                                                    <label>
                                                                        <input type="checkbox" name="email_notification_2">
                                                                        <span></span>
                                                                    </label>
                                                                </span>
                                                                <span class="form-text text-muted">Like Email, make sure the phone number is correct and active.</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 col-form-label">Receive SMS When</label>
                                                            <div class="col-9 ">
                                                                <div class="kt-checkbox-list">
                                                                    <label class="kt-checkbox  kt-checkbox--brand">
                                                                        <input type="checkbox"> Success Tap Card
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="kt-checkbox  kt-checkbox--brand">
                                                                        <input type="checkbox"> Fail Tap Card
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="kt-checkbox  kt-checkbox--brand">
                                                                        <input type="checkbox">Incorrect password entered exceeded after reach maximum limit
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
                                                <div class="kt-section kt-section--first">
                                                    <div class="kt-section__body">
                                                        <div class="row">
                                                            <label class="col-3"></label>
                                                            <div class="col-9">
                                                                <h3 class="kt-section__title kt-section__title-sm">3rd Party Notification:</h3>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-last row">
                                                            <label class="col-3 col-form-label">Telegram Notification</label>
                                                            <div class="col-9">
                                                                <span class="kt-switch">
                                                                    <label>
                                                                        <input type="checkbox" name="email_notification_2">
                                                                        <span></span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-3 col-form-label">Bot Token</label>
                                                            <div class="col-6">
                                                                <input class="form-control" type="text" placeholder="Telegram Bot Token" id="example-text-input">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
                                            </div>

                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-3"></div>
                                                    <div class="col-9">
                                                        <a href="#" class="btn btn-label-brand btn-bold" onclick="saveRules()">Save Changes</a>
                                                        <a href="#" class="btn btn-clean btn-bold">Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="kt_portlet_tab_1_3">
                                        <form class="kt-form kt-form--label-right" id="password_form" action="{{ route('update.password', [$datauser->id_project ,$datauser->id]) }}" method="post">
                                                @method('PUT')
                                                @csrf
                                            <div class="row ml-5 mr-5">
                                                <div class="col-12">
                                                    <div class="alert alert-solid-danger alert-bold fade show kt-margin-t-20 kt-margin-b-40" role="alert">
                                                        <div class="alert-icon"><i class="fa fa-exclamation-triangle"></i></div>
                                                        <div class="alert-text">
                                                            <ul>
                                                                <li>Leave it blank if you want to remove the password and save!</li>
                                                                <li>Password must be numeric 6 digits, beetwen 0 - 9.</li>
                                                                <li>Make sure your devices have a keypad!</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3"></label>
                                                <div class="col-6">
                                                    <h3 class="kt-section__title kt-section__title-sm">Change Or Remove Password:</h3>
                                                </div>
                                            </div>
                                            @if($datauser->password == null)

                                            @else
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Current Password</label>
                                                <div class="col-6 ">
                                                    <input type="password" class="form-control" name="currpassword" value="" placeholder="Current password">
                                                </div>
                                            </div>
                                            @endif
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">New Password</label>
                                                <div class="col-6">
                                                    <input type="password" class="form-control" name="passowrd" value="" placeholder="New password">
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-3 col-form-label">Repeat Password</label>
                                                <div class="col-6">
                                                    <input type="password" class="form-control" name="password_confirmation" value="" placeholder="Verify password">
                                                </div>
                                            </div>
                                            <div class="kt-portlet__foot mt-5">
                                                <div class="kt-form__actions">
                                                    <div class="row">
                                                        <div class="col-3">
                                                        </div>
                                                        <div class="col-9">
                                                            <button  onclick="updatePasswordSubmit()" id="btnSavePassword" class="btn btn-label-brand btn-bold">Change Password</button>&nbsp;
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="tab_access">
                                        <div class="kt-widget1 kt-widget1--fit m-5">
                                            <h3 class="kt-section__title kt-section__title-sm">Has access :</h4>
                                            @foreach ($rooms as $index => $room)
                                            <div class="kt-widget1__item">
                                                <div class="kt-widget1__info">
                                                    <h3 class="kt-widget1__title"><a class="kt-font-dark kt-font-bold" href="{{route('placement.detail', [$id_projects, $room->id])}}">{{$index +1}}.&nbsp;{{ $room->name }}</a></h3>
                                                    <span class="kt-widget1__desc">{{ $room->description }}</span>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_setting">
                                        <div class="form-group row m-5">
                                            <label class="col-3 col-form-label">User</label>
                                            <div class="col-9">
                                                <button type="button"  onclick="deleteModalUser('{{$datauser->id}}')" data-toggle="modal" data-target="#deleteModalUser" class="btn btn-outline-danger  btn-sm kt-margin-t-5 kt-margin-b-5">Delete User</button>
                                                <span class="form-text text-muted mt-2">
                                                        Remember, this action cannot be undone and deleted data cannot be restored.
                                                <a href="#" class="kt-link">Learn more</a>.
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-5">
                <div class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--skin-solid kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder kt-portlet__space-x">
                        <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title kt-font-dark ">
                            User Parameters
                        </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                             <span class="kt-font-dark">
                                 @if($datauser->parameters->count() > 0)
                                    {{ $datauser->parameters->count() }} Parameters
                                @else
                                    You don't have any parameter.
                                @endif
                            </span>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <div class="kt-widget17">
                            <div class="kt-widget17__visual kt-widget17__visual--chart kt-portlet-fit--top kt-portlet-fit--sides" style="background-color: #fff">
                                <div class="kt-widget17__chart" style="height:120px;">
                                </div>
                            </div>
                            <div class="row">
                                <div class="kt-widget17__stats">
                                    @forelse ($datauser->parameters as $userparam)
                                    <div class="kt-widget17__items">
                                        <div class="col-12">
                                            <div class="kt-widget17__item">
                                                <a href="javascript:void(0)" class="btn btn-clean btn-icon float-right" data-toggle="dropdown">
                                                    <i class="flaticon-more-1 "></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <a href="{{route('user.log.parameters', [$datauser->id_project, $datauser->id, $userparam->pivot->id])}}" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon-eye"></i>
                                                                <span class="kt-nav__link-text">View</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <h1 class=" user-param-text">
                                                    @if($userparam->pivot->values >= 10000)
                                                        {{ round($userparam->pivot->values/ 1000, 1) . "K"}}
                                                    @else
                                                        {{$userparam->pivot->values}}
                                                    @endif
                                                    <span class="font-units">
                                                        {{$userparam->units }}
                                                    </span>
                                                </h1>

                                                <span class="kt-widget17__subtitle text-capitalize">
                                                    {{$userparam->name}}
                                                </span>
                                                <span class="kt-widget17__desc">
                                                    {{$userparam->pivot->updated_at ? $userparam->pivot->updated_at->diffForHumans() : ' - '}}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    @empty
                                    <p class="text-center"> - </p>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
</div>


       <!-- Modal -->
       <div class="modal fade" id="deleteModalUser" tabindex="-1" role="dialog" aria-labelledby="deleteModalUser" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" id="formDeleteUser" method="post" class="remove-record-model">
                    {{method_field('delete')}}
                    {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    All data related to this user will be deleted and can't be undone!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger"  data-dismiss="modal" onclick="formDeletetSubmit()" >Delete</button>
                </div>
                </form>
            </div>
        </div>
    </div>



<script type="text/javascript">
  "use strict";

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imgcontainer').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#profile_avatar").change(function(){
        readURL(this);
    });

    function updatePasswordSubmit(){
        $('#btnSavePassword').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--brand').attr('disabled', true);
        $('#password_form').submit();
    }

    function updateBasicInfoSubmit(){
        $('#btnSaveBasicInfo').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--brand').attr('disabled', true);
        $('#basicinfo_form').submit();
    }

   
</script>

@endsection
