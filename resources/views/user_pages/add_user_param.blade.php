@extends('layouts.mainlayout')
@section('title', 'Dashboard')
@section('content')

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid mt-3">

        <!--Begin::Section-->
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Add New Field<small>Fill out forms below</small></h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                {{-- <a href="javascript:history.back()" class="btn btn-clean kt-margin-r-10">
                                    <i class="la la-arrow-left"></i>
                                    <span class="kt-hidden-mobile">Back</span>
                                </a> --}}
                                <div class="btn-group">
                                    <button type="button" id="save" class="btn btn-brand" onclick="addSubmit()">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Save</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="kt-portlet__body">
                            <div class="row">
                                <div class="col-1"></div>
                                <div class="col-9">
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                    @endif
                                </div>
                            </div>
                        <form class="kt-form" id="addparam" method="post" action="{{route('save.user.val', $id_projects )}}">
                                @csrf
                                <div class="row">
                                    <div class="col-1"></div>
                                    <div class="col-11">
                                        <div class="kt-section kt-section--first">
                                            <div class="kt-section__body">
                                                {{-- <h3 class="kt-section__title kt-section__title-sm">Basic Info:</h3> --}}
                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label">Allow Remote</label>
                                                    <div class="col-lg-3">
                                                        <span class="kt-switch">
                                                            <label>
                                                                <input type="checkbox" value='1' checked="checked" name="allow_remote"/>
                                                                <span></span>
                                                            </label>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 col-form-label">Field Name</label>
                                                    <div class="col-7">
                                                        <input class="form-control" name="field_name" type="text" placeholder="Enter a field name" value="{{ old('field_name') }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3 col-form-label">Value</label>
                                                    <div class="col-4">
                                                        <input class="form-control" name="value" type="text" placeholder="Enter value" value="{{ old('value') }}">
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="input-group">
                                                            <div class="input-group-append"><span class="input-group-text" id="unit">$</span></div>
                                                            <input type="text" name="unit" class="form-control" placeholder="Unit" aria-describedby="unit">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <!--end::Portlet-->
                </div>
            </div>
        <!--End::Section-->
        </div>
        <!-- end:: Content -->

        <script>
            function addSubmit(){
                $('#save').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                $('#addparam').submit();
            }
        </script>

@endsection
