
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
    <!-- begin::Head -->
    <head><!--begin::Base Path (base relative path for assets of this page) -->
<base href="../../../../"><!--end::Base Path -->
        <meta charset="utf-8"/>

        <title>Verify Email | Cat Space</title>

    <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!--begin::Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">        <!--end::Fonts -->

        <link href="{{ asset('css/pages/login/login-6.css') }}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">        <!--end::Fonts -->


<!--begin:: Global Mandatory Vendors -->
<link href="{{ asset('vendors/general/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" type="text/css" />
<!--end:: Global Mandatory Vendors -->


<link href="{{ asset('vendors/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/multi.min.css')}}" rel="stylesheet" type="text/css"/>
<!--begin::Global Theme Styles(used by all pages) -->

<link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<!--end::Layout Skins -->
<script src="{{ asset('vendors/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.min.js" type="text/javascript"></script>
<link rel="shortcut icon" href="{{ asset('media/logos/favicon.ico') }}" />



        <!--begin::Layout Skins(used by all pages) -->
                <!--end::Layout Skins -->

    <link rel="shortcut icon" href="./assets/media/logos/favicon.ico" />
    </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body  class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >


<!-- end::Page Loader -->
    	<!-- begin:: Page -->
	<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
		<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
        <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
            <div class="kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__body">
                        <div class="kt-login__logo">
                            <a href="#">
                                <img src="{{ asset('media/company-logos/logo-2.png') }}">
                            </a>
                        </div>

                        <div class="kt-login__signin">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">{{ __('Verify Your Email Address') }}</h3>
                            </div>
                            @if(session()->has('message'))
                                <div class="alert alert-solid-brand alert-bold mt-4" role="alert">
                                    <div class="alert-text">{{ session()->get('message') }}</div>
                                </div>
                            @endif
                            <div class="kt-login__form">
                                @if (session('resent'))
                                <div class="alert alert-solid-brand alert-bold kt-font-bold" role="alert">
                                    {{ __('A fresh verification link has been sent to your email address.') }}
                                </div>
                            @endif
                        <p class="text-center">
                            {{ __('Before proceeding, please check your email or spam folder for a verification link.') }}  <br><br>
                            {{ __('If you did not receive the email') }},<br>
                        </p>
                            <form class="kt-form mt-2" method="POST" action="{{ route('verification.resend') }}">
                                @csrf
                                <button type="submit" class="btn btn-brand btn-block btn-pill btn-elevate mt-3">{{ __('Resend Email Verification') }}</button>.
                            </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        {{-- Photo by Robin Glauser on Unsplash --}}
        <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-image: url({{ asset('media/bg/bg-0.jpg')}}">
            <div class="kt-login__section">
                <div class="kt-login__block">
                    <h3 class="kt-login__title"><a class="kt-font-light" href="https://github.com/AsyaSyarif/RFID-Spacecat">Getting Started</a></h3>
                    <div class="kt-login__desc">
                        Spacecat is an Arduino library for ESP8266/ESP32 <br>
                        to makes things easier that requires authentication with an RFID card.
                        <footer class="blockquote-footer kt-font-light">Made With&nbsp;<i class="fas fa-xs fa-heart"></i>&nbsp;Love By Arif</footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>         	</div>


        <script>
            var KTAppOptions = {"colors":{"state":{"brand":"#374afb","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};
        </script>

<script src="{{ asset('vendors/general/jquery/dist/jquery.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/js-cookie/src/js.cookie.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/moment/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js') }}" type="text/javascript"></script>
 <script src="{{ asset('js/scripts.bundle.js') }}" type="text/javascript"></script>

</body>
<script>

function btnlogin(){
    $('#kt_login_signin_submit').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
    $('#formlogin').submit();
}

</script>
</html>

