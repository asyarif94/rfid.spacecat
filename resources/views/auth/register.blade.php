
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
    <!-- begin::Head -->
    <head><!--begin::Base Path (base relative path for assets of this page) -->
<base href="../../../../"><!--end::Base Path -->
        <meta charset="utf-8"/>

        <title>Sign Up | Spacecat</title>

    <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">        <!--end::Fonts -->


<!--begin::Page Custom Styles(used by this page) -->
            <link href="{{ asset('css/pages/login/login-6.css') }}" rel="stylesheet" type="text/css" />
<!--end::Page Custom Styles -->


<!--begin::Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">        <!--end::Fonts -->

<!--end::Page Vendors Styles -->

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">        <!--end::Fonts -->
<link href="{{ asset('css/pages/login/login-6.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">        <!--end::Fonts -->
<link href="{{ asset('vendors/general/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('vendors/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/multi.min.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<!--end::Layout Skins -->
<script src="{{ asset('vendors/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.min.js" type="text/javascript"></script>
<link rel="shortcut icon" href="{{ asset('media/logos/favicon.ico') }}" />


    <link rel="shortcut icon" href="{{ asset('media/logos/favicon.ico') }}" />
<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<!--end::Layout Skins -->
<script src="{{ asset('vendors/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.min.js" type="text/javascript"></script>
<link rel="shortcut icon" href="{{ asset('media/logos/favicon.ico') }}" />

    <link rel="shortcut icon" href="{{ asset('media/logos/favicon.ico') }}" />
    </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body  class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >


<!-- end::Page Loader -->
    	<!-- begin:: Page -->
	<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
		<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
        <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
            <div class="kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__body">
                        <div class="kt-login__logo">
                            <a href="#">
                                <img src="{{ asset('media/company-logos/logo-2.png') }}">
                            </a>
                        </div>

                        <div class="kt-login__options">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">{{ __('Please Sign Up') }}</h3>
                            </div>
                            <div class="row ">
                                <div class="col text-center">
                                    <a id="btn-github" onclick="btnsignup()" href="{{ route('login.github') }}" class="btn btn-pill btn-dark btn-block btn-elevate" >
                                        <span style="margin-right: 10px">
                                            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <g transform="translate(-614.000000, -553.000000)" fill="#FFFFFF" fill-rule="nonzero">
                                                        <g id="github" transform="translate(614.000000, 553.000000)">
                                                            <path d="M12,0.297 C5.37,0.297 0,5.67 0,12.297 C0,17.6 3.438,22.097 8.205,23.682 C8.805,23.795 9.025,23.424 9.025,23.105 C9.025,22.82 9.015,22.065 9.01,21.065 C5.672,21.789 4.968,19.455 4.968,19.455 C4.422,18.07 3.633,17.7 3.633,17.7 C2.546,16.956 3.717,16.971 3.717,16.971 C4.922,17.055 5.555,18.207 5.555,18.207 C6.625,20.042 8.364,19.512 9.05,19.205 C9.158,18.429 9.467,17.9 9.81,17.6 C7.145,17.3 4.344,16.268 4.344,11.67 C4.344,10.36 4.809,9.29 5.579,8.45 C5.444,8.147 5.039,6.927 5.684,5.274 C5.684,5.274 6.689,4.952 8.984,6.504 C9.944,6.237 10.964,6.105 11.984,6.099 C13.004,6.105 14.024,6.237 14.984,6.504 C17.264,4.952 18.269,5.274 18.269,5.274 C18.914,6.927 18.509,8.147 18.389,8.45 C19.154,9.29 19.619,10.36 19.619,11.67 C19.619,16.28 16.814,17.295 14.144,17.59 C14.564,17.95 14.954,18.686 14.954,19.81 C14.954,21.416 14.939,22.706 14.939,23.096 C14.939,23.411 15.149,23.786 15.764,23.666 C20.565,22.092 24,17.592 24,12.297 C24,5.67 18.627,0.297 12,0.297" id="Shape"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </span> Sign Up with Github
                                    </a>
                                    {{-- <a href="#" class="btn btn-dark btn-block"><i class="fab fa-github"></i>Log in with Github</a>&nbsp; --}}
                                </div>
                            </div>
                        </div>

                        <div class="kt-section__content kt-section__content--solid mt-5">
                            <div class="kt-divider">
                                <span></span>
                                <span>or</span>
                                <span></span>
                            </div>
                        </div>

                        <div class="kt-login__signin">
                            <div class="kt-login__head">

                            </div>
                            <div class="kt-login__form">
                                <form class="kt-form" method="POST" action="{{ route('register') }}" id="formregister">
                                    @csrf
                                    <div class="form-group">
                                        <input class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{ old('name') }}" type="text" placeholder="Full Name"  required autocomplete="name" autofocus>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" id="email" type="email" placeholder="Email" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="password" placeholder="Password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <input  id="password_confirmation" type="password" class="form-control" name="password_confirmation" required autocomplete="password_confirmation" placeholder="Confirm Passowrd">
                                    </div>
                                    <div class="kt-login__extra">
                                        <label class="kt-checkbox">
                                            <input type="checkbox" name="agree"> I Agree the <a href="#">terms and conditions</a>.
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="kt-login__actions">
                                        <button id="kt_login_signup_submit" class="btn btn-brand btn-pill btn-elevate"  onclick="registersubmit()">Sign Up</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-login__account">
                    <span class="kt-login__account-msg">
                        Already have an account ?
                    </span>&nbsp;&nbsp;
                    <a href="{{route('login')}}" id="kt_login_signup" class="kt-login__account-link">Sign In!</a>
                </div>
            </div>
        </div>

        {{-- Photo by Robin Glauser on Unsplash --}}
        <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-image: url({{ asset('media/bg/bg-0.jpg')}}">
            <div class="kt-login__section">
                <div class="kt-login__block">
                    <h3 class="kt-login__title"><a class="kt-font-light" href="https://github.com/AsyaSyarif/RFID-Spacecat">Getting Started</a></h3>
                    <div class="kt-login__desc">
                        Spacecat is an Arduino library for ESP8266/ESP32 <br>
                        to makes things easier that requires authentication with an RFID card.
                        <footer class="blockquote-footer kt-font-light">Made With&nbsp;<i class="fas fa-xs fa-heart"></i>&nbsp;Love By Arif</footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>         	</div>

<!-- end:: Page -->


        <!-- begin::Global Config(global config for global JS sciprts) -->
        <script>
            var KTAppOptions = {"colors":{"state":{"brand":"#374afb","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};
        </script>
        <!-- end::Global Config -->

        <script src="{{ asset('vendors/general/jquery/dist/jquery.js') }}" type="text/javascript"></script>
        <script src="{{ asset('vendors/general/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('vendors/general/js-cookie/src/js.cookie.js') }}" type="text/javascript"></script>
        <script src="{{ asset('vendors/general/moment/min/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js') }}" type="text/javascript"></script>
         <script src="{{ asset('js/scripts.bundle.js') }}" type="text/javascript"></script>

    <script src="{{ asset('js/scripts.bundle.js') }}" type="text/javascript"></script>

</body>
  <script>
        function registersubmit(){
            $('#kt_login_signup_submit').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $('#formregister').submit();
        }

        function btnsignup(){
            $('#btn-github').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

        }
  </script>
</html>
