{{-- @extends('errors::minimal')

@section('title', __('Server Error'))
@section('code', '500')
@section('message', __('Server Error')) --}}

<!DOCTYPE html>
<html lang="en" >

    <head>
        <meta charset="utf-8"/>

        <title>Uups.. | Spacecat</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--begin::Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
            <link href="{{ asset('css/pages/error/error-6.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
            <link rel="shortcut icon" href="{{ asset('media/logos/favicon.ico') }}" />

   </head>
    <!-- end::Head -->
<style>
    *{
    transition: all 0.6s;
}

html {
    height: 100%;
}

body{
    font-family: 'Lato', sans-serif;
    color: #888;
    margin: 0;
}

#main{
    display: table;
    width: 100%;
    height: 100vh;
    text-align: center;
}

.fof{
	  display: table-cell;
	  vertical-align: middle;
}

.fof h1{
	  font-size: 50px;
	  display: inline-block;
	  padding-right: 12px;
	  animation: type .5s alternate infinite;
}

@keyframes type{
	  from{box-shadow: inset -3px 0px 0px #888;}
	  to{box-shadow: inset -3px 0px 0px transparent;}
}
</style>
    <!-- begin::Body -->
    <body  class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >

        <div id="main">
            <div class="fof">
                    <h1>Error 500 :(</h1>
            </div>
    </div>

        <script>

        </script>


            </body>

</html>
