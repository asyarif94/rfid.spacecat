@extends('layouts.mainlayout')
@section('title', "Placement ".$room->name)
@section('content')

    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    {{$subheader_title }}
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <ul class="breadcrumb">
                    <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('projects.show', $id_projects) }}">Projects</a></li>
                    <li><a href="{{route('daftar.placement', $id_projects)}}">Placement</a></li>
                    <li>{{$room->name}}</li>
                </ul>
            </div>
            <div class="kt-subheader__toolbar">
                <span class="kt-subheader__desc mr-3" id="kt_subheader_total">Created at {{ \Carbon\Carbon::parse($room->created_at)->format('l, j \\ F Y ') }}</span>
            </div>
        </div>
    </div>
        <!-- end:: Content Head -->
       <!-- begin:: Content -->
       <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Section-->
        <div class="row">

            <div class="col-lg-12 col-xl-12">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__body">
                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top">
                                <div class="kt-widget__media kt-hidden-">
                                    <img src="{{ asset('media/rooms/default.png') }}" alt="image">
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head">
                                        <a href="{{ url('viewroom', [$room->id_project , $room->id])}}" class="kt-widget__username">
                                        @if($room->status == '1')
                                            <span class="kt-badge kt-badge--success kt-badge--dot kt-badge--lg"></span>
                                        @elseif($room->status == '3')
                                            <span class="kt-badge kt-badge--warning kt-badge--dot kt-badge--lg"></span>
                                        @else
                                            <span class="kt-badge kt-badge--danger kt-badge--dot kt-badge--lg"></span>
                                        @endif
                                            {{ $room->name }}
                                        </a>
                                        <div class="kt-widget__action">
                                            <div class="dropdown">
                                                <button type="button" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Options
                                                    </button>
                                                <div class="dropdown-menu">
                                                    <ul class="kt-nav">
                                                        <div class="dropdown-divider"></div>
                                                        <li class="kt-nav__item">
                                                            <a href="javaScript:void(0);" class="kt-nav__link" data-toggle="modal" data-target="#credentialsmodal">
                                                                <i class="kt-nav__link-icon flaticon2-ui"></i>
                                                                <span class="kt-nav__link-text">Credentials</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">

                                                            <a href="{{ route('placement.edit', [ $room->id_project, $room->id ]) }}" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                                <span class="kt-nav__link-text">Edit</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="javascript:;" class="kt-nav__link" onclick="deleteModalRoom('{{$room->id}}')" data-toggle="modal" data-target="#deleteModalRoom">
                                                                <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                                <span class="kt-nav__link-text">Delete</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-widget__info">
                                        <div class="kt-widget__desc">
                                            <div class="row">
                                                <div class="col-8">
                                                    {{ $room->description }}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="kt-widget__bottom">
                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-pie-chart "></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Total Taping Card</span>
                                        <span class="kt-widget__value">{{$room->tapToday->count()}}<span class="small">&nbsp;Times Today</span></span>
                                    </div>
                                </div>

                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-diagram"></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Total Taping Card</span>
                                        <span class="kt-widget__value">{{$room->allTap->count()}}<span class="small">&nbsp;All Time</span></span>
                                    </div>
                                </div>

                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-users "></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Total Users</span>
                                        <span class="kt-widget__value">{{$room->associatedUser->groupBy('id_user')->count() }}<span class="small">&nbsp;Users</span></span>
                                    </div>
                                </div>

                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-rocket"></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Device Connected</span>
                                        <span class="kt-widget__value">{{$devices->count() }}<span class="small">&nbsp;Devices</span></span>
                                    </div>
                                </div>

                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-interface-7"></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Settings</span>
                                        <a href="{{route('placement.settings', [$room->id_project, $room->id])}}" class="kt-widget__value kt-font-dark kt-label-font-color-3">Preferences</a>
                                    </div>
                                </div>

                                <div class="kt-widget__item">
                                    <div class="kt-widget__icon">
                                        <i class="flaticon-statistics"></i>
                                    </div>
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">Log</span>
                                        <a href="{{url('log', [$id_projects, $room->id])}}" class="kt-widget__value kt-font-brand">View</a>
                                    </div>
                                </div>

                                {{-- <div class="kt-widget__item">
                                    <a href="{{url('addrules', [$room->id_project,$room->id])}}" class="btn btn-label-brand btn-sm btn-bold  btn-block">Add Rule</a>
                                </div> --}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($room->enable_parameters == 1)
                <div class="col-lg-12 col-xl-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__body  kt-portlet__body--fit">
                            <div class="row row-no-padding row-col-separator-lg">
                                @foreach ($room->placementparameters as $item)
                                <div class="col-md-12 col-lg-6 col-xl-3">
                                    <div class="kt-widget24">
                                        <div class="kt-widget24__details">

                                            <div class="kt-widget24__info">
                                                <h4 class="kt-widget24__title">
                                                <a href="{{route('placement.edit.settings', [$room->id_project, $room->id, $item->id])}}" class=""> {{$item->name}} </a>
                                                </h4>

                                                <span class="kt-widget24__desc">
                                                    {{$item->sub_name}}
                                                </span>
                                            </div>

                                            <span class="kt-widget24__stats kt-label-font-color-2">
                                                @if($item->value >= 10000)
                                                    {{round($item->value/1000,1). "K"}}<small>&nbsp;{{$item->unit}}</small>
                                                @else
                                                    {{$item->value}} <small>{{$item->unit}}</small>
                                                @endif
                                            </span>
                                        </div>

                                            <span class="kt-widget24__change">
                                                @if($item->operator == "add")
                                                Addition
                                                @elseif($item->operator == "sub")
                                                Subtraction
                                                @elseif($item->operator == "perctpls")
                                                Percent++
                                                @elseif($item->operator == "perctmin")
                                                Percent--
                                                @elseif($item->operator == "replc")
                                                Equals
                                                @endif
                                            </span>
                                            by {{$item->by_value}}{{$item->unit}}</small>
                                            <div class="progress progress--sm">
                                                @if($item->value == 0 && $item->max_value == 00 && $item->min_value == 0)
                                                        <div class="progress-bar bg-brand" role="progressbar"
                                                            style="width: 0%;"
                                                            aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                @else
                                                        <div class="progress-bar bg-brand" role="progressbar"
                                                            style="width: {{$item->value*100/$item->max_value}}%;"
                                                            aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                @endif
                                            </div>

                                        <div class="kt-widget24__action">
                                            <span class="kt-label-font-color-2">Min {{$item->min_value}}</span>
                                            <span class="kt-widget24__number">
                                                <span class="kt-label-font-color-2">
                                                    @if($item->max_value >= 10000)
                                                        {{round($item->max_value/1000,1). "K"}}<small>&nbsp;{{$item->unit}}</small>
                                                    @else
                                                        {{$item->max_value}} <small>{{$item->unit}}</small>
                                                    @endif
                                            </span>
                                        </div>

                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @else
            @endif
        </div>
        @if($room->status == '1')
        @else
        <div class="alert alert-light alert-elevate fade show" role="alert">
            <div class="alert-icon "><i class="flaticon-warning kt-font-danger"></i></div>
            <div class="alert-text">
                <strong>Attention!</strong>&nbsp; User cannot access to this placement if status is inactive,
                <br>
                For more info please visit the page's <a class="kt-link kt-font-bold" href="#" target="_blank">Guide</a>.
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-xl-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Rules
                            </h3>

                        </div>
                        <div class="clearfix">
                            <div class="float-left"></div>
                            <div class="float-right mt-3">
                                <a href="{{route('add.rules', [$room->id_project,$room->id])}}" class="btn btn-label-brand btn-sm btn-bold  btn-block">Add Rule</a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <!--begin::Section-->
                        <div class="kt-section">
                            <span class="kt-section__info">
                                @if($room->therules->isEmpty())
                                <div class="kt-block-center">
                                    {{-- deskripsi --}}
                                </div>
                                @endif
                            </span>
                            <div class="kt-section__content">
                                <table class="table table-hover table-striped w-100 d-print-block d-print-table">
                                      <thead class="thead-light">
                                        <tr class="text-center">
                                              <th>Rule</th>
                                              <th>Schedule</th>
                                              <th>Time Start</th>
                                              <th>Time End</th>
                                              <th>Offset Time</th>
                                              <th>Users</th>
                                              <th>Parameters</th>
                                              <th>By</th>
                                              <th>Options</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($room->therules as $rules)
                                        <tr class="text-center">
                                            <td >
                                                <a href="{{url('rule/view', [$room->id_project, $room->id, $rules->id])}}" class="kt-widget__username ">
                                                    @if($rules->status == '1')
                                                    <span class="kt-badge kt-badge--success kt-badge--dot kt-badge--lg mb-1"></span>
                                                    @elseif($rules->status == '3')
                                                        <span class="kt-badge kt-badge--warning kt-badge--dot kt-badge--lg mb-1"></span>
                                                    @else
                                                        <span class="kt-badge kt-badge--danger kt-badge--dot kt-badge--lg mb-1"></span>
                                                    @endif
                                                    <span class="kt-font-bold kt-font-primary">{{$rules->rule_name}} </span><br>
                                                </a>
                                                <span class="d-flex justify-content-center kt-font-dark">{{$rules->sub_name}}</span>
                                            </td>
                                            <td class="text-capitalize">
                                                {{$rules->day}}
                                            </td>
                                        <td>
                                            @if($rules->start_time != 0)
                                                {{ Carbon\Carbon::parse($rules->start_time)->format('H:i') }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if($rules->end_time != 0)
                                                {{ Carbon\Carbon::parse($rules->end_time)->format('H:i') }}
                                            @else
                                             -
                                            @endif
                                        </td>
                                        <td>
                                            @if($rules->offset_time != 0)
                                                {{$rules->offset_time}}&nbsp;Min
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            <div class="kt-media-group d-flex justify-content-center">
                                            {{-- {{$rules->list_user}} --}}
                                                @if($rules->list_user->count() == 0)
                                                <p>-</p>
                                                @endif
                                                @foreach ($rules->list_user->slice(0, 5)->sortByDesc('created_at') as $aUser)
                                                    <a href="{{ route('lihat.user', [$aUser->id_project, $aUser->id_user]) }}" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="dark" data-placement="top" title="" data-original-title="{{@$aUser->user->name}}">
                                                        <img src="{{ asset($aUser->user->url_avatar)}}" alt="image">
                                                    </a>
                                                @endforeach
                                                @if($rules->list_user->count() > 5)
                                                <a href="#" class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-skin="dark" data-placement="top" title="" data-original-title="And more">
                                                    <span>+{{$rules->list_user->count() - 5}}</span>
                                                </a>
                                                @else
                                                @endif
                                            </div>
                                        </td>
                                        <td>

                                            @if(isset($rules->conectorparameter->parameters->name))
                                                <a href="" class="kt-widget__username kt-font-bold"> {{ $rules->conectorparameter->parameters->name }} </a>
                                                <span class="d-flex justify-content-center kt-font-dark">
                                                    @if($rules->conectorparameter->operator == "add")
                                                    <span class="d-flex justify-content-center kt-font-dark">Addition</span>
                                                @elseif($rules->conectorparameter->operator == "sub")
                                                    <span class="d-flex justify-content-center kt-font-dark">Subtraction </span>
                                                @elseif($rules->conectorparameter->operator == "perctpls")
                                                    <span class="d-flex justify-content-center kt-font-dark">Percent++ </span>
                                                @elseif($rules->conectorparameter->operator == "perctmin")
                                                    <span class="d-flex justify-content-center kt-font-dark">Percent-- </span>
                                                @elseif($rules->conectorparameter->operator == "replc")
                                                    <span class="d-flex justify-content-center kt-font-dark">Equals </span>
                                                @endif</span>
                                            @endif
                                        </td>

                                        <td>
                                            @if(isset($rules->conectorparameter->by_value))
                                                {{ $rules->conectorparameter->by_value }}
                                            @endif
                                                &nbsp;
                                            @if(isset($rules->conectorparameter->parameters->units))
                                                {{ $rules->conectorparameter->parameters->units }}
                                            @endif </td>
                                        <td>
                                            <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-clean btn-icon" data-toggle="dropdown">
                                                    <i class="flaticon-more-1"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <a href="{{url('rule/view', [$room->id_project, $room->id, $rules->id])}}" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon-eye"></i>
                                                                <span class="kt-nav__link-text">View</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="#" onclick="deletrule('{{$rules->id}}')" data-toggle="modal" data-target="#deleteModalRule" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                                <span class="kt-nav__link-text">Delete</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <!-- end:: Content -->
    <div class="modal fade" id="credentialsmodal" tabindex="-1" role="dialog" aria-labelledby="credentialsmodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" id="formcredentialsmodal" method="post" class="remove-record-model">
                    {{method_field('delete')}}
                    {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="deletemodallabel">Please keep it secret</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center"> <code> Your secret credentials are used authenticate access to this room.</br>
                     Keep the 'Credentials' a secret, this should never be human-readable. </code></p>
                    </br>
                     <div class="kt-section__body">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text" id="basic-addon2">Username</span></div>
                                <input type="text" class="form-control" value="{{$projects_[0]->project_key}}"  readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text" id="basic-addon2">Password</span></div>
                                <input type="text" class="form-control" value="{{$room->secret_key}}"  readonly="readonly">
                                </div>
                            </div>
                        </div>
                    </div>
                    Clik <a href="#">here</a> if you don't know how to use it.
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-block" data-dismiss="modal" >Close</button>
                </div>
            </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="deleteModalRoom" tabindex="-1" role="dialog" aria-labelledby="deleteModalRoom" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" id="formDeleteUser" method="post" class="remove-record-model">
                    {{method_field('delete')}}
                    {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    All data related to this room will be deleted and can't be undone!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger"  data-dismiss="modal" onclick="formDeletetSubmit()" >Delete</button>
                </div>
             </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModalRule" tabindex="-1" role="dialog" aria-labelledby="deleteModalRule" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" id="formDeleteRule"  method="delete" class="remove-record-model">
                    {{method_field('delete')}}
                    {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure to delete the rule ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger"  data-dismiss="modal" onclick="formDeleteRuleSubmit()" >Delete</button>
                </div>
             </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModalField" tabindex="-1" role="dialog" aria-labelledby="deleteModalField" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" id="formDeleteField"  method="delete" class="remove-record-model">
                    {{method_field('delete')}}
                    {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure to delete the field ?
                    {{-- <input type="hidden" name="id" id="id"> --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger"  data-dismiss="modal" onclick="formDeleteFieldSubmit()" >Delete</button>
                </div>
             </form>
            </div>
        </div>
    </div>

<script type="text/javascript">
    "use strict";


</script>
@endsection
