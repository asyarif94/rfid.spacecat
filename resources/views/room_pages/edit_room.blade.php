
@extends('layouts.mainlayout')
@section('title', 'Dashboard')
@section('content')
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid mt-3">

    <!--Begin::Section-->

    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <div class="kt-subheader__main mb-3">
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <ul class="breadcrumb">
                        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li><a href="{{ route('projects.show', $id_projects) }}">Projects</a></li>
                        <li><a href="{{route('daftar.placement', $id_projects)}}">Placement</a></li>
                        <li><a href="{{route('placement.detail', [$id_projects, $room->id])}}">{{$room->name}}</a></li>
                        <li>Edit</li>
                    </ul>
            </div>
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Edit<small>Fill out forms below</small></h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="javascript:history.back()" class="btn btn-clean kt-margin-r-10">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">Back</span>
                        </a>
                        <div class="btn-group">
                            <button type="button" id="save" class="btn btn-brand" onclick="addSubmit()">
                                <i class="la la-check"></i>
                                <span class="kt-hidden-mobile">Save</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif

                    <form class="kt-form" id="addrom" method="post" action="{{ route('placement.update', $room->id) }}">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-8">
                                <div class="kt-section kt-section--first">
                                    <div class="kt-section__body">
                                        <h3 class="kt-section__title kt-section__title-sm">Basic Info:</h3>
                                        <input type="hidden" name="id" id="id" value="{{ $room->id }}">
                                        <input type="hidden" name="id_project" id="id_project" value="{{ $room->id_project }}">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label">Name</label>
                                            <div class="col-8">
                                                <input class="form-control" name="name" type="text" placeholder="Room Name" value="{{ old('nama', $room->name) }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label" for="status">Status :</label>
                                            <div class="col-8">
                                                <span class="kt-switch">
                                                    <label>
                                                        @if($room->status)
                                                            <input type="checkbox" checked="checked" name="status" value ="1">
                                                        @else
                                                            <input type="checkbox"  name="status" value="1">
                                                        @endif
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-last row">
                                            <label class="col-3 col-form-label">Description</label>
                                            <div class="col-8">
                                                <textarea  name="description" class="form-control" id="description" rows="5">{{old('description', $room->description)}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        <!--end::Portlet-->
        </div>
    </div>
    <!--End::Section-->


    </div>
    <!-- end:: Content -->

    <script>
        function addSubmit(){
            $('#save').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $('#addrom').submit();
        }
    </script>

@endsection
