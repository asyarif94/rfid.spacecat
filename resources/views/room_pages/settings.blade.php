@extends('layouts.mainlayout')
@section('title', "Setting ".$page_title)
@section('content')

<div class="kt-container  kt-grid__item kt-grid__item--fluid mt-3 mb-3">
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <div class="kt-subheader__main mb-3">
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <ul class="breadcrumb text-capitalize">
                    <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ route('projects.show', $id_projects) }}">Projects</a></li>
                    <li><a href="{{route('daftar.placement', $id_projects)}}">Placement</a></li>
                    <li><a href="{{route('placement.detail', [$id_projects, $room->id])}}">{{$room->name}}</a></li>
                    <li>Settings</li>
                </ul>
            </div>

                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="kt-portlet kt-portlet--tabs">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        Settings
                                    </h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                    <ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#general_tab" role="tab">
                                              General
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#advanced_tab" role="tab">
                                               Advanced
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general_tab" role="tabpanel">
                                        <div class="kt-portlet__body">
                                            <form class="kt-form" id="save" method="post" action="{{ route('placement.update.settings', [$room->id_project, $room->id]) }}">
                                                @csrf
                                                <div class="form-group row">
                                                    <div class="col-3"></div>
                                                    <div class="col-8">
                                                        <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                            <input type="checkbox" name="en_placement_parameters"
                                                            @if($room->enable_parameters == '1')
                                                            checked
                                                            @else

                                                            @endif
                                                            > Enable Placement Parameters
                                                            <span></span>
                                                        </label>
                                                        <span class="form-text text-muted">By enabling this parameters you can do something fun.</span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="kt-portlet__foot">
                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-lg-3 col-xl-3">
                                                    </div>
                                                    <div class="col-9">
                                                        <button onclick="saveUbmit()" id="btnSaveBasicInfo" class="btn btn-label-brand btn-bold">Save Changes</button>&nbsp;
                                                        <button type="reset" class="btn btn-clean btn-bold">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="advanced_tab" role="tabpanel">
                                        <div class="clearfix">
                                            <div class="float-left">
                                                You can even customize the return value or structure of json.
                                            </div>
                                            <div class="float-right">
                                            <span class="kt-switch">
                                                <label>
                                                    <input type="checkbox" value='1' name="en_return_custom"/>
                                                    <span></span>
                                                </label>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label >Default</label>
                                                    <div class="kt-checkbox-list">
                                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                                                            <input type="checkbox" checked="checked" disabled> Basic Info
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                    <label class="mt-2">Placement</label>
                                                    <div class="kt-checkbox-list">
                                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                                                            <input type="checkbox" checked="checked" disabled> Placement Parameters
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                    <div class="kt-checkbox-list ml-4">
                                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                                                            <input type="checkbox" checked="checked" disabled> Param 1
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                    <div class="kt-checkbox-list ml-4">
                                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                                                            <input type="checkbox" checked="checked" disabled> Param 2
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                    <label class="mt-2">User</label>
                                                    <div class="kt-checkbox-list">
                                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                                                            <input type="checkbox" checked="checked" disabled> User Parameters
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                    <div class="kt-checkbox-list ml-4">
                                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                                                            <input type="checkbox" checked="checked" disabled> Param 1
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                    <div class="kt-checkbox-list ml-4">
                                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                                                            <input type="checkbox" checked="checked" disabled> Param 2
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-9">
                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1">Example textarea</label>
                                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="20" cols="50">
{
    "code": 104,
    "reference": "2304626799",
    "name": "Arif",
    "messages": "success",
    "placement" : {
        "param1" : "null",
        "param2" : "null",
        "param3" : "null"
    },
    "user" : {
        "param1" : "null",
        "param2" : "null",
        "param3" : "null"
    }
}
                                                    </textarea>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        </div>
    </div>
</div>

<script>
    "use strict";
    function saveUbmit(){
            $('#btnSaveBasicInfo').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--brand').attr('disabled', true);
            $('#save').submit();
        }
</script>
@endsection
