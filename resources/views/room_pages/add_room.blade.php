
@extends('layouts.mainlayout')
@section('title', 'Add Placement')
@section('content')

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid mt-3 mb-3">

    <!--Begin::Section-->
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="kt-subheader__main mb-3">
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <ul class="breadcrumb">
                        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li><a href="{{ route('projects.show', $id_projects) }}">Projects</a></li>
                        <li><a href="{{route('daftar.placement', $id_projects)}}">Placement</a></li>
                        <li>Add New</li>
                    </ul>
                </div>
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Add New Placement<small>Fill out forms below</small></h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            {{-- <a href="javascript:history.back()" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a> --}}
                            <div class="btn-group">
                                <button type="button" id="save" class="btn btn-brand" onclick="addSubmit()">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Save</span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-8">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                                @endif
                            </div>
                        </div>
                        <form class="kt-form" id="addrom" method="post" action="{{ route('room.store') }}">
                            @csrf
                            <div class="row">
                                <div class="col-2"></div>
                                <div class="col-10">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <h3 class="kt-section__title kt-section__title-sm">Basic Info:</h3>
                                            <input type="hidden" name="id" id="id" value="{{ $id_projects }}">
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Name</label>
                                                <div class="col-7">
                                                    <input class="form-control" name="name" type="text" placeholder="Enter Placement Name" value="{{ old('name') }}">
                                                    <span class="form-text text-muted">e.g : room/class/things</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label" for="status">Status :</label>
                                                <div class="col-7">
                                                    <span class="kt-switch">
                                                        <label>
                                                            <input type="checkbox" value='1' checked="checked" name="status"/>
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-3 col-form-label">Description</label>
                                                <div class="col-7">
                                                    <textarea  name="description" class="form-control" id="description" placeholder="Description about your placement" rows="5">{{ old('description') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            <!--end::Portlet-->
            </div>
        </div>
    <!--End::Section-->
    </div>
    <!-- end:: Content -->

    <script>
        function addSubmit(){
            $('#save').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $('#addrom').submit();
        }
    </script>

@endsection
