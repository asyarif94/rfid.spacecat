
@extends('layouts.mainlayout')
@section('title', "Placements ".$page_title)
@section('content')

  <!-- begin:: Content Head -->
  <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    {{$subheader_title }}
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                <ul class="breadcrumb">
                    <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ route('projects.show', $id_projects) }}">Projects</a></li>
                    <li><a href="{{route('daftar.placement', $id_projects)}}">Placement</a></li>
                    <li>List</li>
                </ul>

            </div>
            <div class="kt-subheader__toolbar">
                <form class="kt-margin-r-10 " id="kt_subheader_search_form">
                    {{ csrf_field() }}
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                        <input type="text" class="form-control" placeholder="Search..." id="cariUser">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"/>
                                        <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                        <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"/>
                                    </g>
                                </svg>                                    <!--<i class="flaticon2-search-1"></i>-->
                            </span>
                        </span>
                    </div>
                </form>
                @if($getRoom->count() >=  auth()->user()->plan->total_room)
                <a href="#" class="btn btn-label-brand btn-sm btn-bold disabled">Add New</a>
                @else
                <a href="{{ route('placement.addnew', $id_projects)}}" class="btn btn-label-brand btn-sm btn-bold">Add New</a>
                @endif
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="row">
            @if($getRoom->isEmpty())
            <div class="kt-block-center">
                <p class="text-center kt-font-bold">No placement found!</p><br>
                <p class="text-center"> Click <b class="kt-font-info">Add New</b> button to add the placement to yur project.</p>
            </div>
            @endif
            @foreach ($getRoom->sortByDesc('created_at') as $room)
            <div class="col-lg-3 col-xl-3">
                <!--begin:: Widgets/Blog-->
                <div class="kt-portlet kt-portlet--height-fluid kt-widget19">
                    <div class="kt-portlet__body kt-portlet__body--fit kt-portlet__body--unfill">
                        <div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides" style="min-height: 300px; background-image: url({{ asset('media//rooms/default.png')}})">
                            {{-- Photo by Christian Stahl on Unsplash --}}
                            <a href="javascript:void(0)" class="btn btn-clean btn-icon mt-1 mr-1 float-right" data-toggle="dropdown">
                                <i class="flaticon-more-1 "></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="{{ route('placement.detail', [$room->id_project , $room->id]) }}" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon-eye"></i>
                                            <span class="kt-nav__link-text">View</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="{{ route('placement.edit', [ $room->id_project, $room->id ]) }}" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-edit"></i>
                                            <span class="kt-nav__link-text">Edit</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="" onclick="deleteModalRoom('{{$room->id}}')" data-toggle="modal" data-target="#deleteModalRoom" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Delete</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <h3 class="kt-widget19__title mb-4 kt-font-light">
                                <a class="kt-font-light text-capitalize" href="{{ route('placement.detail', [$room->id_project , $room->id]) }}">
                                    @if($room->status == '1')
                                        <span class="kt-badge kt-badge--success kt-badge--dot"></span>
                                    @else
                                        <span class="kt-badge kt-badge--danger kt-badge--dot"></span>
                                    @endif
                                    &nbsp;{{$room->name}}
                                </a>
                            </h3>
                            <h6  class="kt-widget19__title kt-font-light mt-4 ml-4 blockquote-footer">{{ \Carbon\Carbon::parse($room->created_at)->longRelativeDiffForHumans() }}</h6>
                            <div class="kt-widget19__shadow"></div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget19__wrapper">
                            <div class="kt-widget19__text">
                                    {{$room->description}}
                            </div>
                        </div>
                        <div class="kt-widget19__action">
                            <a href="{{ route('placement.detail', [$room->id_project , $room->id]) }}" class="btn btn-sm btn-label-brand btn-bold">View Details</a>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Blog-->
            </div>
            @endforeach
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="deleteModalRoom" tabindex="-1" role="dialog" aria-labelledby="deleteModalRoom" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" id="formDeleteUser" method="post" class="remove-record-model">
                    {{method_field('delete')}}
                    {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="deletemodallabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    All data related to this room will be deleted and can't be undone!
                    {{-- <input type="hidden" name="id" id="id"> --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger"  data-dismiss="modal" onclick="formDeletetSubmit()" >Delete</button>
                </div>
             </form>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
    <script>
    "use strict";

    </script>
@endsection

