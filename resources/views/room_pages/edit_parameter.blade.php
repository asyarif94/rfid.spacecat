
@extends('layouts.mainlayout')
@section('title', 'Add Room')
@section('content')

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid mt-3 mb-3">

    <!--Begin::Section-->
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="kt-subheader__main mb-3">
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <ul class="breadcrumb">
                        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li><a href="{{ route('projects.show', $id_projects) }}">Projects</a></li>
                        <li><a href="{{route('daftar.placement', $id_projects)}}">Placement</a></li>
                        <li><a href="{{route('placement.detail', [$id_projects, $room->id])}}">{{$room->name}}</a></li>
                        <li>{{$parameter->name}}</li>
                    </ul>
                </div>
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Placement Parameters<small>{{$parameter->name}}</small></h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="btn-group">
                                <button type="button" id="save" class="btn btn-brand" onclick="addSubmit()">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Save</span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-8">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                                @endif
                            </div>
                        </div>
                        <form class="kt-form" id="saveparam" method="post" action="{{ route('placement.save.settings', [$id_projects, $room->id, $parameter->id]) }}">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="col-2"></div>
                                <div class="col-10">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Name</label>
                                                <div class="col-7">
                                                    <input class="form-control" name="name" type="text" placeholder="Enter Placement Name" value="{{ old('name', $parameter->name) }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Sub Name</label>
                                                <div class="col-7">
                                                    <input class="form-control" name="sub_name" type="text" placeholder="Enter Placement Name" value="{{ old('sub_name', $parameter->sub_name) }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label" for="status">Allow Remote</label>
                                                <div class="col-7">
                                                    <span class="kt-switch">
                                                        <label>
                                                            <input type="checkbox" value='1' checked="checked" name="allow_remote"/>
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Operator</label>
                                                <div class="col-7">
                                                    <select class="form-control" id="operator" name="operator">
                                                        <option selected disabled hidden>-</option>
                                                        <option value="add" @if($parameter->operator == 'add') {{ 'selected' }} @endif >[+] Addition</option>
                                                        <option value="sub" @if($parameter->operator == 'sub') {{ 'selected' }} @endif>[-] Subtraction</option>
                                                        <option value="perct" @if($parameter->operator == 'perctmin') {{ 'selected' }} @endif>[%] Percent--</option>
                                                        <option value="perct" @if($parameter->operator == 'perctmin') {{ 'selected' }} @endif>[%] Percent++</option>
                                                        <option value="replc" @if($parameter->operator == 'replc') {{ 'selected' }} @endif>[X] Replace</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">By Value</label>
                                                <div class="col-3">
                                                    <input class="form-control" name="by_value" type="text" placeholder="Value" value="{{ old('by_value', $parameter->by_value) }}">
                                                </div>
                                                <label class="col-1 col-form-label">Unit</label>
                                                <div class="col-3">
                                                    <input class="form-control" name="unit" type="text" placeholder="Unit" value="{{ old('unit', $parameter->unit) }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Min Value</label>
                                                <div class="col-3">
                                                    <input class="form-control" name="min_value" type="text" placeholder="Min Value" value="{{ old('min_value', $parameter->min_value) }}">
                                                </div>
                                                <label class="col-1 col-form-label">Max</label>
                                                <div class="col-3">
                                                    <input class="form-control" name="max_value" type="text" placeholder="Max Value" value="{{ old('max_value', $parameter->max_value) }}">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            <!--end::Portlet-->
            </div>
        </div>
    <!--End::Section-->
    </div>
    <!-- end:: Content -->

    <script>
        function addSubmit(){
            $('#save').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $('#saveparam').submit();
        }
    </script>

@endsection
