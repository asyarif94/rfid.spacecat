@extends('layouts.mainlayout')
@section('title', 'Add Project')
@section('content')

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">

        </div>
    </div>
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <ul class="breadcrumb">
                    <li class="mb-3"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li ><a href="{{route('dashboard')}}">Project</a></li>
                    <li>Add New</li>
                </ul>
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Add New Project<small>Fill out forms below</small></h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <button type="button" class="btn btn-brand" onclick="addSubmit()" id="save">
                                <i class="la la-check"></i>
                                <span class="kt-hidden-mobile">Save</span>
                            </button>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                        <div class="col-2"></div>
                        <div class="col-8">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                            @endif
                        </div>
                        </div>
                        <form class="kt-form" id="addproject" method="post" action="{{ route('projects.store') }}">
                            @csrf
                            <div class="row">
                                <div class="col-2"></div>
                                <div class="col-8">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <h3 class="kt-section__title kt-section__title-lg">Basic Info:</h3>

                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">Project Name</label>
                                                <div class="col-9">
                                                    <input class="form-control" name="name" type="text" placeholder="Enter your project name" value="{{ old('name') }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label" for="statusrule">Status :</label>
                                                <div class="col-9">
                                                    <span class="kt-switch">
                                                        <label>
                                                            <input type="checkbox" value='1' checked="checked" name="status"/>
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-3 col-form-label">Description</label>
                                                <div class="col-9">
                                                    <textarea  name="description" placeholder="Give a description about your project here" class="form-control" id="description" rows="3">{{ old('description') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-2"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



</div>
    <script>
        function addSubmit(){
            $('#save').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $('#addproject').submit();
        }
    </script>

@endsection
