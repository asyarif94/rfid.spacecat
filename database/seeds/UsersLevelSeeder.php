<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_level')->insert([
            'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name_level' => 'Administrator',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
