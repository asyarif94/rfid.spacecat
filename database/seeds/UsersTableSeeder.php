<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!User::count()){
            User::create([
                'name'      => 'Arif',
                'email'     => 'a.syarifudin@windowslive.com',
                'status'    => '1',
                'id_plan'       => env('PLAN_ID'),
                'id_level'      => env('LEVEL_ID'),
                'password'  => bcrypt('kmzwa8awaa')

            ]);
        }
    }
}

/*
public function run()
    {
        if(!User::count()){
            User::create([
                'name'      => 'Arif',
                'email'     => 'a.syarifudin@windowslive.com',
                'status'    => '1',
                'id_plan'   => '474c114f-8e15-4f25-bfda-b90052701699',
                'id_level'  => 'ca37e82d-235d-4246-a850-7861d9282ef3',
                'password'  => bcrypt('kmzwa8awaa')

            ]);
        }
    }

*/
