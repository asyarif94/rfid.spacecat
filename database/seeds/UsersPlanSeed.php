<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UsersPlanSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_plan')->insert([
            'id'                => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'              => 'Basic',
            'total_project'     => '1',
            'total_users'       => '10',
            'total_room'        => '1',
            'total_rules'       => '1',
            'total_schedule'    => '1',
            'total_amount'      => '1',
            'total_devices'     => '2',
            'total_request'     => '50',
            'price'             => 'free',
            'description'       => 'for beta testing only. ',
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now(),
        ]);
    }
}
