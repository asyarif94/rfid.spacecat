<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('id_room');
            $table->string('id_project');
            $table->string('device_name');
            $table->string('type_devices');
            $table->string('firmware_version');
            $table->string('ssid');
            $table->string('wifi_strength');
            $table->string('secret_key_room');
            $table->string('project_key');
            $table->string('mac_address');
            $table->string('local_ip_address');
            $table->string('esp_chip_id');
            $table->string('core_version');
            $table->string('sdk_version');
            $table->string('boot_version');
            $table->string('boot_mode');
            $table->string('uptime');
            $table->string('rc522_chip_version');
            $table->timestamps();

            $table->foreign('id_room')
                ->references('id')->on('roomdata')
                ->onDelete('cascade');

            $table->foreign('id_project')
                ->references('id')->on('projects')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
