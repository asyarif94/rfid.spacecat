<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRulesAccess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('rules_access', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('id_project')->nullable();
            $table->string('id_rules')->nullable();
            $table->string('project_key');
            $table->string('id_room')->nullable();
            $table->string('secret_key');
            $table->string('id_user')->nullable();;
            $table->string('rfid_user');
            $table->timestamps();

            $table->foreign('id_project')
                  ->references('id')->on('projects')
                  ->onDelete('cascade');

            $table->foreign('id_room')
                  ->references('id')->on('roomdata')
                  ->onDelete('cascade');

            $table->foreign('id_user')
                  ->references('id')->on('usersdata')
                  ->onDelete('cascade');

            $table->foreign('id_rules')
                  ->references('id')->on('rulesonroom')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rules_access');
    }
}
