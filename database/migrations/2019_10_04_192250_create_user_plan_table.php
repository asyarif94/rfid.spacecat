<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_plan', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('total_project');
            $table->string('total_users');
            $table->string('total_room');
            $table->string('total_parameter');
            $table->string('total_rules');
            $table->string('total_schedule');
            $table->string('total_amount');
            $table->string('total_devices');
            $table->string('total_request');
            $table->string('total_user_field');
            $table->string('total_placement_field');
            $table->string('price');
            $table->mediumText('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_plan');
    }
}
