<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomdata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('roomdata', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('id_project');
            $table->string('name');
            $table->boolean('status');
            $table->boolean('enable_parameters')->default('0');
            $table->string('secret_key');
            $table->mediumText('description')->nullable();
            $table->timestamps();

            $table->foreign('id_project')
                  ->references('id')->on('projects')
                  ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roomdata');
    }
}
