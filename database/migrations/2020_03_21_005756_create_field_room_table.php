<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_room', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('id_placement');
            $table->string('field_name');
            $table->boolean('status')->default('1');
            $table->string('value')->default('0');
            $table->string('storage')->default('0');
            $table->string('units');
            $table->timestamps();

            $table->foreign('id_placement')
                    ->references('id')->on('roomdata')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_room');
    }
}
