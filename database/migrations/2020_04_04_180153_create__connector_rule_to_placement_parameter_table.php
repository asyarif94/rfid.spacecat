<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConnectorRuleToPlacementParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connector_rule_to_placement_parameter', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('id_rule');
            $table->string('id_placement_param');
            $table->timestamps();

            $table->foreign('id_placement_param')
                    ->references('id')->on('placement_parameters')
                    ->onDelete('cascade');

            $table->foreign('id_rule')
                    ->references('id')->on('rulesonroom')
                    ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_connector_rule_to_placement_parameter');
    }
}
