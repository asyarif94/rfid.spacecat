<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogUserParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_user_parameter', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('user_id');
            $table->string('parameter_id');
            $table->string('last_value');
            $table->string('new_value');
            $table->string('operator');
            $table->timestamps();


            $table->foreign('user_id')
                    ->references('id')->on('usersdata')
                    ->onDelete('cascade');

            $table->foreign('parameter_id')
                    ->references('id')->on('users_to_parameters')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_user_parameter');
    }
}
