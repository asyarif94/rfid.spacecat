<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersdata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersdata', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('id_project');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phonenr')->nullable();
            $table->string('rfid');
            $table->string('password')->nullable();
            $table->boolean('status');
            $table->string('avatar')->nullable();
            $table->longText('description')->nullable();
            $table->timestamp('last_taping')->nullable();
            $table->timestamps();

            $table->foreign('id_project')
                  ->references('id')->on('projects')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersdata');
    }
}
