<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersToParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_to_parameters', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('user_model_id');
            $table->string('user_parameters_id');
            $table->string('rule_id');
            $table->string('id_projects');
            $table->string('operator');
            $table->float('values');
            $table->timestamps();

            $table->foreign('user_model_id')
                    ->references('id')->on('usersdata')
                    ->onDelete('cascade');

            $table->foreign('user_parameters_id')
                    ->references('id')->on('parameters')
                    ->onDelete('cascade');

            $table->foreign('id_projects')
                    ->references('id')->on('projects')
                    ->onDelete('cascade');

            $table->foreign('rule_id')
                    ->references('id')->on('rulesonroom')
                    ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_to_parameters');
    }
}
