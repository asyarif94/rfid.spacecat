<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacementParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('placement_parameters', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('id_placement');
            $table->string('name');
            $table->string('sub_name');
            $table->integer('value');
            $table->string('unit')->nullable();
            $table->boolean('allow_remote')->default('0');
            $table->integer('min_value')->default('0');
            $table->integer('max_value')->default('0');
            $table->string('operator');
            $table->string('by_value');
            $table->timestamps();

            $table->foreign('id_placement')
                    ->references('id')->on('roomdata')
                    ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_placement_parameters');
    }
}
