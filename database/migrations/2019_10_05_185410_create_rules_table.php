<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rulesonroom', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('rule_name');
            $table->string('sub_name')->nullable();
            $table->string('id_room')->nullable();
            $table->boolean('status')->default('1');
            $table->boolean('enable_schedule')->default('0');
            $table->string('day')->nullable();
            $table->string('date')->nullable();
            $table->time('start_time')->default('0')->nullable();
            $table->time('end_time')->default('0')->nullable();
            $table->string('offset_time')->default('0')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
     //
            $table->foreign('id_room')
                ->references('id')->on('roomdata')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rulesonroom');
    }
}
