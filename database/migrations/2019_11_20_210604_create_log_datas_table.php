<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('log_data', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->String('id_room');
            $table->String('id_reference_user_doing')->nullable();
            $table->String('old_room_name')->nullable();
            $table->String('id_rules')->nullable();
            $table->String('old_rule_name')->nullable();
            $table->String('id_project');
            $table->String('old_project_name')->nullable();
            $table->String('id_user')->nullable();
            $table->String('old_user_name')->nullable();
            $table->String('id_device');
            $table->String('mac_address');
            $table->String('ip_address');
            $table->String('wifi_ssid');
            $table->String('old_device_name')->nullable();
            $table->String('user_doing')->nullable();
            $table->String('callback')->nullable();
            $table->String('note_schedule')->nullable();
            $table->timestamps();

            $table->foreign('id_project')
                    ->references('id')->on('projects')
                    ->onDelete('cascade');

            $table->foreign('id_room')
                    ->references('id')->on('roomdata')
                    ->onDelete('cascade');

            $table->foreign('id_user')
                    ->references('id')->on('usersdata')
                    ->onDelete('cascade');

            $table->foreign('id_device')
                    ->references('id')->on('devices')
                    ->onDelete('cascade');

            $table->foreign('id_rules')
                ->references('id')->on('rulesonroom')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_datas');
    }
}
