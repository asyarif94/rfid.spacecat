<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('avatar');
			$table->boolean('status');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('provider_id')->nullable();
            $table->string('provider')->nullable();
            $table->string('id_plan')->nullable();;
            $table->string('id_level')->nullable();;
            $table->string('token');
            $table->string('proficiency')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('id_plan')
                  ->references('id')->on('user_plan')
                  ->onDelete('cascade');

            $table->foreign('id_level')
                  ->references('id')->on('user_level')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
