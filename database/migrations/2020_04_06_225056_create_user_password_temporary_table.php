<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPasswordTemporaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_password_temporary', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('id_refrence');
            $table->string('id_user');
            $table->string('secret_key_room');
            $table->string('id_project');
            $table->boolean('used')->default('0');

            $table->foreign('id_project')
                    ->references('id')->on('projects')
                    ->onDelete('cascade');

            $table->foreign('id_user')
                    ->references('id')->on('usersdata')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_password_temporary');
    }
}
