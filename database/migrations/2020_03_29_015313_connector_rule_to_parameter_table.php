<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConnectorRuleToParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connector_rule_to_parameter', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('id_projects');
            $table->string('rule_id');
            $table->string('parameter_id');
            $table->boolean('enable')->default('0');
            $table->integer('min_value')->default('0');
            $table->integer('max_value')->default('0');
            $table->string('id_placement');
            $table->string('by_value');
            $table->string('operator');
            $table->timestamps();

            $table->foreign('id_projects')
                    ->references('id')->on('projects')
                    ->onDelete('cascade');

            $table->foreign('id_placement')
                    ->references('id')->on('roomdata')
                    ->onDelete('cascade');

            $table->foreign('parameter_id')
                    ->references('id')->on('parameters')
                    ->onDelete('cascade');


            $table->foreign('rule_id')
                    ->references('id')->on('rulesonroom')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connector_rule_to_parameter');
    }
}
